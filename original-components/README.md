# original-components

Each folder consits of a React component. They are made with using React and styled-components library. Also some uses Sass to compare how a component styling would look using each library.

Each component has its style in the same file with the component.

### Running the projects

To run one of the components you have to navigate to the subfolder and run the following commands to start the component in the browser:

```
npm i
npm start
```

Some components also have Stroybook. To run the component in storybook run:

```
npm run storybook
```

### Conclusion of using styled-components library with React

- It allowed to add components styling stright into the componet file and made them more dynamic - didn't have to create new classes for every style but instead it gave the opportunity to insert variable into the styling and change style according to them.
- Downside was that the style classes were created by the library so there wouldn't be any classes overlapping, but this also ment that debugging and finding classnames was difficult.
