import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { StandaloneAlert } from "./components/StandaloneAlert/StandaloneAlert";
import styled from "styled-components";
import {AlertWithType} from "./components/Alert/AlertWithType";
import { AlertButton } from "./components/StandaloneAlert/AlertButton";
import {Icon} from "./components/Icon/Icon";
const TableStyle = styled.table`
  width: ${(props) => props.width ? props.width: 75}%;
  border: 1px solid;
  border-collapse: collapse;
`;

const ThStyle = styled.th`
  width: 50%;
  border: 1px solid;
`;

const TdStyle = styled.td`
    width:  ${(props) => props.width ? props.width : 50}%;
    border: 1px solid;
`;


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <div>
        <h1>Inline alert</h1>
    <TableStyle width={50}>
        <tr>
            <TdStyle width={20}> Notice </TdStyle>
            <TdStyle> <AlertWithType type="info" >
                <span>To add Club One number to this passenger, please call our customer support: <b>17 808</b></span>
            </AlertWithType>
            </TdStyle>
        </tr>
        <tr>
            <TdStyle width={20}> Error </TdStyle>
            <TdStyle> <AlertWithType type="error">
                <span>Your booking was unsuccessfully paid and is unconfirmed. <u><b>See confirmation PDF</b></u> (also sent to your e-mail) for full details about the
                additional payment options.</span>
            </AlertWithType> </TdStyle>
        </tr>
        <tr>
            <TdStyle width={20}> Important </TdStyle>
            <TdStyle> <AlertWithType type="important">Club One number must be added to this passenger, please call our
                customer support: <b>17 808</b></AlertWithType> </TdStyle>
        </tr>
        <tr>
            <TdStyle width={20}> Success </TdStyle>
            <TdStyle> <AlertWithType type="success">
                <span> Minu text suur <b>Dismiss</b></span>
            </AlertWithType> </TdStyle>
        </tr>
        </TableStyle>
      <h1>Standalone alert</h1>
        <TableStyle>
            <tr>
                <TdStyle width={20}> type:info, size: small </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"info"}
                        size={"small"}
                        header="Just so you know"
                    >
                        To add Club One number to this passenger, please call our customer support: <b>17 808</b>
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:info, size: large </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"info"}
                        size={"large"}
                        header="Just so you know"
                    >
                        To add Club One number to this passenger, please call our customer support: <b>17 808</b>
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:success, size: small </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        header={"Great success!"}>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:success, size: large </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"large"}>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:error, size: small </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"small"}
                        header={"Oh no, Peter, something went wrong!"}>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:error, size: large </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"large"}
                        header={"Oh no, Peter, something went wrong!"}>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>

            <tr>
                <TdStyle width={20}> type:error, size: small, fullscreen </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"small"}
                        isFullScreen>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:error, size: large, fullscreen </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"large"}
                        isFullScreen>
                        Please check the e-mail address.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>
        <h1>Dismiss</h1>
        <TableStyle width={100}>
            <tr>
                <TdStyle width={20}> type:success, size: small, dismiss </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                    >
                        Club One number to this passenger can be added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:success, size: large, dismiss </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"large"}
                        interactionContent={"Dismiss"}
                    >
                        Club One number to this passenger can be added. Great discount available. Just letting you know.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>
        <h1>Different types </h1>
        <TableStyle width={100}>
            <tr>
                <TdStyle width={20}> type:success, size: small, dismiss </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                    >
                        Club One number to this passenger can be added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:error, size: small, dismiss </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                    >
                        Club One number to this passenger can be added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> type:info, size: small, dismiss </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"info"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                    >
                        Club One number to this passenger can be added. Great discount available. Just letting you know.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>

        <h1>Different parent component size</h1>

        <TableStyle width={30}>
            <tr>
                <TdStyle> success, small, dismiss, 30% </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                        header={"Great success!"}
                    >
                        Club One number to this passenger can be added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>

        <TableStyle width={50}>
            <tr>
                <TdStyle> error, small, dismiss, 50% </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"error"}
                        size={"small"}
                        interactionContent={"Reload"}
                        header={"Great error!"}
                    >
                        Something happened. Please try to reload the page. If the problem persists please call our customer support. We apologize for the inconvenience.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>

        <TableStyle width={60}>
            <tr>
                <TdStyle> info, small, dismiss, 60% </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"info"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                        header={"Great info!"}
                    >
                        Club One number to this passenger can be added. Great discount available. Just letting you know.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>

        <TableStyle width={60}>
            <tr>
                <TdStyle> info, small, dismiss+bottom, 60% </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"info"}
                        size={"small"}
                        interactionContent={[<AlertButton key="1" onClick={() => { alert('clicked');}}>AWESOME, ADD NUMBER</AlertButton>
                            , <a key="2" href="#">Maybe later</a>]}
                        header={"Great info!"}
                    >
                        Club One number to this passenger can be added. Great discount available. Just letting you know.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>
        <h1>With icon examples</h1>
        <TableStyle>
            <tr>
                <TdStyle width={20}> success, small, dismiss, icon24 </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                        icon={<Icon size={24} name={"sightseeing"}/>}
                    >
                        Club One number to this passenger was successfully added. TEST TEST
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> success, small, dismiss, icon24, header </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={"Dismiss"}
                        header={"Test header"}
                        icon={<Icon size={24} name={"sightseeing"}/>}
                    >
                        Club One number to this passenger was successfully added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> success,  smale, dismiss button, icon18, header </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={[<AlertButton onClick={() => { alert('clicked');}}>Button text</AlertButton>,<a href="#">Dismiss</a>]}
                        header={"Test header"}
                        icon={<Icon size={18} name={"sightseeing"}/>}
                    >
                        Club One number to this passenger was successfully added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
            <tr>
                <TdStyle width={20}> success,  smale, dismiss button, icon18, header </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={[<AlertButton onClick={() => { alert('clicked');}}>Button text</AlertButton>,<a href="http://www.cs.ut.ee">Dismiss</a>]}
                        header={"Test header"}
                        icon={<Icon size={18} name={"sightseeing"}/>}
                    >
                        Club One number to this passenger was successfully added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>

        <h1>Test</h1>
        <TableStyle>
            <tr>
                <TdStyle width={20}> success,  smale, dismiss button, icon18, header </TdStyle>
                <TdStyle>
                    <StandaloneAlert
                        type={"success"}
                        size={"small"}
                        interactionContent={[<AlertButton onClick={() => { alert('clicked');}}>Awasome</AlertButton>,<a href="http://www.cs.ut.ee">Dismiss</a>]}
                        icon={<Icon size={18} name={"sightseeing"}/>}
                        header={"Test header"}
                    >
                        Club One number to this passenger was successfully added.
                    </StandaloneAlert>
                </TdStyle>
            </tr>
        </TableStyle>
     </div>
);
