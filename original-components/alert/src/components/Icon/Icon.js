import styled from "styled-components";

export const IconStyled = styled.img`
  object-fit: contain;
  flex-grow: 0;
  width: ${props => props.size ? props.size : 18}px;
`;

export function Icon(props) {
  const { name, size } = props;
  return <IconStyled size={size} src={`/icons/${name}.svg`}/>;
}
