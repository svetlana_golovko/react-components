import styled from "styled-components";
import { Icon } from "../Icon/Icon";
import GTWalsheimPro from "../../fonts.css";

const AlertText = styled.div`
  align-self: stretch;
  flex-grow: 0;
  font-family: GTWalsheimPro;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  text-align: left;
  color: #004152;
`;
const AlertIcon = styled.div`
  width: 18px;
  height: 18px;
  flex-grow: 0;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  gap: 10px;
  padding: 1px 0;
`;
const AlertBox = styled.div`
  align-self: stretch;
  flex-grow: 0;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  gap: 8px;
  padding: 0;
  margin: 8px 0;
`;

export function Alert(props) {
  const {
    type,
    size,
    isEmbedded,
    isFullscreen,
    icon,
    optionalHeading,
    optionalIntercation,
    children,
  } = props;
  return (
    <AlertBox>
      <AlertIcon>
        <Icon iconName={icon} />
      </AlertIcon>
      <AlertText>{children}</AlertText>
    </AlertBox>
  );
}
