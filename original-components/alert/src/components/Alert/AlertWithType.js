import styled from "styled-components";
import { Icon } from "../Icon/Icon";
import WalsheimProRegular from "../../fonts.css";
const AlertText = styled.div`
  align-self: stretch;
  flex-grow: 0;
  font-family: WalsheimProRegular;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  text-align: left;
  color: #004152;
`;
const AlertIcon = styled.div`
  width: 18px;
  height: 18px;
  flex-grow: 0;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  padding: 1px 0;
`;
const AlertBox = styled.div`
  align-self: stretch;
  flex-grow: 0;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  gap: 8px;
  padding: 0;
  margin: 8px 0;
`;

function getTypeIcon(type) {
  if (type === "error")
    return "Type=error"
  if (type === "important")
    return "Type=important"
  if (type === "success")
    return "Type=success"
  return "Type=info"
}



export function AlertWithType(props) {
  const {
    type
  } = props;
  return (
    <AlertBox>
      <AlertIcon>
        <Icon name={getTypeIcon(type)} />
      </AlertIcon>
      <AlertText>{props.children}</AlertText>
    </AlertBox>
  );
}
