import React from "react";

import { AlertWithType } from "./AlertWithType";

export default {
  title: "Components/Alert with type",
  component: AlertWithType,
  type: {
    options: ["info", "error", "success", "important"],
    control: { type: 'select' },
  },
};

const Template = (args) => <AlertWithType {...args} />;

export const Error = Template.bind({});
Error.args = {
  type: "error",
  children: "Error icon",
};
export const Important = Template.bind({});
Important.args = {
  type: "important",
  children: "Important icon",
};
export const Success = Template.bind({});
Success.args = {
  type: "success",
  children: "Success icon",
};
export const Info = Template.bind({});
Info.args = {
  type: "info",
  children: "Info icon",
};