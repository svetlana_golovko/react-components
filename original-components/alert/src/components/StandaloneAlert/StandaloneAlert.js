import styled, { css } from "styled-components";
import { useEffect, useRef, useState } from "react";
import { colors } from "../../styles/colors";
import { AlertInteraction } from "./AlertInteraction";
import { AlertBox } from "./AlertBox";

const largeStyled = css`
  gap: ${(props) => (props.alertFlexDirection ? "11px" : "23px")};
  padding: 16px 24px 16px ${(props) => (props.isFullScreen ? "24px" : "28px")};
`;

const smallStyled = css`
  gap: ${(props) => (props.alertFlexDirection ? "7px" : "15px")};
  padding: 12px 16px 12px ${(props) => (props.isFullScreen ? "16px" : "20px")};
`;

const infoStyled = css`
  box-shadow: ${(props) =>
    props.isFullScreen
      ? null
      :`inset 4px 0 0 0 ${colors.poolLightShadow}`};
  background-color: ${colors.poolLight};
`;
const successStyled = css`
  box-shadow: ${(props) =>
    props.isFullScreen
      ? null 
      : `inset 4px 0 0 0 ${colors.buoy}`};
  background-color: ${colors.buoyLight};
`;
const errorStyled = css`
  box-shadow: ${(props) =>
    props.isFullScreen
      ? null
      : `inset 4px 0 0 0 ${colors.error}`};
  background-color: ${colors.sunset};
`;

const StandaloneAlertStyled = styled.div`
  align-self: stretch;
  flex-grow: 0;
  display: flex;
  flex-direction: ${(props) => (props.alertFlexDirection ? "column" : "row")};
  flex-warp: wrap;
  justify-content: flex-start;
  align-items: center;
  padding: 0;
  max-width: 100%;
  ${(props) => {
    if (props.size === "small") {
      return smallStyled;
    }
    if (props.size === "large") {
      return largeStyled;
    }
  }}
  ${(props) => {
    if (props.type === "info") {
      return infoStyled;
    }
    if (props.type === "success") {
      return successStyled;
    }
    if (props.type === "error") {
      return errorStyled;
    }
  }}
  @media screen and (max-width: 600px) {
    flex-direction: column;
  }
`;

export function StandaloneAlert(props) {
  const { interactionContent } = props;
  const alertBoxRef = useRef();
  const [alertFlexDirection, setAlertFlexDirection] = useState(false);

  useEffect(() => {
    if (alertBoxRef.current) {
      const { width } = alertBoxRef.current.getBoundingClientRect();
      setAlertFlexDirection(width < 600);
    }
  },[alertFlexDirection]);

  return (
    <StandaloneAlertStyled
      {...props}
      ref={alertBoxRef}
      alertFlexDirection={alertFlexDirection}
    >
      <AlertBox {...props} />

      {interactionContent && (
        <AlertInteraction
          alertFlexDirection={alertFlexDirection}
          content={interactionContent}
        />
      )}
    </StandaloneAlertStyled>
  );
}
