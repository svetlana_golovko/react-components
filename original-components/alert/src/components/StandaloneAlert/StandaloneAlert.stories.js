import React from "react";

import { StandaloneAlert } from "./StandaloneAlert";
import { Icon } from "../Icon/Icon";
import { AlertButton } from "./AlertButton";

export default {
  title: "Components/Standalone alert",
  component: StandaloneAlert,
  argTypes: {
    size: {
      options: ["small", "large"],
      control: { type: "radio" },
    },
    type: {
      options: ["info", "error", "success"],
      control: { type: "select" },
    },
  },
};

const Template = (args) => <StandaloneAlert {...args} />;

export const Small = Template.bind({});
Small.args = {
  type: "info",
  size: "small",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Large = Template.bind({});
Large.args = {
  type: "info",
  size: "large",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Info = Template.bind({});
Info.args = {
  type: "info",
  size: "large",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Error = Template.bind({});
Error.args = {
  type: "error",
  size: "large",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Success = Template.bind({});
Success.args = {
  type: "success",
  size: "large",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Fullscreen = Template.bind({});
Fullscreen.args = {
  type: "error",
  size: "large",
  isFullScreen: true,
  children: "Please check the e-mail address.",
};
export const Embedded = Template.bind({});
Embedded.args = {
  type: "info",
  size: "large",
  isFullScreen: false,
  children: "Please check the e-mail address.",
};

export const Button = Template.bind({});
Button.args = {
  type: "success",
  size: "large",
  isFullScreen: false,
  interactionContent: "Dismiss",
  children: "Please check the e-mail address.",
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  type: "success",
  size: "large",
  isFullScreen: false,
  interactionContent: "Dismiss",
  icon: <Icon name={"home"} size={20} />,
  children: "Please check the e-mail address.",
};

export const Header = Template.bind({});
Header.args = {
  type: "success",
  size: "large",
  isFullScreen: false,
  interactionContent: "Dismiss",
  icon: <Icon name={"home"} size={20} />,
  header: "Test header",
  children: "Club One number to this passenger was successfully added.",
};

export const All = Template.bind({});
All.args = {
  type: "success",
  size: "small",
  isFullScreen: false,
  interactionContent: [
    <AlertButton
      key="1"
      onClick={() => {
        alert("clicked");
      }}
    >
      AWESOME, ADD NUMBER
    </AlertButton>,
    <a key="2" href="#">
      Maybe later
    </a>,
  ],
  icon: <Icon name={"home"} size={20} />,
  header: "Test header",
  children: "Club One number to this passenger was successfully added.",
};
