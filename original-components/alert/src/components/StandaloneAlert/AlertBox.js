import styled from "styled-components";
import { colors } from "../../styles/colors";
import WalsheimProRegular from "../../fonts.css";
const AlertHeader = styled.div`
  align-self: stretch;
  flex-grow: 0;
  font-family: WalsheimProRegular;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: ${(props) => (props.size === "large" ? 1.3 : 1.33)};
  font-size: ${(props) => (props.size === "large" ? 20 : 18)}px;
  letter-spacing: normal;
  text-align: left;
  color: ${colors.midnight};
`;

const AlertText = styled.div`
  align-self: stretch;
  flex-grow: 0;
  font-family: WalsheimProRegular;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  text-align: left;
  color: ${colors.midnight};
`;
const AlertIcon = styled.div`
  flex-grow: 0;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  align-self: center;
  gap: 10px;
  padding: 1px 0;
`;

const AlertBody = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  gap: ${(props) => {
    if (props.size === "small") {
      return "6px";
    }
    if (props.size === "large") {
      return "8px";
    }
  }};
  padding: 0;
`;

const AlertBoxStyled = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  gap: ${(props) => {
    if (props.size === "small") {
      return "16px";
    }
    if (props.size === "large") {
      return "24px";
    }
  }};
`;

export function AlertBox(props) {
  const { size, header, children, icon} =
    props;
  return (
    <AlertBoxStyled {...props}>
        {icon && (
            <AlertIcon>
                {icon}
            </AlertIcon>
        )}

      <AlertBody {...props}>
        {header && (
          <AlertHeader size={size}>{header}</AlertHeader>
        )}
        <AlertText>{children}</AlertText>
      </AlertBody>
    </AlertBoxStyled>
  );
}
