import styled, { css } from "styled-components";
import WalsheimProRegular from "../../fonts.css";
const ButtonStyled = styled.button`
  min-height: 40px;
  align-self: stretch;
  flex-grow: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 8px 24px;
  background-color: #00ecc9;
  border-width: 0;
`;

const ButtonTextStyled = styled.div`
  flex-grow: 0;
  font-family: WalsheimProRegular;
  font-size: 16px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.38;
  letter-spacing: normal;
  text-align: center;
  color: #004152;
`;
export function AlertButton(props) {
  const { onClick } = props;
  return (
    <ButtonStyled
      onClick={() => {
        onClick();
      }}
    >
      {" "}
      <ButtonTextStyled>{props.children}</ButtonTextStyled>{" "}
    </ButtonStyled>
  );
}
