import styled, { css } from "styled-components";
import { colors } from "../../styles/colors";
import WalsheimProRegular from "../../fonts.css";

const AlertInteractionStyled = styled.div`
  flex-grow: 0;
  font-family: WalsheimProRegular;
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  text-align: left;
  color: ${colors.midnight};
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: ${(props) => (props.alertFlexDirection ? "column" : "row")};
  gap: ${(props) => (props.alertFlexDirection ? "12px" : "24px")};
   ${(props) => {
  if (props.alertFlexDirection) {
    return css`
        background-image: linear-gradient(
          to left,
          ${colors.midnight} 33%,
          rgba(255, 255, 255, 0) 0%
        );
        background-position: top;
        background-size: 5px 1px;
        background-repeat: repeat-x;
        min-width: 100%;
        padding-top: 12px;
      `;
  } else {
    return css`
        background-image: linear-gradient(
          to top,
          ${colors.midnight} 33%,
          rgba(255, 255, 255, 0) 0%
        );
        background-position: left;
        background-size: 1px 5px;
        background-repeat: repeat-y;
        min-height: 100%;
        align-self: stretch;
        padding-left: 24px;
        padding-top: 7px;
        padding-bottom: 7px;
      `;
  }
}}
@media screen and (max-width: 600px) {
  flex-direction: column;
  gap: 12px;
  background-image: linear-gradient(
    to left,
    ${colors.midnight} 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: top;
  background-size: 5px 1px;
  background-repeat: repeat-x;
  min-width: 100%;
  padding-top: 12px;
  padding-left: 0;
  padding-right: 0;
  padding-bottom: 0;
}
`;


export function AlertInteraction(props) {
  const { content, alertFlexDirection } = props;
  return (
      <AlertInteractionStyled alertFlexDirection={alertFlexDirection}>
        {content}
      </AlertInteractionStyled>
  );
}
