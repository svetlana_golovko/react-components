export const colors = {
    poolLight: "#c2f7ff",
    poolLightShadow: "#6ecbd9",
    midnight: "#004152",
    buoyLight: "#cdf8eb",
    buoy: "#00bea2",
    sunset: "#ffc8be",
    error: "#d97e7e",
  };