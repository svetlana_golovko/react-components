import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import { ToolttipSASSExample, ToolttipSCExample } from "./App";
/*
  To try Styled Component (SC) example comment out line 2 in Tooltip.js so there wont be any interference with SASS and SC
  To try SASS example also uncomment line 2 in Tooltip.js
*/
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ToolttipSCExample />
  </React.StrictMode>
);
