import {useEffect, useRef, useState} from "react";
//import "./Tooltip.scss";

export default function Tooltip({children, content}) {
    const triangleLocationOffset = 28;
    const [showTooltip, setShowTooltip] = useState(false);
    const [tooltipTriangleVerticalPosition, setTooltipTriangleVerticalPosition] = useState(
        "tooltip-triangle-top-left"
    );
    const [tooltipClick, setTooltipClick] = useState(false);
    const [pos, setPos] = useState({});
    const tooltipRef = useRef(null);
    const tooltipTargetRef = useRef(null);

    useEffect(() => {
        if (showTooltip) {
            setTooltipNewPosition();
        } else {
            setTooltipClick(false);
            setPos({});
        }
    }, [showTooltip]);

    const setTooltipNewPosition = ( ) => {
        const {right} = tooltipRef.current.getBoundingClientRect();
        const {width} = tooltipTargetRef.current.getBoundingClientRect();
        const windowWidth = window.visualViewport.width;

        let xPos = 0;

        if (windowWidth < right) {
            xPos = windowWidth - right;
        } else {
            xPos = width / 2 - triangleLocationOffset;
        }
        setPos((previousState) => {
            return {
                ...previousState,
                "--x-position": `${xPos}px`,
            };
        });
        setTooltipTriangleNewPosition( );
    };

    const setTooltipTriangleNewPosition = () => {
        const windowWidth = window.visualViewport.width;
        const windowHeight = window.visualViewport.height;

        const {right, bottom} = tooltipRef.current.getBoundingClientRect();
        const {width} = tooltipTargetRef.current.getBoundingClientRect();
        const tooltipTargetCenterWithOffset = width / 2 - 10;
        console.log("windowWidth:"+windowWidth +
            ",right:"+right+
            ",windowHeight:"+windowHeight+
            ",bottom:"+bottom
        )
        if (windowWidth < right && windowHeight > bottom) {
            setTooltipTriangleVerticalPosition("tooltip-triangle-top-right");
            console.log(1)
        } else if (windowWidth < right && windowHeight < bottom) {
            console.log(2)
            setTooltipTriangleVerticalPosition("tooltip-triangle-bottom-right bottom");
        } else if (windowWidth > right && windowHeight < bottom) {
            console.log(3)
            setTooltipTriangleVerticalPosition("tooltip-triangle-bottom-left bottom");
        }
        if (windowWidth < right) {
            console.log(4)
            setPos((previousState) => {
                return {
                    ...previousState,
                    "--tooltip-triangle-left": `${
                        Math.abs(windowWidth - right) + tooltipTargetCenterWithOffset
                    }px`,
                };
            });
        }
    };

    const handleOnMouseEnter = (e) => {
        setShowTooltip(true);
    };
    const handleOnMouseLeave = () => {
        if (!tooltipClick) {
            setShowTooltip(false);
            setPos({});
        }
    };
    const handleTooltipClick = (e) => {
        setTooltipClick(true);
    };

    const handleCtaClick = () => {
        setShowTooltip(false);
    };

    return (
        <div
            className="tooltip-target"
            onMouseEnter={(e) => handleOnMouseEnter(e)}
            onMouseLeave={() => handleOnMouseLeave()}
            onClick={(e) => handleTooltipClick(e)}
            ref={tooltipTargetRef}
        >
            {children}
            {showTooltip && (
                <div
                    ref={tooltipRef}
                    className={`tooltip-container ${tooltipTriangleVerticalPosition}`}
                    style={pos}
                >
                    {content.header && (
                        <div className="tooltip-header">{content.header}</div>
                    )}
                    <div className="tooltip-text">{content.text}</div>
                    {tooltipClick ? (
                        <div className="tooltip-button-container">
                            <button
                                className="tooltip-button"
                                onClick={() => handleCtaClick()}
                            >
                                Dismiss
                            </button>
                        </div>
                    ) : null}
                </div>
            )}
        </div>
    );
}
