import { TooltipSCButtonContainer } from "../TooltipSCButtonContainer/TooltipSCButtonContainer";
import styled from "styled-components";
import WalsheimProRegular from "../../../fonts.css"

const TooltipText = styled.div`
  font-family: WalsheimProRegular;
  font-weight: regular;
  font-size: 14px;
`;

const TooltipHeader = styled.div`
  height: 24px;
  align-self: stretch;
  font-size: 18px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: left;
  margin-bottom: 6px;
  font-family: WalsheimProRegular;
  font-weight: 500;
`;
const TooltipContainer = styled.div`
  display: block;
  background-color: white;
  min-width: 240px;
  max-width: min-content;
  color: #004152;
  border-left: 4px solid #6ecbd9;
  position: absolute;
  z-index: 1;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.1);
  padding: 12px 16px;
  text-align: left;
  margin-top: 12px;
  margin-bottom: 12px;
  transform: translateX(${(props) => props.pos.xPosition});
  &:after {
    content: " ";
    position: absolute;
    border-width: 7px 8px;
    border-style: solid;
    top: ${(props) => props.tooltipTriangleVerticalPosition.tooltipTriangleTop};
    bottom: ${(props) =>
      props.tooltipTriangleVerticalPosition.tooltipTriangleBottom};
    left: ${(props) => props.pos.tooltipTriangleLeft};
    border-color: ${(props) =>
      props.tooltipTriangleVerticalPosition.tooltipTriangleBorderColor};
  }
  bottom: ${(props) => (props.isBottom ? "100%" : "none")};
`;

export function TooltipSCContainer(props) {
  const {
    tooltipRef,
    tooltipTriangleVerticalPosition,
    isBottom,
    pos,
    handleCtaClick,
    tooltipClick,
    content,
  } = props;
  return (
    <TooltipContainer
      ref={tooltipRef}
      tooltipTriangleVerticalPosition={tooltipTriangleVerticalPosition}
      isBottom={isBottom}
      pos={pos}
    >
      {content.header && <TooltipHeader>{content.header}</TooltipHeader>}
      <TooltipText>{content.text}</TooltipText>
      {tooltipClick ? (
        <TooltipSCButtonContainer handleCtaClick={() => handleCtaClick()} />
      ) : null}
    </TooltipContainer>
  );
}
