import styled from "styled-components";
import WalsheimProRegular from "../../../fonts.css"

const TooltipButton = styled.button`
  text-decoration: underline;
  margin-top: 12px;
  margin-bottom: 12px;
  cursor: pointer;
  display: block;
  background: none !important;
  border: none;
  padding: 0 !important;
  margin: 0 auto;
  color: #004152;
  font-family: WalsheimProRegular;
  font-weight: 500;
  font-size: 14px;
`;
const TooltipButtonContainer = styled.div`
  padding-top: 12px;
  background-image: linear-gradient(
    to right,
    black 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: top;
  background-size: 5px 1px;
  background-repeat: repeat-x;
  margin-top: 12px;
`;
export function TooltipSCButtonContainer(props) {
  const { handleCtaClick } = props;

  return (
    <TooltipButtonContainer>
      <TooltipButton onClick={() => handleCtaClick()}>Dismiss</TooltipButton>
    </TooltipButtonContainer>
  );
}
