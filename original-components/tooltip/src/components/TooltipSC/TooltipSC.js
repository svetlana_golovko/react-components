import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { TooltipSCContainer } from "./TooltipSCContainer/TooltipSCContainer";

const TooltipTarget = styled.div`
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
  cursor: pointer;
`;

export default function TooltipSC({ children, content }) {
  const triangleLocationOffset = 28;
  const [showTooltip, setShowTooltip] = useState(false);
  const [tooltipTriangleVerticalPosition, setTooltipTriangleVerticalPosition] =
    useState({
      tooltipTriangleTop: null,
      tooltipTriangleBottom: "100%",
      tooltipTriangleBorderColor: "transparent transparent white transparent",
    });
  const [isBottom, setIsBottom] = useState(false);
  const [tooltipClick, setTooltipClick] = useState(false);
  const [pos, setPos] = useState({});
  const tooltipRef = useRef(null);
  const tooltipTargetRef = useRef(null);

  useEffect(() => {
    if (showTooltip) {
      setTooltipNewPosition();
    } else {
      setTooltipClick(false);
      setPos({});
      setIsBottom(false);
    }
  }, [showTooltip]);

  const setTooltipNewPosition = () => {
    const { right } = tooltipRef.current.getBoundingClientRect();
    const { width } = tooltipTargetRef.current.getBoundingClientRect();
    const windowWidth = window.visualViewport.width;

    let xPos = 0;

    if (windowWidth < right) {
      xPos = windowWidth - right;
    } else {
      xPos = width / 2 - triangleLocationOffset;
    }
    setPos((previousState) => {
      return {
        ...previousState,
        xPosition: `${xPos}px`,
      };
    });
    setTooltipTriangleNewPosition();
  };

  const setTooltipTriangleNewPosition = () => {
    const windowWidth = window.visualViewport.width;
    const windowHeight = window.visualViewport.height;

    const { right, bottom } = tooltipRef.current.getBoundingClientRect();
    const { width } = tooltipTargetRef.current.getBoundingClientRect();
    const tooltipTargetCenterWithOffset = width / 2 - 10;

    if (windowWidth < right && windowHeight > bottom) {
      setTooltipTriangleVerticalPosition({
        tooltipTriangleTop: null,
        tooltipTriangleBottom: "100%",
        tooltipTriangleBorderColor: "transparent transparent white transparent",
      });
    } else if (windowWidth < right && windowHeight < bottom) {
      setTooltipTriangleVerticalPosition({
        tooltipTriangleTop: "100%",
        tooltipTriangleBottom: null,
        tooltipTriangleBorderColor: "white transparent transparent transparent",
      });
      setIsBottom(true);
    } else if (windowWidth > right && windowHeight < bottom) {
      setTooltipTriangleVerticalPosition({
        tooltipTriangleTop: "100%",
        tooltipTriangleBottom: null,
        tooltipTriangleBorderColor: "white transparent transparent transparent",
      });
      setIsBottom(true);
    }
    if (windowWidth < right) {
      setPos((previousState) => {
        return {
          ...previousState,
          tooltipTriangleLeft: `${
            Math.abs(windowWidth - right) + tooltipTargetCenterWithOffset
          }px`,
        };
      });
    }
  };

  const handleOnMouseEnter = (e) => {
    setShowTooltip(true);
  };
  const handleOnMouseLeave = () => {
    if (!tooltipClick) {
      setShowTooltip(false);
      setPos({});
    }
  };
  const handleTooltipClick = (e) => {
    setTooltipClick(true);
  };

  const handleCtaClick = () => {
    setShowTooltip(false);
  };

  return (
    <TooltipTarget
      onMouseEnter={(e) => handleOnMouseEnter(e)}
      onMouseLeave={() => handleOnMouseLeave()}
      onClick={(e) => handleTooltipClick(e)}
      ref={tooltipTargetRef}
    >
      {children}
      {showTooltip && (
        <TooltipSCContainer
          tooltipRef={tooltipRef}
          tooltipTriangleVerticalPosition={tooltipTriangleVerticalPosition}
          isBottom={isBottom}
          pos={pos}
          handleCtaClick={handleCtaClick}
          tooltipClick={tooltipClick}
          content={content}
        />
      )}
    </TooltipTarget>
  );
}
