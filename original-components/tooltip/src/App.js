import Tooltip from "./components/Tooltip/Tooltip";
import TooltipSC from "./components/TooltipSC/TooltipSC";

export function ToolttipSASSExample() {
  return (
    <div className="container">
      Tere tere
      <Tooltip
        content={{
          header: "Optional Header",
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </Tooltip>
      tere tere tere tere tere tere tere tere tere tere tere tere tere tere tere
      tere tere tere tere tere tere tere
      <Tooltip
        content={{
          text: "Lorem ipsum dolor sit amet, blandit massa.",
        }}
      >
        Tere ja Tere
      </Tooltip>
      tere tere tere tere tere tere
      <Tooltip
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </Tooltip>
      tere tere
      <Tooltip
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </Tooltip>
      <Tooltip
        content={{
          text: "L.",
        }}
      >
        Tere
      </Tooltip>
      <Tooltip
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </Tooltip>
      <div className="footer">
        <Tooltip
          content={{
            text: "L.",
          }}
        >
          Tere ja tere
        </Tooltip>
        test test test test test test test test test
        <Tooltip
          content={{
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
          }}
        >
          Tereeeeeeeeeee
        </Tooltip>
        test test test test test test test test test
        <Tooltip
          content={{
            header: "Header",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
          }}
        >
          Tereeeee
        </Tooltip>
      </div>
    </div>
  );
}

export function ToolttipSCExample() {
  return (
    <div className="container">
      Tere tere
      <TooltipSC
        content={{
          header: "Optional Header",
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </TooltipSC>
      tere tere tere tere tere tere tere tere tere tere tere tere tere tere tere
      tere tere tere tere tere tere tere
      <TooltipSC
        content={{
          text: "Lorem ipsum dolor sit amet, blandit massa.",
        }}
      >
        Tere ja Tere
      </TooltipSC>
      tere tere tere tere tere tere
      <TooltipSC
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </TooltipSC>
      tere tere
      <TooltipSC
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </TooltipSC>
      <TooltipSC
        content={{
          text: "L.",
        }}
      >
        Tere
      </TooltipSC>
      <TooltipSC
        content={{
          text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
        }}
      >
        Tere
      </TooltipSC>
      <div className="footer">
        <TooltipSC
          content={{
            text: "L.",
          }}
        >
          Tere ja tere
        </TooltipSC>
        test test test test test test test test test
        <TooltipSC
          content={{
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
          }}
        >
          Tereeeeeeeeeee
        </TooltipSC>
        test test test test test test test test test
        <TooltipSC
          content={{
            header: "Header",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ex fringilla, pretium magna a, blandit massa.",
          }}
        >
          Tereeeee
        </TooltipSC>
      </div>
    </div>
  );
}

