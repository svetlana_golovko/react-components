import React from "react";
import { createRoot } from "react-dom/client";
import {
  render,
  fireEvent,
  cleanup,
  screen,
  getByTestId,
} from "@testing-library/react";
import "@testing-library/jest-dom";
import Hero from "./Hero";
import { ArrowButtons } from "./components/Buttons";

//mis teegid kasutusel
//unit testid
//font on browser
//end to end test
//ilma backendita testid
//viimase commiti kaudu oleks võimalik näidata

/*
enzyme - test  components in isolation; shallow and mount. Shallow mocks children and doesnt mount it 
react-testing-library - functionalty from the end users point of view

*/

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  cleanup;
});

describe("Hero Component", () => {
  it("renders Hero without crashing", () => {
    const div = document.createElement("div");
    const root = createRoot(div);
    root.render(
      <Hero
        isFullBleed={true}
        colorScheme={"cruise_1"}
        data={[
          {
            img: "img/water.jpg",
            textHeader: `header 4`,
            textContext: "Context 4",
          },
        ]}
      />
    );
  });
  it("clicks next arrow button", () => {
    const wrapper = render(
      <Hero
        isFullBleed={true}
        colorScheme={"cruise_1"}
        data={[
          {
            img: "img/earth.jpg",
            textHeader: `header 1`,
            textContext: "Context 1",
          },
          {
            img: "img/water.jpg",
            textHeader: `header 4`,
            textContext: "Context 4",
          },
        ]}
      />
    );

    const header1 = wrapper.getByText("header 1");
    const header4 = wrapper.getByText("header 4");
    const nextArrow = wrapper.getByText("\u276E");
    
    expect(nextArrow).toBeVisible;
    expect(header1).toBeVisible();
    expect(header4).not.toBeVisible();

    fireEvent.click(nextArrow);

    expect(header1).not.toBeVisible();
    expect(header4).toBeVisible();
  });
});

/*
it("Item changes when arrow is clicked", async () => {
  render(
    <Hero
      isFullBleed={true}
      colorScheme={"cruise_1"}
      data={[
        {
          img: "img/water.jpg",
          textHeader: `header 1`,
          textContext: "Context 1",
        },
        {
          img: "img/water.jpg",
          textHeader: `header 2`,
          textContext: "Context 2",
        },
      ]}
    />
  );

});*/
