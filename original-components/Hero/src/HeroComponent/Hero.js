import { Carousel } from "./components/Carousel";
import {HeroContext} from "./contexts/HeroContext";

function Hero(props) {
  let { isFullBleed, colorScheme, data } = props;
  return (
        <HeroContext.Provider value={{isFullBleed: isFullBleed, colorScheme: colorScheme, data: data}}>
            <Carousel/>
        </HeroContext.Provider>
  );
}

export default Hero;
