import {createContext} from "react";


export const HeroContext = createContext({isFullBleed: true, colorScheme: "combo-1", data: []})