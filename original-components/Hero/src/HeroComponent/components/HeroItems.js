import parse, { domToReact } from "html-react-parser";
import { HeroContext } from "../contexts/HeroContext";
import { Button } from "./Buttons";
import React from "react";

export default function HeroItems(props) {
    const context = React.useContext(HeroContext);
    return (context.data.map((heroObject, index) => (
        <HeroItem
            image={heroObject["img"]}
            textHeader={heroObject["textHeader"]}
            textContent={heroObject["textContext"]}
            isActive={props.activeIndex === index}
            key={index}
            isFullBleed = {context.isFullBleed}
            colorScheme = {heroObject.colorScheme || context.colorScheme}
        />)));
}

function formatHeader(headerContent) {
  const startTag = "colored";
  let className = "textarea-header-text-second-color";

  const options = {
    replace: (domNode) => {
      if (domNode.attribs && domNode.name === `${startTag}`) {
        return (
            <span className={className}>
              {domToReact(domNode.children, options)}
            </span>
        );
      }
    },
  };
  return parse(headerContent, options);
}

function HeroItem(props) {
  let { textHeader, textContent, image, isActive, isFullBleed, colorScheme } = props;
  let hasContent = !!textHeader || !!textContent;
  return (
          <div
            className={`hero-item ${isActive ? "hero-item-active" : null} ${
              isFullBleed
                ? "hero-item-full-bleed"
                : "hero-item-half-bleed"
            }`}
          >
            <div>
              <img src={image} />
              {hasContent && (
                <div className={`textarea ${colorScheme}`}>
                  <div className="textarea-border">
                    <div className="textarea-svg-container">
                      <svg className="svg" viewBox="0 0 10 10">
                        <circle cx="50%" cy="50%" r="5" />
                      </svg>
                    </div>
                    <h1 className="textarea-header textarea-header-text-main-color">
                      {formatHeader(textHeader)}
                    </h1>{" "}
                    <br />
                    <h5 className="textarea-content">{textContent}</h5> <br />
                    <Button style={"textarea-button"} buttonText={"Action"} />
                  </div>
                </div>
              )}
            </div>
          </div>
        );
}
