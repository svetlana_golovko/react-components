import {HeroContext} from "../contexts/HeroContext";
import React from "react";

function ArrowButton(props) {
  const { onClick, style, buttonText } = props;
  return (
    <a className={style} onClick={() => onClick()}>
      {buttonText}
    </a>
  );
}


function onClickNext(activeIndex, itemsCount, setActiveIndex) {
  setActiveIndex((activeIndex + 1) % itemsCount);
}

function onClickPrev(activeIndex, itemsCount, setActiveIndex) {
  const nextIndex = activeIndex < 1 ? itemsCount - 1 : activeIndex - 1;
  setActiveIndex(nextIndex);
}

export function ArrowButtons(props) {
    let {activeIndex, onClick, count} = props;

    return (
        <>
            <ArrowButton style={"prev"} buttonText={"\u276E"} onClick={() => onClickPrev(activeIndex, count, onClick)}/>
            <ArrowButton style={"next"} buttonText={"\u276F"} onClick={() => onClickNext(activeIndex, count, onClick)}/>
        </>
    );
}



export function Button(props) {
  const { style, buttonText } = props;
  return <button className={style}>{buttonText}</button>;
}
