import React, { useState, useEffect } from "react";
import "./style.css";
import Dots from "./Dots";
import HeroItems from "./HeroItems";
import {ArrowButtons} from "./Buttons";
import {HeroContext} from "../contexts/HeroContext";

export function Carousel() {
    const [activeIndex, setActiveIndex] = useState(0);
    const [pauseCarousel, setPauseCarousel] = useState(false);

    const context = React.useContext(HeroContext);
    const itemsCount = context.data.length

    function onClickNext() {
        setActiveIndex((activeIndex + 1) % itemsCount);
    }

    function handlePauseToggle() {
        setPauseCarousel(!pauseCarousel);
    }

    useEffect(() => {
        let counter = 0;
        if (!pauseCarousel) {
            const interval = setInterval(() => {
                counter++;
                if (counter === 3) {
                    onClickNext();
                }
            }, 1000);
            return () => clearInterval(interval);
        }
    });

  return (
    <div
      className="container"
      onMouseEnter={() => handlePauseToggle()}
      onMouseLeave={() => handlePauseToggle()}
      onTouchStart={() => handlePauseToggle()}
      onTouchEnd={() => handlePauseToggle()}
    >
      <div>
        <HeroItems activeIndex={activeIndex} />
        <ArrowButtons activeIndex={activeIndex} onClick={(index) => { setActiveIndex(index); }} count={itemsCount}/>
        <Dots activeIndex={activeIndex} onClick={(index) => { setActiveIndex(index); }} count={itemsCount} />
      </div>
    </div>
  );
}
