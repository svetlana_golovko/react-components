import {HeroContext} from "../contexts/HeroContext";
import React from "react";

export default function Dots(props) {
  let { activeIndex, onClick, count} = props;
  return (<div className="indicator">{createDots(activeIndex, count, onClick)}</div>);
}

function createDots(activeIndex, count, onClick) {
  const FILLER = new Array(count).fill(1);
  return FILLER.map((_, index) => (
      <Indicator
          isActive={activeIndex === index}
          onClick={() => onClick(index)}
          key = {index}
      />
  ));
}

export function Indicator(props) {
  const { isActive, onClick} = props;
  return (
    <div onClick={onClick} className={isActive ? "dot dot-active" : "dot"} />
  );
}
