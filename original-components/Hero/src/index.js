import Hero from './HeroComponent/Hero';
import { createRoot } from 'react-dom/client';


const container = document.getElementById('root');
const data=[{"img": "img/earth.jpg", "textHeader": `header with <colored>colored text<colored>`, "textContext": "Context 1"},
            {"img": "img/wind.jpg",  "textHeader": `header with <colored>two</colored> colored <colored>tekst</colored>`, "textContext": "Context 2"},
            {"img": "img/water.jpg", "textHeader": `header 3`, "textContext": "Context 3"},
            {"img": "img/water.jpg", "textHeader": `header 4`, "textContext": "Context 4", "colorScheme":"shuttle-1"}]

const root = createRoot(container);
root.render( <Hero isFullBleed={true} colorScheme={"cruise-1"} data={data}/>);


