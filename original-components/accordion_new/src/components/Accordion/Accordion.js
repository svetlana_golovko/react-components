import React from 'react';
import {ThemeProvider} from "styled-components";
import {AccordionItem} from "./AccordionItem";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faCoffee, faStar} from "@fortawesome/free-solid-svg-icons";
import {styleLargeDark, styleLargeLight, styleMediumLight, styleSmallDark, styleSmallLight} from "../../styles/themes";

library.add(faCoffee, faStar);

const getScheme = (props) => {
    const {colorScheme, size, padding} = props;

    const styleSwitch = (scheme, size) => {
        switch (true) {
            case size === "small" && scheme === "dark":
                return styleSmallDark;
            case  size === "medium" && scheme === "dark":
                return styleSmallDark;
            case  size === "large" && scheme === "dark":
                return styleLargeDark;
            case size === "small" && scheme === "light":
                return styleSmallLight;
            case  size === "medium" && scheme === "light":
                return styleMediumLight;
            case size === "large" && scheme === "light":
                return styleLargeLight;
            default:
                return styleSmallLight;
        }
    };
    let selectedTheme = styleSwitch(colorScheme, size);
    let schemeObject = Object.assign({}, selectedTheme);
    schemeObject.padding = padding;
    return schemeObject;
}

export function Accordion(props) {
    const selectedTheme = getScheme(props)
    let children = props.children
    return (
        <ThemeProvider theme={selectedTheme}>
            {children}
        </ThemeProvider>
    );
}



export function AccordionOld(props) {
    const selectedTheme = getScheme(props)
    let children = props.children
    return (
        <ThemeProvider theme={selectedTheme}>
            {children.map((child, index) => {
                    const {header, count, icon} = child.props;
                    return (
                        <AccordionItem
                            key={index}
                            icon={icon}
                            header={header}
                            count={count}
                            content={child.props.children}
                        ></AccordionItem>
                    );
                }
            )}
        </ThemeProvider>
    );
}
