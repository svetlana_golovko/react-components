import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
library.add(faArrowDown);

const InlineAccordionTextContainer = styled.div`
  transition: opacity 0.35s;
  overflow: hidden;
`;
const InlineAccordionText = styled.div`
  padding-top: 16px;
  padding-bottom: 16px;
`;
const InlineAccordionAnchor = styled.div`
  color: #009d85;
  cursor: pointer;
  font-weight: bold;
  width: max-content;
  padding-top: 16px;
`;
const StyledFontAwesomeIcon = styled(FontAwesomeIcon)`
  margin-left: 5px;
  transition: all 0.25s;
`;
const InlineAccordion = styled.div`
  ${InlineAccordionTextContainer} {
    max-height: ${(props) => (props.visibleInlineAccordionItem ? "100%" : "0")};
    opacity: ${(props) => (props.visibleInlineAccordionItem ? "1" : "0")};
  }
  ${StyledFontAwesomeIcon} {
    transform: ${(props) =>
      props.visibleInlineAccordionItem ? "rotateX(180deg)" : "0"};
  }
`;

export function InlineAccordionItem(props) {
  const { header, text } = props;
  const [visibleInlineAccordionItem, setVisibleInlineAccordionItem] =
    useState(false);

  const handleInlineAccordionToggle = () => {
    setVisibleInlineAccordionItem(!visibleInlineAccordionItem);
  };

  return (
    <InlineAccordion visibleInlineAccordionItem={visibleInlineAccordionItem}>
      <InlineAccordionAnchor onClick={handleInlineAccordionToggle}>
        {header}
        <StyledFontAwesomeIcon icon="fa-solid fa-arrow-down" />
      </InlineAccordionAnchor>
      <InlineAccordionTextContainer>
        <InlineAccordionText>{text}</InlineAccordionText>
      </InlineAccordionTextContainer>
    </InlineAccordion>
  );
}
