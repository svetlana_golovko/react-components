import React from "react";
import ReactDOM from "react-dom/client";
import { createGlobalStyle } from "styled-components";

import {Accordion} from "./components/Accordion/Accordion";
import {InlineAccordionItem} from "./components/InlineAccordion/InlineAccordion";
import {AccordionItem} from "./components/Accordion/AccordionItem";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  body {
    margin: 0;
  }
`;

let getAccordionItems = (size,  colorScheme, padding, icon,  count) => {
    return (
        <Accordion colorScheme={colorScheme} size={size} padding={padding}>
            <AccordionItem icon={icon} header="Etiam cursus, risus non rutrum fringilla" count={count}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam mollis, libero et eleifend gravida, mauris diam hendrerit ipsum, sit amet vestibulum ipsum ante quis eros. Etiam cursus, risus non rutrum fringilla, diam sem placerat ex, vel tincidunt urna justo eu odio. Vivamus imperdiet felis in pharetra rhoncus.
            </AccordionItem>
            <AccordionItem icon={icon} header="Header 2" >
                Content 2
            </AccordionItem>
            <AccordionItem header="Header 3" count={count}>
                Content 3
            </AccordionItem>
        </Accordion>
    );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <GlobalStyle />
        <p><h1>Accordion light </h1></p>
        <p><h2>Small</h2></p>
        <table >
            <tr>
                <th>Without padding</th>
                <th>With padding</th>
            </tr>
            <tr>
                <td> {getAccordionItems("small", "light", "false", "star", "23")} </td>
                <td> {getAccordionItems("small", "light", "true", "star", "2")} </td>
            </tr>
        </table>


        <p><h2>Medium </h2></p>
        <table >
            <tr>
                <th>Without padding</th>
                <th>With padding</th>
            </tr>

            <tr>
                <td> {getAccordionItems("medium", "light", "false", "star", "2")} </td>
                <td> {getAccordionItems("medium", "light", "true", "star", "2")} </td>
            </tr>
        </table>


        <p><h2>Large </h2></p>
        <table >
            <tr>
                <th>Without padding</th>
                <th>With padding</th>
            </tr>
            <tr>
                <td> {getAccordionItems("large", "light", "false", "star", "2")} </td>
                <td> {getAccordionItems("large", "light", "true", "star", "2")} </td>
            </tr>
        </table>

        <p> <h1>Inline Accordion</h1> </p>
        <InlineAccordionItem
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam felis mauris, convallis a augue rhoncus, molestie euismod velit. Ut sem tortor, condimentum quis scelerisque a, interdum sed velit. Aliquam bibendum tellus et posuere sollicitudin. Praesent laoreet vitae enim nec elementum"
            header="Lorem ipsum"
        />
        <InlineAccordionItem
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam felis mauris, convallis a augue rhoncus, molestie euismod velit. Ut sem tortor, condimentum quis scelerisque a, interdum sed velit. Aliquam bibendum tellus et posuere sollicitudin. Praesent laoreet vitae enim nec elementum"
            header="teine"
        />
    </React.StrictMode>
);

