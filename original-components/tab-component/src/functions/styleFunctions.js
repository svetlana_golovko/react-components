import {css} from "styled-components";

export const setPaddingSize = (size) => {
    switch (size) {
        case "small":
            return "16px";
        case "medium":
            return "16px";
        case "large":
            return "24px";
        default:
            return "0";
    }
};
export const setIconSize = (size) => {
    switch (size) {
        case "small":
            return css`
        width: 22px;
        height: 22px;
      `;
        case "medium":
            return css`
        width: 24px;
        height: 24px;
      `;
        case "large":
            return css`
        width: 30px;
        height: 30px;
      `;
        default:
            return "0";
    }
};
export const setCountSize = (size) => {
    switch (size) {
        case "small":
            return css`
        min-width: 24px;
        height: 22px;
      `;
        case "medium":
            return css`
        min-width: 30px;
        height: 26px;
      `;
        case "large":
            return css`
        min-width: 30px;
        height: 26px;
      `;
        default:
            return css`
        min-width: 24px;
        height: 22px;
      `;
    }
};
export const setFontSize = (size) => {
    switch (size) {
        case "small":
            return "16px";
        case "medium":
            return "20px";
        case "large":
            return "24px";
        default:
            return "30px";
    }
};

