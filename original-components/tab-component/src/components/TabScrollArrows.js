import styled from "styled-components";
import React, { useEffect, useState } from "react";

export const setFontSize = (size) => {
  switch (size) {
    case "small":
      return "14px";
    case "medium":
      return "16px";
    case "large":
      return "24px";
    default:
      return "14px";
  }
};
const setTabHeight = (size) => {
  switch (size) {
    case "small":
      return "30px";
    case "medium":
      return "35px";
    case "large":
      return "40px";
    default:
      return "30px";
  }
};

const ScrollArrow = styled.a`
  cursor: pointer;
  position: absolute;
  background-color: white;
  font-weight: bold;
  width: 32px;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: translate(0, -5px);
  font-size: ${(props) => setFontSize(props.size)};
  height: calc(${(props) => setTabHeight(props.size)} + 10px);
`;

const LeftScrollArrow = styled(ScrollArrow)`
  border-radius: 0 3px 3px 0;
  border-right: 1px solid #d6dfdf;
  left: 0;
`;
const RightScrollArrow = styled(ScrollArrow)`
  right: 0;
  border-radius: 3px 0 0 3px;
  border-left: 1px solid #d6dfdf;
`;

export function TabScrollArrows(props) {
  const { tabsRef, size } = props;
  const [scrollPosition, setScrollPosition] = useState(0);
  const [showLeftArrow, setShowLeftArrow] = useState(false);
  const [showRightArrow, setShowRightArrow] = useState(false);

  useEffect(() => {
    const handleWindowSizeChange = () => {
      const offset = 2;
      setScrollPosition(Math.round(tabsRef.current.scrollLeft));
      setShowLeftArrow(scrollPosition !== 0);
      setShowRightArrow(
        Math.round(scrollPosition + tabsRef.current.offsetWidth + offset) <
          tabsRef.current.scrollWidth
      );
    };
    if (tabsRef.current) {
      window.addEventListener("resize", handleWindowSizeChange);
      handleWindowSizeChange();
      return () => {
        window.removeEventListener("resize", handleWindowSizeChange);
      };
    }
  });

  const handleScrollClick = (scrollOffset) => {
    const scrollLength = 50;
    tabsRef.current.scrollBy({
      left: scrollLength * (scrollOffset ? 1 : -1),
    });
    setScrollPosition(tabsRef.current.scrollLeft);
  };

  return (
    <>
      {showLeftArrow && (
        <LeftScrollArrow onClick={() => handleScrollClick(false)} size={size}>
          {"\u276E"}
        </LeftScrollArrow>
      )}
      {showRightArrow && (
        <RightScrollArrow onClick={() => handleScrollClick(true)} size={size}>
          {"\u276F"}
        </RightScrollArrow>
      )}
    </>
  );
}
