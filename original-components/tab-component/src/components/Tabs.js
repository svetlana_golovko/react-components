import React from 'react';
import {Tab} from './Tab';
import styled, {ThemeProvider} from "styled-components";
import {useState, useEffect} from "react";

const lightTheme = {

    activeTextColor: "#004152",
    inActiveTextColor: "#004152",
    activeBackground: "#ffffff",
    inActiveBackground: "#eef1f1",
    activeTabCountColor: "#00bea2",
    inactiveTabCountColor: "#d6dfdf",
    activeTabCountTextColor: "#ffffff",
    inactiveTabCountTextColor: "#004152",

    background: "#eef1f1",
    separatorColor: "#b2bdbd",
};

const darkTheme = {
    color: "#f5f8f7",
    background: "#004152",
    separatorColor: "#396a76",
    activeTabCountColor: "#00bea2",
    inactiveTabCountColor: "#396a76",
};

const getScheme = (props) => {
    const {colorScheme, size, padding, tabStyle} = props;
    let selectedTheme = (colorScheme === "light") ? lightTheme : darkTheme;
    selectedTheme.size = size;
    selectedTheme.isLineStyle = (tabStyle === "line");
    selectedTheme.padding = padding;

    selectedTheme.background = selectedTheme.isLineStyle ? selectedTheme.activeBackground : selectedTheme.background;
    selectedTheme.activeTextColor =  (selectedTheme.isLineStyle) ? "#00bea2" : selectedTheme.activeTextColor;
    selectedTheme.border =  selectedTheme.isLineStyle ? ""  :`1px solid ${selectedTheme.separatorColor}`;

    selectedTheme.activeBorderBottom = selectedTheme.isLineStyle ? `3px solid ${selectedTheme.activeTabCountColor}` : `3px solid ${selectedTheme.activeBackground}`;
    selectedTheme.inActiveBorderBottom = `1px solid ${( selectedTheme.isLineStyle ? selectedTheme.separatorColor : selectedTheme.separatorColor)}`;

    return selectedTheme;
}
const TabsStyled = styled.div`
  display: flex;
  align-items: stretch;
`;

export function Tabs(props) {
    const selectedTheme = getScheme(props)
    let children = props.children
    const [activeTab, setActiveTab] = useState(0);

    const onClickTabItem = (tab) => {
        setActiveTab(tab)
    }
    useEffect(() => {
        setActiveTab(children[0].props.header);
    }, []);

    return (
        <ThemeProvider theme={selectedTheme}>
            <TabsStyled >
            {children.map((child) => {
                    const { header, count, icon} = child.props;
                    return (
                        <Tab
                            isActive={activeTab === header}
                            key={header}
                            onClick={onClickTabItem}
                            header={header}
                            count = {count}
                            icon = {icon}
                        />
                    );
                }
            )}
            </TabsStyled>
            <div className="tab-content">
                {children.map((child) => {
                    if (child.props.header !== activeTab) return undefined;
                    return child.props.children;
                })}
            </div>
        </ThemeProvider>
    );
}