import React from "react";
import styled, {ThemeContext} from "styled-components";
import {useContext} from 'react'

import { setPaddingSize, setFontSize, setCountSize } from "../functions/styleFunctions";

const TabHeaderStyled = styled.div`
  display: flex;
  align-items: center;
  align-content: center;
  gap: 12px;
  padding: 9px  ${(props) => setPaddingSize(props.theme.size)} 9px ; 
  font-size: ${(props) => setFontSize(props.theme.size)};
  cursor: pointer;
  color:  ${(props) => (props.isActive ? props.theme.activeTextColor: props.theme.inActiveTextColor)};

`;
const CountStyled = styled.div`
  border-radius: 4px;
  margin-right: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1px 4px;
  background-color: ${(props) => (props.isActive ? props.theme.activeTabCountColor: props.theme.inactiveTabCountColor)}; 
  font-size: ${(props) => setFontSize(props.theme.size)};
  color: ${(props) => (props.isActive ? props.theme.activeTabCountTextColor: props.theme.inactiveTabCountTextColor)};
  ${(props) => setCountSize(props.theme.size)};
`;

export default function TabHeader(props) {
    const {icon, header, count, isActive} = props
    console.log(isActive)
    const themeContext = useContext(ThemeContext)
    return (
        <TabHeaderStyled isActive={isActive}>
            {themeContext.isLineStyle && icon && <img src={icon} alt="Snow"></img>}
            {header}
            {count && <CountStyled isActive={isActive}>{count}</CountStyled>}
        </TabHeaderStyled>
    );
}