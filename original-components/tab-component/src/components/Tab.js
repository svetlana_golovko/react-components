import React from 'react';
import TabHeader from "./TabHeader";
import styled from "styled-components";

const TabStyled = styled.div`
  display: flex;
  align-items: stretch;
  align-content: center;
  background-color: ${(props) => (props.isActive ? props.theme.activeBackground :  props.theme.inActiveBackground)}; 
  border:  ${(props) => props.theme.border };
  border-bottom:  ${(props) => (props.isActive ? props.theme.activeBorderBottom : props.theme.inActiveBorderBottom )};
  list-style: none;
  margin-bottom: -1px;
`;

export function Tab(props){
    const {onClick, header, count, icon, isActive} = props;
    let iconSrc = null
    if ( icon ) {
        iconSrc = 'notactive\\'+icon
        if (isActive) {
            iconSrc = 'active\\'+icon;
        }
    }

    const onClickTab = () => {
        onClick(header);
    }

    return (
        <TabStyled onClick={onClickTab} isActive={isActive}>
            <TabHeader header={header} count={count} icon={iconSrc} isActive={isActive} />
        </TabStyled>
    );
}