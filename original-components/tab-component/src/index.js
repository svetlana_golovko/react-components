import React from 'react';
import ReactDOM from 'react-dom/client';
import {Tabs} from "./components/Tabs";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <div>
          <h1>Tabs Demo</h1>
          <Tabs colorScheme="light" size="small" tabStyle="line">
              <div header="Rooms" count="23" icon="weather.png">
                  See ya later, <em>Alligator</em>!
              </div>
              <div header="Restaurant & Bar" count="55">
                  After 'while, <em>Crocodile</em>!
              </div>
              <div header="Spa & Fitness">
                  Nothing to see here, this tab is <em>extinct</em>!
              </div>
          </Tabs>
      </div>
  </React.StrictMode>
);

