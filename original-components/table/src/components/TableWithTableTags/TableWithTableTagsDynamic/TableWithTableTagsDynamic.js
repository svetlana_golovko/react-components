import { useEffect, useRef, useState } from "react";
import parse, { attributesToProps, domToReact } from "html-react-parser";
import "../TableWithTableTags.scss";

export function TableWithTableTags(props) {
  const { vertical, body } = props;
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  function parseTable(table) {
    const getTransposedTable = (tableBody) => {
      let reversedTableBody = [];
      let tableElementsByRows = tableBody
        .filter((x) => x.attribs)
        .map((x) => x.children)
        .flat()
        .filter((y) => y.attribs)
        .map((x) => x.children);

      let tableExampleRow = tableBody
        .filter((x) => x.attribs)
        .map((x) => x.children)
        .flat()
        .filter((y) => y.attribs)[0];

      for (let rowNr = 0; rowNr < tableElementsByRows.length; rowNr++) {
        for (
          let columnNr = 0;
          columnNr < tableElementsByRows[rowNr].length;
          columnNr++
        ) {
          let rcolumnNr = columnNr;
          if (tableElementsByRows[rowNr][columnNr].attribs) {
            if (tableElementsByRows[rowNr][columnNr].prev.prev) {
              let colspan =
                tableElementsByRows[rowNr][columnNr].prev.prev.attribs.colspan;
              if (colspan) {
                rcolumnNr = columnNr + 2 ** (colspan - 1);
              }
            }
            if (!reversedTableBody[rcolumnNr]) {
              reversedTableBody[rcolumnNr] = [];
            }
            reversedTableBody[rcolumnNr].push(
              tableElementsByRows[rowNr][columnNr]
            );
          }
        }
      }
      reversedTableBody = reversedTableBody
        .filter((x) => x)
        .map((it) => Object.assign({}, tableExampleRow, { children: it }));
      return reversedTableBody;
    };

    const convertToStyled = {
      replace: (domNode) => {
        if (domNode.attribs && domNode.name === `tr`) {
          return <tr>{domToReact(domNode.children, convertToStyled)}</tr>;
        }
        if (domNode.attribs && domNode.name === `table`) {
          return <table>{domToReact(domNode.children, convertToStyled)}</table>;
        }
        if (domNode.attribs && domNode.name === `th`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <th {...props}>{domToReact(domNode.children, convertToStyled)}</th>
          );
        }
        if (domNode.attribs && domNode.name === `td`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <td {...props}>{domToReact(domNode.children, convertToStyled)}</td>
          );
        }
      },
    };
    const transposeAndConvertToStyled = {
      transposedTable: [],
      replace: (domNode) => {
        if (domNode.attribs && domNode.name === `table`) {
          transposeAndConvertToStyled.transposedTable = getTransposedTable(
            domNode.children
          );
          return (
            <table>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </table>
          );
        }
        if (domNode.attribs && domNode.name === `tbody`) {
          return (
            <tbody>
              {domToReact(
                transposeAndConvertToStyled.transposedTable,
                transposeAndConvertToStyled
              )}
            </tbody>
          );
        }
        if (domNode.attribs && domNode.name === `thead`) {
          return <></>;
        }
        if (domNode.attribs && domNode.name === `tr`) {
          return (
            <tr>{domToReact(domNode.children, transposeAndConvertToStyled)}</tr>
          );
        }
        if (domNode.attribs && domNode.name === `th`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <th {...props} vertical={vertical}>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </th>
          );
        }
        if (domNode.attribs && domNode.name === `td`) {
          const props = attributesToProps(domNode.attribs);
          if (props.colSpan) {
            props.rowSpan = props.colSpan;
            delete props.colSpan;
          }
          return (
            <td {...props} vertical={vertical}>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </td>
          );
        }
      },
    };

    let options = vertical ? transposeAndConvertToStyled : convertToStyled;
    return parse(table, options);
  }

  return (
    <div className="scroll-wrapper" marginWidth={marginWidth} ref={wrapperRef}>
      {parseTable(body)}
    </div>
  );
}
