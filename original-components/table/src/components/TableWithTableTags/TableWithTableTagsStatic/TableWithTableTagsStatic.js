import { useEffect, useRef, useState } from "react";
import "../TableWithTableTags.scss";

export function TableWithTableTags1Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 1 column</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags2Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 2 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 3 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum dolor sit amet</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 4 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 6 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 6+ columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td colSpan={4}>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function TableWithTableTags2ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 2 columns with colspan 2</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags3ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 3 columns with colspan 2</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags4ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 4 columns with colspan 2 and 3</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td colSpan={3}>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags6ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 6 columns with colspan 3 and 4</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Lorem ipsum</td>
            <td colSpan={3}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td colSpan={4}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function TableWithTableTags6PlusColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="scroll-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Table with table tags - 6+ columns with colspan 2 and 4</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>L</td>
            <td>Lorem ipsum</td>
            <td colSpan={4}>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function TableWithTableTagsExamples() {
  return (
    <>
      <TableWithTableTags1Cols />
      <TableWithTableTags2Cols />
      <TableWithTableTags3Cols />
      <TableWithTableTags4Cols />
      <TableWithTableTags6Cols />
      <TableWithTableTags6PlusCols />
    </>
  );
}
export function TableWithTableTagsAndSpanExamples() {
  return (
    <>
      <TableWithTableTags2ColsWithSpan />
      <TableWithTableTags3ColsWithSpan />
      <TableWithTableTags4ColsWithSpan />
      <TableWithTableTags6ColsWithSpan />
      <TableWithTableTags6PlusColsWithSpan />
    </>
  );
}

