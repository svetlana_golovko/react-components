import { useEffect, useRef, useState } from "react";
import styled, { css } from "styled-components";
import parse, { attributesToProps, domToReact } from "html-react-parser";

function setColWidthByColCount(colCount) {
  let styleResult = ``;
  for (let i = 1; i <= colCount; i++) {
    styleResult += `&:first-child:nth-last-child(${colCount}),
    &:first-child:nth-last-child(${colCount}) ~ * {
      width: calc(100vw / ${i});
    }`;
  }
  return css`
    ${styleResult}
  `;
}

const Row = styled.tr``;
const TD = styled.td`
  background-image: linear-gradient(
    to right,
    black 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: bottom;
  background-size: 5px 1px;
  background-repeat: repeat-x;
  height: 45px;
  text-align: left;
  vertical-align: top;
  padding: 0 8px;
  min-width: calc(100vw - var(--margin-width));
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
`;
const TH = styled(TD)`
  &&&&:first-child {
    ${(props) =>
      props.vertical &&
      css`
        width: 1% ;
        min-width: 0 ;
        white-space: nowrap ;
      `}
  }
  font-weight: bold;
`;
const Table = styled.table`
  min-width: 100%;
  border-collapse: collapse;
  margin-bottom: 15px;
  }
`;
const Wrapper = styled.div`
  overflow-x: auto;
  width: calc(100% - var(--margin-width));
  margin-left: var(--margin-size);
  --margin-size: 16px;
  --margin-width: ${(props) =>
    props.marginWidth ? props.marginWidth : "var(--margin-size) * 2"};
  @media screen and (min-width: 360px) {
    ${TD}, ${TH} {
      ${setColWidthByColCount(2)}
      min-width: 45vw;
    }
    --margin-size: 16px;
  }
  @media screen and (min-width: 600px) {
    ${TD}, ${TH} {
      ${setColWidthByColCount(3)}
      min-width: 30vw;
    }
    --margin-size: 20px;
  }
  @media screen and (min-width: 800px) {
    ${TD}, ${TH} {
      ${setColWidthByColCount(3)}
      min-width: 30vw;
      padding: 0 12px;
      &:first-child {
        padding-left: 0;
      }
      &:last-child {
        padding-right: 0;
      }
    }
    --margin-size: 28px;
  }
  @media screen and (min-width: 1024px) {
    ${TD}, ${TH} {
      ${setColWidthByColCount(4)}
      min-width: 23vw;
    }
    --margin-size: 32px;
  }
  @media screen and (min-width: 1288px) {
    ${TD}, ${TH} {
      ${setColWidthByColCount(6)}
      min-width: 15vw;
    }
  }
`;

const setFirstColumnStyle = (hug) => {
  if (!hug) {
    return css`
      @media screen and (min-width: 360px) {
        min-width: calc(100vw / 4 * 3);
      }
      @media screen and (min-width: 600px) {
        min-width: calc(100vw / 6 * 3);
      }
      @media screen and (min-width: 800px) {
        min-width: calc(100vw / 6 * 3);
      }
      @media screen and (min-width: 1024px) {
        min-width: calc(100vw / 12 * 3);
      }
      @media screen and (min-width: 1288px) {
        min-width: calc(100vw / 12 * 3);
      }
    `;
  } else {
    return css`
      width: 1%;
      min-width: 0;
      white-space: nowrap;
    `;
  }
};

const TableOffgrid = styled(Table)`
  &&& {
    min-width: 100%;
    border-collapse: collapse;
    margin-bottom: 15px;
    tr {
      background-image: linear-gradient(
        to right,
        black 33%,
        rgba(255, 255, 255, 0) 0%
      );
      background-position: bottom;
      background-size: 5px 1px;
      background-repeat: repeat-x;
      th,
      td {
        height: 45px;
        text-align: left;
        vertical-align: top;
        padding: 0 8px;
        word-wrap: normal;
        width: 1%;
        min-width: 0;
        white-space: nowrap;
        &:first-child {
          ${(props) => setFirstColumnStyle(props.hug)}
          padding-left: 0;
          font-weight: bold;
        }
        &:last-child {
          padding-right: 0;
        }
      }
      th {
        font-weight: bold;
      }
    }
  }
`;

export function TableSC(props) {
  const { vertical, body, offgrid, hugOffgridContent } = props;
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  function parseTable(table) {
    const getTransposedTable = (tableBody) => {
      let reversedTableBody = [];
      let tableElementsByRows = tableBody
        .filter((x) => x.attribs)
        .map((x) => x.children)
        .flat()
        .filter((y) => y.attribs)
        .map((x) => x.children);

      let tableExampleRow = tableBody
        .filter((x) => x.attribs)
        .map((x) => x.children)
        .flat()
        .filter((y) => y.attribs)[0];

      for (let rowNr = 0; rowNr < tableElementsByRows.length; rowNr++) {
        for (
          let columnNr = 0;
          columnNr < tableElementsByRows[rowNr].length;
          columnNr++
        ) {
          let rcolumnNr = columnNr;
          if (tableElementsByRows[rowNr][columnNr].attribs) {
            if (tableElementsByRows[rowNr][columnNr].prev.prev) {
              let colspan =
                tableElementsByRows[rowNr][columnNr].prev.prev.attribs.colspan;
              if (colspan) {
                rcolumnNr = columnNr + 2 ** (colspan - 1);
              }
            }
            if (!reversedTableBody[rcolumnNr]) {
              reversedTableBody[rcolumnNr] = [];
            }
            reversedTableBody[rcolumnNr].push(
              tableElementsByRows[rowNr][columnNr]
            );
          }
        }
      }
      reversedTableBody = reversedTableBody
        .filter((x) => x)
        .map((it) => Object.assign({}, tableExampleRow, { children: it }));
      return reversedTableBody;
    };

    const convertToStyled = {
      replace: (domNode) => {
        if (domNode.attribs && domNode.name === `tr`) {
          return <Row>{domToReact(domNode.children, convertToStyled)}</Row>;
        }
        if (domNode.attribs && domNode.name === `table`) {
          return <Table>{domToReact(domNode.children, convertToStyled)}</Table>;
        }
        if (domNode.attribs && domNode.name === `th`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <TH {...props}>{domToReact(domNode.children, convertToStyled)}</TH>
          );
        }
        if (domNode.attribs && domNode.name === `td`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <TD {...props}>{domToReact(domNode.children, convertToStyled)}</TD>
          );
        }
      },
    };
    const transposeAndConvertToStyled = {
      transposedTable: [],
      replace: (domNode) => {
        if (domNode.attribs && domNode.name === `table`) {
          transposeAndConvertToStyled.transposedTable = getTransposedTable(
            domNode.children
          );
          return (
            <Table>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </Table>
          );
        }
        if (domNode.attribs && domNode.name === `tbody`) {
          return (
            <tbody>
              {domToReact(
                transposeAndConvertToStyled.transposedTable,
                transposeAndConvertToStyled
              )}
            </tbody>
          );
        }
        if (domNode.attribs && domNode.name === `thead`) {
          return <></>;
        }
        if (domNode.attribs && domNode.name === `tr`) {
          return (
            <Row>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </Row>
          );
        }
        if (domNode.attribs && domNode.name === `th`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <TH {...props} vertical={vertical}>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </TH>
          );
        }
        if (domNode.attribs && domNode.name === `td`) {
          const props = attributesToProps(domNode.attribs);
          if (props.colSpan) {
            props.rowSpan = props.colSpan;
            delete props.colSpan;
          }
          return (
            <TD {...props} vertical={vertical}>
              {domToReact(domNode.children, transposeAndConvertToStyled)}
            </TD>
          );
        }
      },
    };
    const convertToOffgridStyled = {
      replace: (domNode) => {
        if (domNode.attribs && domNode.name === `tr`) {
          return (
            <Row>{domToReact(domNode.children, convertToOffgridStyled)}</Row>
          );
        }
        if (domNode.attribs && domNode.name === `table`) {
          return (
            <TableOffgrid hug={hugOffgridContent}>
              {domToReact(domNode.children, convertToOffgridStyled)}
            </TableOffgrid>
          );
        }
        if (domNode.attribs && domNode.name === `th`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <th {...props}>
              {domToReact(domNode.children, convertToOffgridStyled)}
            </th>
          );
        }
        if (domNode.attribs && domNode.name === `td`) {
          const props = attributesToProps(domNode.attribs);
          return (
            <td {...props}>
              {domToReact(domNode.children, convertToOffgridStyled)}
            </td>
          );
        }
      },
    };

    let options = offgrid
      ? convertToOffgridStyled
      : vertical
      ? transposeAndConvertToStyled
      : convertToStyled;
    return parse(table, options);
  }

  return (
    <Wrapper marginWidth={marginWidth} ref={wrapperRef}>
      {parseTable(body)}
    </Wrapper>
  );
}
