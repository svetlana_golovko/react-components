import styled, { css } from "styled-components";

const setFirstColumnStyle = (hug) => {
  if (!hug) {
    return css`
      @media screen and (min-width: 360px) {
        min-width: calc(100vw / 4 * 3);
      }
      @media screen and (min-width: 600px) {
        min-width: calc(100vw / 6 * 3);
      }
      @media screen and (min-width: 800px) {
        min-width: calc(100vw / 6 * 3);
      }
      @media screen and (min-width: 1024px) {
        min-width: calc(100vw / 12 * 3);
      }
      @media screen and (min-width: 1288px) {
        min-width: calc(100vw / 12 * 3);
      }
    `;
  } else {
    return css`
      width: 1%;
      min-width: 0;
      white-space: nowrap;
    `;
  }
};

const Table = styled.div`
&&&{
  min-width: 100%;
  border-collapse: collapse;
  margin-bottom: 15px;
  tr {
    background-image: linear-gradient(
      to right,
      black 33%,
      rgba(255, 255, 255, 0) 0%
    );
    background-position: bottom;
    background-size: 5px 1px;
    background-repeat: repeat-x;
    th,
    td {
      height: 45px;
      text-align: left;
      vertical-align: top;
      padding: 0 8px;
      word-wrap: normal;
      width: 1% ;
      min-width: 0 ;
      white-space: nowrap;
      &:first-child {
        ${(props) => setFirstColumnStyle(props.hug)}
        padding-left: 0;
      }
      &:last-child {
        padding-right: 0;
      }
    }
    th {
      font-weight: bold;
    }
  }}
`;

export function TableWithTableTagsOffgridSC(props) {
  const { hug } = props;
  return <Table hug={hug}>{props.children}</Table>;
}
