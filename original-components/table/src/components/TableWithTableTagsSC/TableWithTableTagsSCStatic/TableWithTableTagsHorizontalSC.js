import { useEffect, useRef, useState } from "react";
import styled, { css } from "styled-components";

function setColWidthByColCount(colCount) {
  let styleResult = ``;
  for (let i = 1; i <= colCount; i++) {
    styleResult += `&:first-child:nth-last-child(${colCount}),
    &:first-child:nth-last-child(${colCount}) ~ * {
      max-width: calc(100vw / ${i});
    }`;
  }
  return css`
    ${styleResult}
  `;
}

const Wrapper = styled.div`
  overflow-x: auto;
  width: calc(100% - var(--margin-width));
  margin-left: var(--margin-size);
  --margin-size: 16px;
  --margin-width: ${(props) =>
    props.marginWidth ? props.marginWidth : "var(--margin-size) * 2"};
`;
const Row = styled.tr`
  background-image: linear-gradient(
    to right,
    black 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: bottom;
  background-size: 5px 1px;
  background-repeat: repeat-x;
`;
const TD = styled.td`
  height: 45px;
  text-align: left;
  vertical-align: top;
  padding: 0 8px;
  min-width: calc(100vw - var(--margin-width));
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
`;
const TH = styled(TD)`
  font-weight: bold;
`;
const Table = styled.table`
  min-width: 100%;
  border-collapse: collapse;
  margin-bottom: 15px;
    @media screen and (min-width: 360px) {
      ${TD}, ${TH} {
        ${setColWidthByColCount(2)}
        min-width: 45vw;
      }
    }
    @media screen and (min-width: 600px) {
      ${TD}, ${TH} {
        ${setColWidthByColCount(3)}
        min-width: 30vw;
      }
      --margin-size: 20px;
    }
    @media screen and (min-width: 800px) {
      ${TD}, ${TH} {
        ${setColWidthByColCount(3)}
        min-width: 30vw;
        padding: 0 12px;
      }
      --margin-size: 28px;
    }
    @media screen and (min-width: 1024px) {
      ${TD}, ${TH} {
        ${setColWidthByColCount(4)}
        min-width: 23vw;
      }
      --margin-size: 32px;
    }
    @media screen and (min-width: 1288px) {
      ${TD}, ${TH} {
        ${setColWidthByColCount(6)}
        min-width: 15vw;
      }
    }
  }
`;

function TableWithTableTagsSC1Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 1 column</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
          </Row>
          <Row>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsSC2Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 2 columns</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsSC3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 3 columns</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum dolor sit amet</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsSC4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 4 columns</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsSC6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 6 columns</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsSC6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 6+ columns</h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD colSpan={4}>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}

function TableWithTableTagsWithSpanSC3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 3 columns with span
      </h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanSC4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 4 columns with span
      </h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanSC6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 6 columns with span
      </h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanSC6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 6+ columns with span
      </h2>
      <Table>
        <thead>
          <Row>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
            <TH>Label</TH>
          </Row>
        </thead>
        <tbody>
          <Row>
            <TD colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
            <TD>Lorem ipsum</TD>
          </Row>
          <Row>
            <TD>L</TD>
            <TD>Lorem ipsum</TD>
            <TD colSpan={4}>Lorem ipsum</TD>
            <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
            <TD>Lorem ipsum</TD>
          </Row>
        </tbody>
      </Table>
    </Wrapper>
  );
}

export function TableWithTableTagsHorizontalSCExample() {
  return (
    <>
      <TableWithTableTagsSC1Cols />
      <TableWithTableTagsSC2Cols />
      <TableWithTableTagsSC3Cols />
      <TableWithTableTagsSC4Cols />
      <TableWithTableTagsSC6Cols />
      <TableWithTableTagsSC6PlusCols />
    </>
  );
}
export function TableWithTableTagsWithSpanHorizontalSCExample() {
  return (
    <>
      <TableWithTableTagsWithSpanSC3Cols />
      <TableWithTableTagsWithSpanSC4Cols />
      <TableWithTableTagsWithSpanSC6Cols />
      <TableWithTableTagsWithSpanSC6PlusCols />
    </>
  );
}
