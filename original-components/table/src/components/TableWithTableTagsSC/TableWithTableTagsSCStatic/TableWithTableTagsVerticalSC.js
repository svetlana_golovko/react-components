import { useEffect, useRef, useState } from "react";
import styled, { css } from "styled-components";

function setColWidthByColCount(colCount) {
  let styleResult = ``;
  for (let i = 1; i <= colCount; i++) {
    styleResult += `&:first-child:nth-last-child(${colCount}),
    &:first-child:nth-last-child(${colCount}) ~ * {
      max-width: calc(90vw / ${i});
    }`;
  }
  return css`
    ${styleResult}
  `;
}

const Wrapper = styled.div`
  overflow-x: auto;
  width: calc(100% - var(--margin-width));
  margin-left: var(--margin-size);
  --margin-size: 16px;
  --margin-width: ${(props) =>
    props.marginWidth ? props.marginWidth : "var(--margin-size) * 2"};
`;
const Row = styled.tr`
  background-image: linear-gradient(
    to right,
    black 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: bottom;
  background-size: 5px 1px;
  background-repeat: repeat-x;
`;
const TD = styled.td`
  height: 45px;
  text-align: left;
  vertical-align: top;
  padding: 0 8px;
  min-width: calc(90vw - var(--margin-width));
  word-wrap: normal;
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
`;
const TH = styled(TD)`
  font-weight: bold;
`;
const Table = styled.table`
  min-width: 100%;
  border-collapse: collapse;
  margin-bottom: 15px;
    @media screen and (min-width: 360px) {
      ${TD}{
        ${setColWidthByColCount(3)}
        min-width: 40vw;
      }
      ${TH}{
        min-width: 10vw !important;
        width: 10vw !important;
    }
    }
    @media screen and (min-width: 600px) {
      ${TD}{
        ${setColWidthByColCount(4)}
        min-width: 25vw;
      }
      --margin-size: 20px;
    }
    @media screen and (min-width: 800px) {
      ${TD}{
        ${setColWidthByColCount(4)}
        min-width: 25vw;
        padding: 0 12px;
      }
      --margin-size: 28px;
    }
    @media screen and (min-width: 1024px) {
      ${TD}{
        ${setColWidthByColCount(5)}
        min-width: 20vw;
      }
      --margin-size: 32px;
    }
    @media screen and (min-width: 1288px) {
      ${TD} {
        ${setColWidthByColCount(7)}
        min-width: 13vw;
      }
    }
  }
`;

function TableWithTableTagsVerticalSC1Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 1 column</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsVerticalSC2Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 2 columns</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsVerticalSC3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 3 columns</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum dolor sit amet</TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>L</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsVerticalSC4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 4 columns</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsVerticalSC6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 6 columns</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsVerticalSC6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>Table with table tags using Styled Components - 6+ columns</h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}

function TableWithTableTagsWithSpanVerticalSC3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 3 columns with span
      </h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD colSpan={2}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor
          </TD>
          <TD>Lorem ipsum dolor sit amet</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanVerticalSC4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 4 columns with span
      </h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD colSpan={2}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>L</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanVerticalSC6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 6 columns with span
      </h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD colSpan={2}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>L</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}
function TableWithTableTagsWithSpanVerticalSC6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth) setMarginWidth("var(--margin-size)");
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <Wrapper ref={wrapperRef} marginWidth={marginWidth}>
      <h2>
        Table with table tags using Styled Components - 6+ columns with span
      </h2>
      <Table>
        <Row>
          <TH>Label</TH>
          <TD colSpan={2}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat
          </TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
          <TD>Lorem ipsum</TD>
        </Row>
        <Row>
          <TH>Label</TH>
          <TD>L</TD>
          <TD>Lorem ipsum</TD>
          <TD colSpan={4}>Lorem ipsum</TD>
          <TD>Lorem ipsum dolor sit amet, consectetur adipiscing elit</TD>
          <TD>Lorem ipsum</TD>
        </Row>
      </Table>
    </Wrapper>
  );
}

export function TableWithTableTagsVerticalSCExample() {
  return (
    <>
      <TableWithTableTagsVerticalSC1Cols />
      <TableWithTableTagsVerticalSC2Cols />
      <TableWithTableTagsVerticalSC3Cols />
      <TableWithTableTagsVerticalSC4Cols />
      <TableWithTableTagsVerticalSC6Cols />
      <TableWithTableTagsVerticalSC6PlusCols />
    </>
  );
}
export function TableWithTableTagsWithSpanVerticalSCExample() {
  return (
    <>
      <TableWithTableTagsWithSpanVerticalSC3Cols />
      <TableWithTableTagsWithSpanVerticalSC4Cols />
      <TableWithTableTagsWithSpanVerticalSC6Cols />
      <TableWithTableTagsWithSpanVerticalSC6PlusCols />
    </>
  );
}
