import {
  TableWithTableTagsHorizontalSCExample,
  TableWithTableTagsWithSpanHorizontalSCExample,
} from "./TableWithTableTagsHorizontalSC";
import {
  TableWithTableTagsVerticalSCExample,
  TableWithTableTagsWithSpanVerticalSCExample,
} from "./TableWithTableTagsVerticalSC";

export function TableWithTableTagsSC(props) {
  const { vertical } = props;
  return vertical ? (
    <TableWithTableTagsVerticalSCExample />
  ) : (
    <TableWithTableTagsHorizontalSCExample />
  );
}

export function TableWithTableTagsWithSpanSC(props) {
  const { vertical } = props;
  return vertical ? (
    <TableWithTableTagsWithSpanVerticalSCExample />
  ) : (
    <TableWithTableTagsWithSpanHorizontalSCExample />
  );
}
