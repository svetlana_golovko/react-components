import { useEffect, useRef, useState } from "react";
import "./GridTable.scss";

export function GridTable() {
  return (
    <>
      <div className="grid.table" style={{ border: "1px solid black" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>

      <div className="grid-table" style={{ border: "1px solid orange" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>

      <div className="grid-table" style={{ border: "1px solid red" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>

      <div className="grid-table" style={{ border: "1px solid gray" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>

      <div className="grid-table" style={{ border: "1px solid blue" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>

      <div className="grid-table" style={{ border: "1px solid black" }}>
        <div className="grid-table-row">
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
          <div>Lorem ipsum dolor sit amet, consectetur</div>
        </div>
      </div>
    </>
  );
}

export function GridTableWithTableTags2Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 2 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags3Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  return (
    <div className="grid-wrapper" ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 3 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags4Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 4 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags6Cols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 6 columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags6PlusCols() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 6+ columns</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function GridTableWithTableTags2ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 2 columns with colspan 2</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags3ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 3 columns with colspan 2</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags4ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 4 columns with colspan 2 and 3</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={3}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags6ColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 6 columns with colspan 2 and 4</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td colSpan={4}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
export function GridTableWithTableTags6PlusColsWithSpan() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="grid-wrapper"  ref={wrapperRef} style={marginWidth}>
      <h2>Grid Table with table tags - 6+ columns with colspan 2 and 4</h2>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={2}>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td colSpan={4}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function GridTableWithTableTagsExamples() {
  return (
    <>
      <GridTableWithTableTags2Cols />
      <GridTableWithTableTags3Cols />
      <GridTableWithTableTags4Cols />
      <GridTableWithTableTags6Cols />
      <GridTableWithTableTags6PlusCols />
    </>
  );
}
export function GridTableWithTableTagsAndSpanExamples() {
  return (
    <>
      <GridTableWithTableTags2ColsWithSpan />
      <GridTableWithTableTags3ColsWithSpan />
      <GridTableWithTableTags4ColsWithSpan />
      <GridTableWithTableTags6ColsWithSpan />
      <GridTableWithTableTags6PlusColsWithSpan />
    </>
  );
}
