import { useEffect, useRef, useState } from "react";
import "./FlexTable.scss";

export function FlexTable2Cols(props) {
  const { vertical, offgrid } = props;
  return (
    <>
      <h2>Flex table using divs - 2 columns</h2>
      <div
        className={`container ${
          offgrid ? "offgrid" : vertical ? "vertical" : "horizontal"
        }`}
      >
        <div className="row-header">
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
        </div>

        <div className="row">
          <div className="cell">Lorem ipsum</div>
          <div className="cell">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </div>
        </div>
      </div>
    </>
  );
}
export function FlexTable3Cols(props) {
  const { vertical, offgrid } = props;
  return (
    <>
      <h2>Flex table using divs - 3 columns</h2>
      <div
        className={`container ${
          offgrid ? "offgrid" : vertical ? "vertical" : "horizontal"
        }`}
      >
        <div className="row-header">
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
        </div>

        <div className="row">
          <div className="cell">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
          <div className="cell">Lorem ipsum</div>
        </div>
      </div>
    </>
  );
}
export function FlexTable4Cols(props) {
  const { vertical, offgrid } = props;
  return (
    <>
      <h2>Flex table using divs - 4 columns</h2>
      <div
        className={`container ${
          offgrid ? "offgrid" : vertical ? "vertical" : "horizontal"
        }`}
      >
        <div className="row-header">
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
        </div>

        <div className="row">
          <div className="cell">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
          <div className="cell">Lorem ipsum</div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
        </div>
      </div>
    </>
  );
}
export function FlexTable6Cols(props) {
  const { vertical, offgrid } = props;
  return (
    <>
      <h2>Flex table using divs - 6 columns</h2>
      <div
        className={`container ${
          offgrid ? "offgrid" : vertical ? "vertical" : "horizontal"
        }`}
      >
        <div className="row-header">
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
          <div className="cell-header">Label</div>
        </div>

        <div className="row">
          <div className="cell">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
          <div className="cell">Lorem ipsum</div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
          <div className="cell">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </div>
          <div className="cell">Lorem ipsum dolor sit amet</div>
        </div>
      </div>
    </>
  );
}

export function FlexTableWithTableTags() {
  const wrapperRef = useRef(null);
  const [marginWidth, setMarginWidth] = useState(null);

  const handleWindowSizeChange = () => {
    const { current } = wrapperRef;
    const { clientWidth, scrollWidth } = current;
    if (scrollWidth > clientWidth)
      setMarginWidth({ "--margin-width": "var(--margin-size)" });
    else setMarginWidth(null);
  };

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);
  return (
    <div className="flex-wrapper" ref={wrapperRef} style={marginWidth}>
      <table>
        <thead>
          <tr>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
            <th>Label</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat
            </td>
            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
          </tr>
          <tr>
            <td>Lorem ipsum</td>
            <td>Lorem ipsum</td>
            <td colSpan={2}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function FlexTable() {
  return (
    <>
      <FlexTable2Cols />
      <FlexTable3Cols />
      <FlexTable4Cols />
      <FlexTable6Cols />
    </>
  );
}
