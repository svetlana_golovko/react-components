import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import {
  FlexTable,
  FlexTableWithTableTags,
} from "./components/FlexTable/FlexTable";
import {
  GridTable,
  GridTableWithTableTagsExamples,
  GridTableWithTableTagsAndSpanExamples,
} from "./components/GridTable/GridTable";
import {
  TableWithTableTagsExamples,
  TableWithTableTagsAndSpanExamples,
} from "./components/TableWithTableTags/TableWithTableTagsStatic/TableWithTableTagsStatic";
import {
  TableWithTableTagsSC,
  TableWithTableTagsWithSpanSC,
} from "./components/TableWithTableTagsSC/TableWithTableTagsSCStatic/TableWithTableTagsSC";

import { TableSC } from "./components/TableWithTableTagsSC/TableWithTableTagsSCDynamic/TableSC";
import { TableWithTableTags } from "./components/TableWithTableTags/TableWithTableTagsDynamic/TableWithTableTagsDynamic";

import {
  table2Columns,
  table3Columns,
  table4Columns,
  table6Columns,
  table6PlusColumns,
  table3ColumnsSpan,
  table4ColumnsSpan,
  table4ColumnsOffgrid,
  table3RowsSpan,
} from "./TableExamplesData";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <TableSC body={table4Columns}/>
  </React.StrictMode>
);
