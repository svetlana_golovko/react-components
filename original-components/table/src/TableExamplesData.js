export const table2Columns = `
<h2>Table with table tags using Styled Components - 2 columns</h2>
<table>
<thead>
  <tr>
    <th>Label</th>
    <th>Label</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
  </tr>
  <tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
</tr>
</tbody>
</table>`;
export const table3Columns = `
<h2>Table with table tags using Styled Components - 3 columns</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
    aliquip ex ea commodo consequat
  </td>
  </tr>
  <tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
</tbody>
</table>`;
export const table4Columns = `
<h2>Table with table tags using Styled Components - 4 columns</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
    <th>Label 4</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
    aliquip ex ea commodo consequat
  </td>
  <td>Lorem ipsum</td>
  </tr>
  <tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
</tr>
</tr>
<tr>
<td>Lorem ipsum</td>
<td>Lorem ipsum</td>
<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
<td>Lorem ipsum</td>
</tr>
</tbody>
</table>`;
export const table6Columns = `
<h2>Table with table tags using Styled Components - 6 columns</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
    <th>Label 4</th>
    <th>Label 5</th>
    <th>Label 6</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
      ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
      aliquip ex ea commodo consequat
    </td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
</tr>
</tbody>
</table>`;
export const table6PlusColumns = `
<h2>Table with table tags using Styled Components - 6+ columns</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
    <th>Label 4</th>
    <th>Label 5</th>
    <th>Label 6</th>
    <th>Label 7</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
      ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
      aliquip ex ea commodo consequat
    </td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  </tr>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  </tr>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
<tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td> Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
</tbody>
</table>`;

export const table3ColumnsSpan = `
<h2>Table with table tags using Styled Components - 3 columns and colspan</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td colspan=2>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
    aliquip ex ea commodo consequat
  </td>
  </tr>
  <tr>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum</td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
</tr>
</tbody>
</table>`;
export const table4ColumnsSpan = `
<h2>Table with table tags using Styled Components - 4 columns and colspan</h2>
<table>
<thead>
  <tr>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
    <th>Label 4</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Lorem ipsum</td>
    <td colspan=2>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
    aliquip ex ea commodo consequat
  </td>
  <td>Lorem ipsum</td>
  </tr>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
  <tr>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
</tbody>
</table>`;

export const table4ColumnsOffgrid = `
<h2>Table with table tags using Styled Components - 4 columns, offgrid style headers</h2>
<table>
<thead>
  <tr>    
    <th>Labels</th>
    <th>Label 1</th>
    <th>Label 2</th>
    <th>Label 3</th>
  </tr>
</thead>
<tbody>
  <tr>
    <th>Label 1</th>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
    aliquip ex ea commodo consequat
  </td>
  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
  <td>Lorem ipsum</td>
  </tr>
  <tr>
    <th>Label 2</th>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
  <tr>
    <th>Label 3</th>
    <td>Lorem ipsum</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
    <td>Lorem ipsum</td>
  </tr>
</tbody>
</table>`;