import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Modal, Modal2 } from "./components/Modal/Modal";
import { ModalFooter } from "./components/Modal/ModalFooter";
import { ModalContent } from "./components/Modal/ModalContent";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <>
    <Modal button={<button>Open Modal</button>}>
      <ModalContent>
        <h2>Header here</h2>
        Modal body <br />
        Can be anything <br />
        Please fill in tht fields for our representative to go through them and send you our offer back <br />
        <input type={"text"}></input>
      </ModalContent>
      <ModalFooter>Footer content</ModalFooter>
    </Modal>
    <Modal
      button={<button>Open Modal 2</button>}
      isBottom={true}
      hasCloseButton={false}
    >
      <ModalContent>
        Modal body <br />
        Can be anything <br />
      </ModalContent>
    </Modal>
    <Modal
      button={<button>Open Modal 3</button>}
      isBottom={true}
    >
      <ModalContent>
        Modal body <br />
        Can be anything <br />
      </ModalContent>
      <ModalFooter><button>Submit inquiry</button></ModalFooter>
    </Modal>
  </>
);
