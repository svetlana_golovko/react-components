import "./Modal.scss";

export function ModalFooter(props) {
  return <div className="modal-footer">{props.children}</div>;
}
