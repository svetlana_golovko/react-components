import "./Modal.scss";

export function ModalContent(props) {
  const { children } = props;
  return (
    <div className="modal-body">
      <div className="modal-content">{children}</div>
    </div>
  );
}
