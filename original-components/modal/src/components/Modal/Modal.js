import "./Modal.scss";
import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { ReactComponent as CloseIcon } from "./close.svg";

export function Modal(props) {
  const { isBottom, children, button, hasCloseButton = true } = props;
  const [show, setShow] = useState(false);

  // Disable scroll on the body when Modal is shown
  useEffect(() => {
    if (show) document.body.style.overflow = "hidden";
    if (!show) document.body.style.overflow = "unset";
  }, [show]);

  const toggleShow = () => {
    setShow(!show);
  };

  const body = (
    <div className="modal-overlay">
      <div className={`modal ${isBottom ? "modal-bottom" : ""}`}>
        {children}
        {hasCloseButton && (
          <button
            className="modal-close-button"
            onClick={() => {
              toggleShow();
            }}
          >
            <CloseIcon />
          </button>
        )}
      </div>
    </div>
  );
  return (
    <div>
      {React.cloneElement(button, { onClick: toggleShow })}
      {show && ReactDOM.createPortal(body, document.getElementById("root"))}
    </div>
  );
}
