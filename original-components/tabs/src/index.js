import React from "react";
import ReactDOM from "react-dom/client";
import styled, { createGlobalStyle } from "styled-components";
import { Tabs } from "./components/Tab/Tabs";
import { Tab } from "./components/Tab/Tab";
import { Accordion} from "./components/Accordion/Accordion";
import { AccordionItem } from "./components/Accordion/AccordionItem";
import { bodyTheme } from "./styles/themes";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  body {
    margin: 0;
    background-color: ${bodyTheme.backgroundColor};
  }
`;

const TableStyle = styled.table`
  width: 75%;
  border: 1px solid;
  border-collapse: collapse;
`;

const ThStyle = styled.th`
  width: 50%;
  border: 1px solid;
`;

const TdStyle = styled.td`
  width: 50%;
  border: 1px solid;
`;

const root = ReactDOM.createRoot(document.getElementById("root"));
let getTable = (scalingOption, size, tabStyle, colorScheme) => {
 return (
     <Tabs scalingOption={scalingOption} size={size} tabStyle={tabStyle} colorScheme={colorScheme}>
         <Tab header="Rooms" count="23" icon="weather.svg" index={1}>Test 4</Tab>
         <Tab header="Restaurant & Bar" count="18" index={2}>Test 5</Tab>
         <Tab header="Spa & Fitness" count="18" index={3}>Test 1</Tab>
     </Tabs>
 );
}

let getTableWithPadding = (scalingOption, size, tabStyle, colorScheme) => {
    return (
        <Tabs scalingOption={scalingOption} size={size} tabStyle={tabStyle} colorScheme={colorScheme} firstTabPadding="25px">
            <Tab header="Rooms" count="23" icon="weather.svg" index={1}>Test 4</Tab>
            <Tab header="Restaurant & Bar" count="18" index={2}>Test 5</Tab>
            <Tab header="Spa & Fitness" count="18" index={3}>Test 1</Tab>
            <Tab header="Rooms" count="23" icon="weather.svg" index={4}>Test 4</Tab>
            <Tab header="Restaurant & Bar" count="18" index={5}>Test 5</Tab>
            <Tab header="Spa & Fitness" count="18" index={6}>Test 1</Tab>
        </Tabs>
    );
}


let getAccordion = (size,  colorScheme, padding, icon,  count) => {
    return (
        <Accordion colorScheme={colorScheme} size={size} padding={padding}>
            <AccordionItem icon={icon} header="Etiam cursus, risus non rutrum fringilla" count={count}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam mollis, libero et eleifend gravida, mauris diam hendrerit ipsum, sit amet vestibulum ipsum ante quis eros. Etiam cursus, risus non rutrum fringilla, diam sem placerat ex, vel tincidunt urna justo eu odio. Vivamus imperdiet felis in pharetra rhoncus.
            </AccordionItem>
            <AccordionItem icon={icon} header="Header 2" >
                Content 2
            </AccordionItem>
            <AccordionItem header="Header 3" count={count}>
                Content 3
            </AccordionItem>
        </Accordion>
    );
}

root.render(
  <React.StrictMode>
    <GlobalStyle />
      <h1>tab + scroll </h1>
      <h2>small </h2> {getTable("scroll", "small" , "tab")}
      <h2>small + padding</h2> {getTableWithPadding("scroll", "small" , "tab")}
      <h2>large </h2> {getTable("scroll", "large" , "tab")}
      <h2>large + padding</h2> {getTableWithPadding("scroll", "large" , "tab")}
      <h1>tab + fit </h1>
      <h2>small </h2> {getTable("fit", "small" , "tab")}
      <h2>large </h2> {getTable("fit", "large" , "tab")}
      <h2>large + padding </h2> {getTableWithPadding("fit", "large" , "tab")}
      <h1>line + scroll </h1>
      <h2>small </h2> {getTable("scroll", "small" , "line", "light")}
      <h2>small + padding</h2> {getTableWithPadding("scroll", "small" , "line", "light")}
      <h2>medium </h2> {getTable("scroll", "medium" , "line", "light")}
      <h2>medium + padding</h2> {getTableWithPadding("scroll", "medium" , "line", "light")}
      <h2>large </h2> {getTable("scroll", "large" , "line", "light")}
      <h1>line + fit </h1>
      <h2>small </h2> {getTable("fit", "small" , "line", "dark")}
      <h2>medium </h2> {getTable("fit", "medium" , "line", "dark")}
      <h2>medium + padding </h2> {getTableWithPadding("fit", "medium" , "line", "dark")}
      <h2>large </h2> {getTable("fit", "large" , "line", "dark")}
      <h1>small + accordion</h1> {getTable("accordion", "small" , "tab", "light")}
      <h1>medium + accordion</h1> {getTable("accordion", "medium" , "tab", "light")}
      <h1>large + accordion</h1> {getTable("accordion", "large" , "tab", "light")}
      <p><h2> Only Accordion Small (light)</h2></p>
      <TableStyle>
          <tr>
              <ThStyle>Without padding</ThStyle>
              <ThStyle>With padding</ThStyle>
          </tr>
          <tr>
              <TdStyle> {getAccordion("small", "light", "false", "icon.svg", "2")} </TdStyle>
              <TdStyle> {getAccordion("small", "light", "true", "active-icons\\weather.svg", "2")} </TdStyle>
          </tr>
      </TableStyle>
      <p><h2> Only Accordion Medium (light)</h2></p>
      <TableStyle>
          <tr>
              <ThStyle>Without padding</ThStyle>
              <ThStyle>With padding</ThStyle>
          </tr>
          <tr>
              <TdStyle> {getAccordion("medium", "light", "false", "icon.svg", "2")} </TdStyle>
              <TdStyle> {getAccordion("medium", "light", "true", "active-icons\\weather.svg", "2")} </TdStyle>
          </tr>
      </TableStyle>
      <p><h2> Only Accordion Large (light)</h2></p>
      <TableStyle>
          <tr>
              <ThStyle>Without padding</ThStyle>
              <ThStyle>With padding</ThStyle>
          </tr>
          <tr>
              <TdStyle> {getAccordion("large", "light", "false", "icon.svg", "2")} </TdStyle>
              <TdStyle> {getAccordion("large", "light", "true", "active-icons\\weather.svg", "2")} </TdStyle>
          </tr>
      </TableStyle>
  </React.StrictMode>
);