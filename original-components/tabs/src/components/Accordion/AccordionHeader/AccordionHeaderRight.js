import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import WalsheimProMedium from "../../../fonts.css";

library.add(faAngleDown);

const FontAwesomeIconStyled = styled(FontAwesomeIcon)`
  width: ${(props) => props.theme.arrow.width};
  height: ${(props) => props.theme.arrow.height};
  transform: ${(props) => (props.down === "true" ? "rotateX(180deg)" : "0")};
  padding: ${props => props.theme.arrow.padding};
  flex-grow: 0;
  object-fit: contain;
`;
const AccordionHeaderRightStyled = styled.div`
  display: flex;
  margin-left: auto;
  align-items: center;
  flex-wrap: nowrap;
  gap: 16px;
`;

const AccordionCountStyled = styled.div`
  display: flex;   
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap 10px;
  border-radius: 4px;
  font-family: WalsheimProMedium;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  height: ${(props) => props.theme.count.height};
  min-width: ${(props) => props.theme.count.minWidth};  
  background-color: ${(props) => props.theme.count.backgroundColor};
  color: ${(props) => props.theme.count.textColor};
  font-size: ${(props) => props.theme.count.fontSize};  
  padding: ${(props) => props.theme.count.padding};
  margin: ${props => props.theme.count.margin}
`;

export function AccordionHeaderRight(props) {
  const { count, isReversedArrow } = props;
  return (
    <AccordionHeaderRightStyled>
      {count && <AccordionCountStyled>{count}</AccordionCountStyled>}
      <FontAwesomeIconStyled
        icon={["fas", "angle-down"]}
        down={isReversedArrow.toString()}
      />
    </AccordionHeaderRightStyled>
  );
}
