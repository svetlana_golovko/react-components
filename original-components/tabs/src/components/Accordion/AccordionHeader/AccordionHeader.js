import styled, { ThemeContext } from "styled-components";
import { AccordionHeaderRight } from "./AccordionHeaderRight";
import { useContext } from "react";
import GTWalsheimPro from "../../../fonts.css";

const AccordionHeaderIcon = styled.span`
  width: ${(props) => props.theme.icon.width};
  height: ${(props) => props.theme.icon.height};
  padding: ${(props) => props.theme.icon.padding};
  flex-grow: 0;
  flex-shrink: 0;
  object-fit: contain;
`;

const AccordionHeaderStyled = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  justify-content: row;
  align-items: center;
  gap 16px;
  padding: ${(props) => props.theme.headerPadding}  ${(props) =>
  props.theme.padding === "true" ? props.theme.headerLeftPadding : "0"}
  ;
  cursor: pointer;
  :first-child {
    border-top: 0;
  }
`;

const AccordionHeaderTextStyled = styled.span`
  flex-grow: 1;
  font-family: GTWalsheimPro;
  font-size: ${(props) => props.theme.fontSize};
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: ${(props) => props.theme.lineHeight};
  letter-spacing: normal;
  text-align: left;
  color: ${(props) => props.theme.headerTextColor};
`;

export function AccordionHeader(props) {
  const { isReversedArrow, reverseArrow } = props;
  const { icon, header, count } = props;
  const themeContext = useContext(ThemeContext);
  return (
    <AccordionHeaderStyled onClick={reverseArrow}>
      {icon && (
        <AccordionHeaderIcon>
          <img width="100%" src={icon} className="icon" />
        </AccordionHeaderIcon>
      )}
      <AccordionHeaderTextStyled>{header}</AccordionHeaderTextStyled>
      <AccordionHeaderRight count={count} isReversedArrow={isReversedArrow} />
    </AccordionHeaderStyled>
  );
}
