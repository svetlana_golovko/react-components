import React from "react";
import { ThemeProvider } from "styled-components";
import { AccordionItem } from "./AccordionItem";
import {
  styleLargeDark,
  styleLargeLight,
  styleMediumLight,
  styleSmallDark,
  styleSmallLight,
} from "../../styles/themesAccordion";

const getScheme = (props) => {
  const { colorScheme, size, padding } = props;

  const styleSwitch = (scheme, size) => {
    switch (true) {
      case size === "small" && scheme === "dark":
        return styleSmallDark;
      case size === "medium" && scheme === "dark":
        return styleSmallDark;
      case size === "large" && scheme === "dark":
        return styleLargeDark;
      case size === "small" && scheme === "light":
        return styleSmallLight;
      case size === "medium" && scheme === "light":
        return styleMediumLight;
      case size === "large" && scheme === "light":
        return styleLargeLight;
      default:
        return styleSmallLight;
    }
  };
  let selectedTheme = styleSwitch(colorScheme, size);
  let schemeObject = Object.assign({}, selectedTheme);
  schemeObject.padding = padding;
  return schemeObject;
};

export function Accordion(props) {
  const selectedTheme = getScheme(props);
  let children = props.children;
  return (
    <ThemeProvider theme={selectedTheme}>
      {children.map((child, index) => {
        return (
          <AccordionItem
            key={index}
            {...child.props}
            bottomBorder={index + 1 !== children.length}
          >
            {child.props.children}
          </AccordionItem>
        );
      })}
    </ThemeProvider>
  );
}
