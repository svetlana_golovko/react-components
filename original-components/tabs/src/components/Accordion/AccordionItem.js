import styled from "styled-components";
import { useState } from "react";
import { AccordionHeader } from "./AccordionHeader/AccordionHeader";

const AccordionItemStyled = styled.div`
  border-bottom: ${(props) =>
    props.bottomBorder === true ? props.theme.borderBottom : 0};
`;

const AccordionContent = styled.div`
  font-size: 16px;
  padding: 0px 16px;
  overflow: hidden;
  min-width: 100%;
  font-family: GTWalsheimPro;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.38;
  letter-spacing: normal;
  text-align: left;
  align-self: stretch;
  flex-grow: 0;
  padding: 16px
    ${(props) =>
      props.theme.padding === "true" ? props.theme.headerLeftPadding : 0};
  border-top: ${(props) => props.theme.contentBorderBottom};
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.headerTextColor};
  display: ${(props) => (props.visible ? "block" : "none")};
`;

export function AccordionItem(props) {
  const [isContentVisible, setIsContentVisible] = useState(false);
  const handleAccordionToggle = () => {
    setIsContentVisible(!isContentVisible);
  };

  return (
    <AccordionItemStyled bottomBorder={props.bottomBorder}>
      <AccordionHeader
        {...props}
        isReversedArrow={isContentVisible}
        reverseArrow={handleAccordionToggle}
      />
      <AccordionContent visible={isContentVisible}>
        {props.children}
      </AccordionContent>
    </AccordionItemStyled>
  );
}
