import styled, { css } from "styled-components";
import TabHeader, { TabHeaderStyled } from "./TabHeader";

const TabStyled = styled.div`
  display: flex;
  flex-grow: 0;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  border: ${(props) => props.theme.border};
  cursor: pointer;
  border-bottom: ${(props) =>
    props.isActive ? props.theme.activeBorderBottom : props.theme.borderBottom};
  :hover {
    ${(props) => {
      if (props.theme.isLineStyle && !props.isActive) {
        return css`
          border-bottom: 1px solid ${(props) => props.theme.hoverBorderColor};
        `;
      }
    }}
    ${TabHeaderStyled} {
      ${(props) => {
        if (!props.theme.isLineStyle) {
          return css`
            background-color: ${(props) => props.theme.activeBackgroundColor};
          `;
        }
      }}
    }
  }
  ${(props) => {
    if (props.scalingOption === "scroll")
      return css`
        flex-shrink: 0;
        overflow: hidden;
      `;
  }}
`;

const StripStyled = styled.div`
  height: ${(props) => (props.theme.isLineStyle ? "0px" : "8px")};
  align-self: stretch;
  flew-grow: 0;
  ${(props) => {
    if (props.isActive) {
      return css`
        background-color: ${(props) => props.theme.activeBackgroundColor};
      `;
    } else {
      return css`
        background-image: ${(props) => props.theme.stripLineBackground};
      `;
    }
  }}
`;

export function Tab(props) {
  const { count, header, icon, index, size, scalingOption, onClick, activeTab } =
    props;
  let getIconWithLocation = (icon, isActive) => {
    let iconSrc = null;
    if (icon) {
      iconSrc = icon;
      if (isActive) {
        iconSrc = "active-icons\\" + icon;
      }
    }
    return iconSrc;
  };

  return (
    <TabStyled
      onClick={() => onClick(index)}
      isActive={activeTab === index}
      size={size}
      scalingOption={scalingOption}
    >
      <TabHeader
        header={header}
        count={count}
        icon={getIconWithLocation(icon, activeTab === index)}
        isActive={activeTab === index}
      />
      <StripStyled isActive={activeTab === index}></StripStyled>
    </TabStyled>
  );
}
