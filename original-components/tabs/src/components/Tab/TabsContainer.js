import styled, { css } from "styled-components";
import React, { createRef, useState } from "react";
import { TabPanel } from "./TabPanel";
import { TabScrollArrows } from "./TabScrollArrows";

const TabsContainerStyled = styled.div``;
const TabsWrapper = styled.div`
  position: relative;
  padding-left: ${(props) =>
    props.firstTabPadding && !props.isScrollable ? props.firstTabPadding : 0};
  border-bottom: 1px solid ${(props) => props.theme.borderColor};
`;

const TabsStyled = styled.div`
  display: flex;
  min-width: 0;
  margin-bottom: -1px;
  overflow-x: auto;
  ${(props) => {
    if (props.scalingOption === "scroll") {
      return css`
        height: ${(props) => props.theme.tabHeight};
      `;
    } else {
      return css`
        min-height: ${(props) => props.theme.tabHeight};
      `;
    }
  }}
  ::-webkit-scrollbar {
    display: none;
  }
`;

export function TabsContainer(props) {
  const { size, scalingOption, firstTabPadding } = props;
  const [activeTab, setActiveTab] = useState(null);
  const tabsRef = createRef();
  let children = props.children;
  let childrenArray = Array.isArray(children) ? children : [children];

  const handleTabToggle = (index) => {
    setActiveTab(index);
  };

  return (
    <TabsContainerStyled>
      <TabsWrapper
        firstTabPadding={firstTabPadding}
        isScrollable={scalingOption === "scroll"}
      >
        <TabsStyled ref={tabsRef} size={size} scalingOption={scalingOption}>
          {childrenArray.map((child,i) => {
            return React.cloneElement(child, {
              ...child,
              onClick: handleTabToggle,
              scalingOption: scalingOption,
              size: size,
              activeTab: activeTab,
              key: i
            });
          })}
        </TabsStyled>
        {scalingOption === "scroll" && (
          <TabScrollArrows tabsRef={tabsRef} size={size} />
        )}
      </TabsWrapper>
      {childrenArray.map((child, index) => {
        return <TabPanel key={index} {...child.props} activeTab={activeTab} />;
      })}
    </TabsContainerStyled>
  );
}
