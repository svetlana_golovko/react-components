import React from "react";
import styled, { ThemeContext, css } from "styled-components";
import { useContext } from "react";
import WalsheimProMedium from "../../fonts.css";

const TabIconStyled = styled.div`
  height: ${(props) => props.theme.iconHeight};
  width: ${(props) => props.theme.iconHeight};
`;

export const TabHeaderStyled = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  gap 12px;  
  padding: ${(props) => 
  (props.isActive && props.theme.isLineStyle) 
          ? props.theme.activeHeaderPadding 
          : props.theme.headerPadding};
  cursor: pointer;  
  background-color:  ${(props) =>
    props.isActive
        ? props.theme.activeBackgroundColor
        : props.theme.backgroundColor};      
  font-family: WalsheimProMedium;
  font-size: ${(props) => props.theme.fontSize};
  font-weight: 500;
  line-height: 1.43;
  color: ${(props) =>
    props.isActive
        ? props.theme.activeHeaderTextColor
        : props.theme.headerTextColor};
  text-align: center;
`;

const TabCountStyled = styled.div`
  gap 10px;
  padding: 1px 4px;
  border-radius: 4px;
  background-color: ${(props) =>
    props.isActive
      ? props.theme.count.activeBackgroundColor
      : props.theme.count.backgroundColor};
  font-size: ${(props) => props.theme.count.fontSize};
  font-family: WalsheimProMedium;
  font-weight: 500;
  line-height: 1.33;
  letter-spacing: normal;
  color: ${(props) =>
    props.isActive
        ? props.theme.count.activeTextColor
        : props.theme.count.textColor};
`;

export default function TabHeader(props) {
  const { icon, header, count, isActive } = props;
  const themeContext = useContext(ThemeContext);
  return (
    <TabHeaderStyled className="TabHeaderStyled" isActive={isActive}>
      {themeContext.isLineStyle && icon && (
        <TabIconStyled>
          <img src={icon} width="100%" height="100%" />
        </TabIconStyled>
      )}
      {header}
      {count && (
        <TabCountStyled isActive={isActive}>{count}</TabCountStyled>
      )}
    </TabHeaderStyled>
  );
}
