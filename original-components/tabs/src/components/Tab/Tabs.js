import { useState, useEffect } from "react";
import { TabsContainer } from "./TabsContainer";
import { Accordion } from "../Accordion/Accordion";
import { AccordionItem } from "../Accordion/AccordionItem";
import { ThemeProvider } from "styled-components";
import {
  styleTabsSmall,
  styleTabsLarge,
  styleLineSmallLight,
  styleLineMediumLight,
  styleLineLargeLight,
  styleLineSmallDark,
  styleLineMediumDark,
  styleLineLargeDark,
} from "../../styles/themes";

const getScheme = (props) => {
  const { colorScheme, size, padding, tabStyle } = props;
  const styleSwitch = (style, scheme, size) => {
    switch (true) {
      case style === "line" && size === "small" && scheme === "dark":
        return styleLineSmallDark;
      case style === "line" && size === "medium" && scheme === "dark":
        return styleLineMediumDark;
      case style === "line" && size === "large" && scheme === "dark":
        return styleLineLargeDark;
      case style === "line" && size === "small" && scheme === "light":
        return styleLineSmallLight;
      case style === "line" && size === "medium" && scheme === "light":
        return styleLineMediumLight;
      case style === "line" && size === "large" && scheme === "light":
        return styleLineLargeLight;

      case style === "tab" && size === "small":
        return styleTabsSmall;
      case style === "tab" && size === "large":
        return styleTabsLarge;
      default:
        return styleTabsLarge;
    }
  };

  let selectedTheme = styleSwitch(tabStyle, colorScheme, size);
  let schemeObject = Object.assign({}, selectedTheme);
  schemeObject.size = size;
  schemeObject.padding = padding;
  schemeObject.isLineStyle = tabStyle === "line";

  return schemeObject;
};

export function Tabs(props) {
  const [isMobile, setIsMobile] = useState(false);
  const selectedTheme = getScheme(props);

  useEffect(() => {
    const handleWindowSizeChange = () => {
      let currentWidth = window.innerWidth <= 600;
      if (currentWidth !== isMobile) {
        setIsMobile(currentWidth);
      }
    };
    window.addEventListener("resize", handleWindowSizeChange);
    handleWindowSizeChange();
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  });

  const convertTabToAccordion = (props, isMobile) => {
    const { scalingOption } = props;
    let children = props.children;
    let childrenArray = Array.isArray(children) ? children : [children];

    if (scalingOption === "accordion" || isMobile) {
      return (
        <Accordion {...props}>
          {childrenArray.map((x, i) => {
            return <AccordionItem key={i} {...x.props} />;
          })}
        </Accordion>
      );
    } else {
      return <TabsContainer {...props} />;
    }
  };

  return (
    <ThemeProvider theme={selectedTheme}>
      {convertTabToAccordion(props, isMobile)}
    </ThemeProvider>
  );
}
