import styled from "styled-components";
import WalsheimProBoldOblique from "../../fonts.css";
const TabPanelStyled = styled.div`
  display: ${(props) => (props.isActive ? "block" : "none")};
  margin-top: 15px;
  margin-bottom: 15px;
  text-align: left;
  font-weight: normal;
  font-family: WalsheimProBoldOblique;
`;

export function TabPanel(props) {
  const { activeTab, index } = props;
  return (
    <TabPanelStyled {...props} isActive={activeTab === index}>
      {props.children}
    </TabPanelStyled>
  );
}
