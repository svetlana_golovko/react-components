import styled from "styled-components";
import { useEffect, useState } from "react";
import { bodyTheme } from "../../styles/themes";

const ScrollArrow = styled.a`
  cursor: pointer;
  position: absolute;
  background-color: ${bodyTheme.backgroundColor};
  width: ${props => props.theme.scroll.width};
  top: 0;
  display: flex;
  transform: translate(0, ${(props) => props.theme.scroll.translateY});
  height: ${(props) => props.theme.scroll.height};
  padding: ${(props) => props.theme.scroll.padding};
`;

const LeftScrollArrow = styled(ScrollArrow)`
  border-right: 1px solid ${(props) => props.theme.borderColor};

`;
const RightScrollArrow = styled(ScrollArrow)`
  right: 0;
  border-left: 1px solid ${(props) => props.theme.borderColor};
`;

const ScrollArrowImg = styled.img`
  width: ${props => props.theme.scroll.imgWidth};
  height: ${props => props.theme.scroll.imgHeight};
  object-fit: contain;
`;

export function TabScrollArrows(props) {
  const { tabsRef, size } = props;
  const [scrollPosition, setScrollPosition] = useState(0);
  const [showLeftArrow, setShowLeftArrow] = useState(false);
  const [showRightArrow, setShowRightArrow] = useState(false);

  useEffect(() => {
    const handleWindowSizeChange = () => {
      const offset = 2;
      setScrollPosition(Math.round(tabsRef.current.scrollLeft));
      setShowLeftArrow(scrollPosition !== 0);
      setShowRightArrow(
        Math.round(scrollPosition + tabsRef.current.offsetWidth + offset) <
          tabsRef.current.scrollWidth
      );
    };
    if (tabsRef.current) {
      window.addEventListener("resize", handleWindowSizeChange);
      handleWindowSizeChange();
      return () => {
        window.removeEventListener("resize", handleWindowSizeChange);
      };
    }
  });

  const handleScrollClick = (scrollOffset) => {
    const scrollLength = 50;
    tabsRef.current.scrollBy({
      left: scrollLength * (scrollOffset ? 1 : -1),
    });
    setScrollPosition(tabsRef.current.scrollLeft);
  };

  return (
    <>
      {showLeftArrow && (
        <LeftScrollArrow onClick={() => handleScrollClick(false)} size={size}>
          <ScrollArrowImg src={"arrow-left.svg"} />
        </LeftScrollArrow>
      )}
      {showRightArrow && (
        <RightScrollArrow onClick={() => handleScrollClick(true)} size={size}>
          <ScrollArrowImg src={"arrow-right.svg"}/>
        </RightScrollArrow>
      )}
    </>
  );
}
