const colors = {
  buoySuccess: "#00bea2",
  midnight: "#004152",
  white: "#ffffff",
  groupGrey: "#eef1f1",
  lightMidnight: "#396a76",
  grey: "#d6dfdf",
};

export const styleLightTheme = {
  headerTextColor: colors.midnight,
  backgroundColor: colors.white,
  border: "none",
  borderBottom: `1px solid ${colors.grey}`,
  contentBorderBottom: `1px dashed ${colors.grey}`,
  count: {
    textColor: colors.white,
    backgroundColor: colors.buoySuccess,
  },
};

export const styleDarkTheme = {
  headerTextColor: colors.white,
  backgroundColor: colors.midnight,
  border: "none",
  borderBottom: `1px solid ${colors.lightMidnight}`,
  contentBorderBottom: `1px dashed ${colors.lightMidnight}`,
  count: {
    textColor: colors.white,
    backgroundColor: colors.buoySuccess,
  },
};

export const styleSmall = {
  fontSize: "16px",
  headerPadding: "16px",
  headerLeftPadding: "16px",
  tabHeight: "22px",
  lineHeight: "1.38",
  count: {
    fontSize: "14px",
    padding: "1px 4px",
    height: "22px",
    minWidth: "24px",
    margin: "0",
  },
  icon: {
    width: "22px",
    height: "22px",
    padding: "0",
  },
  arrow: {
    width: "16px",
    height: "16px",
    padding: "3px 0"
  },
};

export const styleMedium = {
  fontSize: "20px",
  headerPadding: "16px",
  headerLeftPadding: "16px",
  tabHeight: "26px",
  lineHeight: "1.3",
  count: {
    fontSize: "16px",
    padding: "2px 6px",
    height: "26px",
    minWidth: "30px",
    margin: "0",
  },
  icon: {
    width: "24px",
    height: "24px",
    padding: "1px 0",
  },
  arrow: {
    width: "16px",
    height: "16px",
    padding: "5px 0"
  },
};

export const styleLarge = {
  fontSize: "24px",
  headerPadding: "24px",
  headerLeftPadding: "24px",
  tabHeight: "30px",
  lineHeight: "1.25",
  count: {
    fontSize: "16px",
    padding: "2px 6px",
    height: "26px",
    minWidth: "30px",
    margin: "2px 0",
  },
  icon: {
    width: "30px",
    height: "30px",
    padding: "0",
  },
  arrow: {
    width: "20px",
    height: "20px",
    padding: "5px 0"
  },
};

export const styleSmallLight = {
  ...styleLightTheme,
  ...styleSmall,
  count: {
    ...styleLightTheme.count,
    ...styleSmall.count,
  },
};

export const styleMediumLight = {
  ...styleLightTheme,
  ...styleMedium,
  count: {
    ...styleLightTheme.count,
    ...styleMedium.count,
  },
};

export const styleLargeLight = {
  ...styleLightTheme,
  ...styleLarge,
  count: {
    ...styleLightTheme.count,
    ...styleLarge.count,
  },
};

export const styleSmallDark = {
  ...styleDarkTheme,
  ...styleSmall,
  count: {
    ...styleDarkTheme.count,
    ...styleSmall.count,
  },
};

export const styleMediumDark = {
  ...styleDarkTheme,
  ...styleMedium,
  count: {
    ...styleDarkTheme.count,
    ...styleMedium.count,
  },
};

export const styleLargeDark = {
  ...styleDarkTheme,
  ...styleLarge,
  count: {
    ...styleDarkTheme.count,
    ...styleLarge.count,
  },
};
