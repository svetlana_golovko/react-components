const colors = {
  buoySuccess: "#00bea2",
  midnight: "#004152",
  white: "#ffffff",
  groupGrey: "#eef1f1",
  lightMidnight: "#396a76",
  grey: "#d6dfdf",
};

export const bodyTheme = {
  backgroundColor: "#f5f8f7",
};

//Line styles
export const styleLineLightTheme = {
  headerTextColor: colors.white,
  activeHeaderTextColor: colors.buoySuccess,
  count: {
    textColor: colors.white,
    activeTextColor: colors.midnight,
    backgroundColor: colors.lightMidnight,
    activeBackgroundColor: colors.buoySuccess,
  },
  border: "none",
  borderBottom: `1px solid ${colors.lightMidnight}`,
  activeBorderBottom: `4px solid ${colors.buoySuccess}`,
  hoverBorderColor: colors.white,
  borderColor: colors.lightMidnight,
};
export const styleLineDarkTheme = {
  headerTextColor: colors.midnight,
  activeHeaderTextColor: colors.buoySuccess,
  count: {
    textColor: colors.midnight,
    activeTextColor: colors.white,
    backgroundColor: colors.groupGrey,
    activeBackgroundColor: colors.buoySuccess,
  },
  border: "none",
  borderBottom: `1px solid ${colors.grey}`,
  activeBorderBottom: `4px solid ${colors.buoySuccess}`,
  hoverBorderColor: colors.midnight,
  borderColor: colors.grey,
};
export const styleLineSmall = {
  fontSize: "14px",
  headerPadding: "0 16px 10px",
  activeHeaderPadding: "0 16px 7px",
  iconWidth: "20px",
  iconHeight: "20px",
  tabHeight: "32px",
  count: {
    fontSize: "12px",
  },
  scroll: {
    height: "48px",
    width: "36px",
    padding: "18px 8px",
    translateY: "-8px",
    imgWidth: "12px",
    imgHeight: "12px",
  },
};
export const styleLineMedium = {
  fontSize: "16px",
  headerPadding: "0 24px 10px",
  activeHeaderPadding: "0 24px 7px",
  iconWidth: "22px",
  iconHeight: "22px",
  tabHeight: "34px",
  count: {
    fontSize: "14px",
  },
  scroll: {
    height: "48px",
    width: "36px",
    padding: "18px 8px",
    translateY: "-7px",
    imgWidth: "12px",
    imgHeight: "12px",
  },
};
export const styleLineLarge = {
  fontSize: "24px",
  headerPadding: "0 40px 10px",
  activeHeaderPadding: "0 40px 7px",
  iconWidth: "24px",
  iconHeight: "24px",
  tabHeight: "42px",
  count: {
    fontSize: "16px",
  },

  scroll: {
    height: "64px",
    width: "48px",
    padding: "24px 16px",
    translateY: "-15px",
    imgWidth: "16px",
    imgHeight: "16px",
  },
};

export const styleLineSmallDark = {
  ...styleLineDarkTheme,
  ...styleLineSmall,
  count: {
    ...styleLineDarkTheme.count,
    ...styleLineSmall.count,
  },
};
export const styleLineSmallLight = {
  ...styleLineLightTheme,
  ...styleLineSmall,
  count: {
    ...styleLineLightTheme.count,
    ...styleLineSmall.count,
  },
};

export const styleLineMediumDark = {
  ...styleLineDarkTheme,
  ...styleLineMedium,
  count: {
    ...styleLineDarkTheme.count,
    ...styleLineMedium.count,
  },
};

export const styleLineMediumLight = {
  ...styleLineLightTheme,
  ...styleLineMedium,
  count: {
    ...styleLineLightTheme.count,
    ...styleLineMedium.count,
  },
};

export const styleLineLargeDark = {
  ...styleLineDarkTheme,
  ...styleLineLarge,
  count: {
    ...styleLineDarkTheme.count,
    ...styleLineLarge.count,
  },
};

export const styleLineLargeLight = {
  ...styleLineLightTheme,
  ...styleLineLarge,
  count: {
    ...styleLineLightTheme.count,
    ...styleLineLarge.count,
  },
};

// Tabs styles
export const styleTabs = {
  headerTextColor: colors.midnight,
  activeHeaderTextColor: colors.midnight,
  border: `1px solid ${colors.grey}`,
  borderBottom: `1px solid ${colors.grey}`,
  activeBorderBottom: `1px solid ${colors.white}`,
  stripLineBackground: `linear-gradient(to bottom, ${colors.groupGrey} 0%, ${colors.grey} 100%)`,
  backgroundColor: colors.groupGrey,
  activeBackgroundColor: colors.white,
  borderColor: colors.grey,
  count: {
    textColor: colors.midnight,
    activeTextColor: colors.white,
    backgroundColor: colors.grey,
    activeBackgroundColor: colors.buoySuccess,
  },
  scroll: {
    height: "56px",
    width: "36px",
    padding: "22px 12px",
    translateY: "-8px",
    imgWidth: "12px",
    imgHeight: "12px",
  },
};

export const styleTabsSmall = {
  ...styleTabs,
  fontSize: "14px",
  headerPadding: "9px 16px 1px",
  iconWidth: "20px",
  iconHeight: "20px",
  tabHeight: "40px",
  count: {
    ...styleTabs.count,
    height: "18px",
    width: "22px",
    fontSize: "12px",
  },
};
export const styleTabsLarge = {
  ...styleTabs,
  fontSize: "16px",
  headerPadding: "9px 24px 1px",
  iconWidth: "20px",
  iconHeight: "20px",
  tabHeight: "42px",
  count: {
    ...styleTabs.count,
    height: "22px",
    width: "24px",
    fontSize: "14px",
  },
};
