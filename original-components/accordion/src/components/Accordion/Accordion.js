import React from 'react';
import {ThemeProvider} from "styled-components";
import {AccordionItem} from "./AccordionItem";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faCoffee, faStar} from "@fortawesome/free-solid-svg-icons";

library.add(faCoffee, faStar);

const lightTheme = {
    color: "#004152",
    background: "#f5f8f7",
    separatorColor: "#d6dfdf"
};

const darkTheme = {
    background: "#004152",
    color: "#f5f8f7",
    separatorColor: "#396a76"
};

const getScheme = (props) => {
    const {colorScheme, size, padding} = props;
    let selectedTheme = (colorScheme === "light") ? lightTheme : darkTheme;
    selectedTheme.size = size;
    selectedTheme.padding = padding;
    return selectedTheme;
}

export function Accordion(props) {
    const selectedTheme = getScheme(props)
    let children = props.children
    return (
        <ThemeProvider theme={selectedTheme}>
            {children.map((child, index) => {
                    const {header, count, icon} = child.props;
                    return (
                        <AccordionItem
                            key={index}
                            icon={icon}
                            header={header}
                            count={count}
                            content={child.props.children}
                        ></AccordionItem>
                    );
                }
            )}
        </ThemeProvider>
    );
}

export function AccordionNew(props) {
    const selectedTheme = getScheme(props)
    let children = props.children
    return (
        <ThemeProvider theme={selectedTheme}>
            {children.map((child, index) => {
                    const {header, count, icon} = child.props;
                    return (
                        <AccordionItem
                            key={index}
                            icon={icon}
                            header={header}
                            count={count}
                            content={child.props.children}
                        ></AccordionItem>
                    );
                }
            )}
        </ThemeProvider>
    );
}