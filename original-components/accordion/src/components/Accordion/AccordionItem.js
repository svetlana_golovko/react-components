import styled from "styled-components";
import {useState} from "react";
import {setFontSize, setPaddingSize} from "../../functions/styleFunctions";
import {AccordionHeader} from "./AccordionHeader/AccordionHeader";


const AccordionItemStyled = styled.div`
  font-size: ${(props) => setFontSize(props.theme.size)};
  color: ${(props) => props.theme.color};
  background-color: ${(props) => props.theme.background};
`;

const AccordionContent = styled.div`
  font-size: 16px;
  background-image: linear-gradient(
    to right,
    ${(props) => props.theme.separatorColor} 33%,
    rgba(255, 255, 255, 0) 0%
  );
  background-position: top;
  background-size: 5px 1px;
  background-repeat: repeat-x;
  padding: 16px ${(props) => (props.theme.padding ? setPaddingSize(props.theme.size) : "0")};
  overflow: hidden;
  display: ${(props) => (props.visible ? "block" : "none")};
`;

export function AccordionItem(props) {
    const {content} = props;
    const [isContentVisible, setIsContentVisible] = useState(false);

    const handleAccordionToggle = () => {
        setIsContentVisible(!isContentVisible);
    };

    return (
        <AccordionItemStyled>
            <AccordionHeader
                {...props}
                isReversedArrow={isContentVisible}
                reverseArrow={handleAccordionToggle}
            />
            <AccordionContent visible={isContentVisible}>
                {content}
            </AccordionContent>
        </AccordionItemStyled>
    );
}
