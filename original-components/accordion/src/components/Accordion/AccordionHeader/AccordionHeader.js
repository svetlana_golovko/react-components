import styled, {ThemeContext} from "styled-components";
import {AccordionHeaderRight} from "./AccordionHeaderRight";
import {setIconSize, setPaddingSize} from "../../../functions/styleFunctions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useContext} from 'react'

const AccordionHeaderText = styled.span`
  font-weight: bold;
`;
const AccordionHeaderIcon = styled.span`
  ${(props) => setIconSize(props.theme.size)};
  margin-right: 16px;
`;
const AccordionHeaderStyled = styled.div`
  display: flex;
  border-top: 1px solid ${(props) => props.theme.separatorColor};
  padding: ${(props) => setPaddingSize(props.theme.size)}
    ${(props) => (props.theme.padding ? setPaddingSize(props.theme.size) : "0")};
  cursor: pointer;
  :first-child {
    border-top: 0;
  }
`;

export function AccordionHeader(props) {
    const {isReversedArrow, reverseArrow} = props;
    const {icon, header, count} = props;
    const themeContext = useContext(ThemeContext)
    console.log('Current theme: ', themeContext)
    return (
        <AccordionHeaderStyled onClick={reverseArrow}>
            {icon && <AccordionHeaderIcon> <FontAwesomeIcon icon={["fas", icon]}/> </AccordionHeaderIcon>}
            <AccordionHeaderText>{header}</AccordionHeaderText>
            <AccordionHeaderRight count={count} isReversedArrow={isReversedArrow}/>
        </AccordionHeaderStyled>
    );
}
