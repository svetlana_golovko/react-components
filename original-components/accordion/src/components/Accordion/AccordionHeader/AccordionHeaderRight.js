import styled from "styled-components";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faAngleDown} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {setCollapseTagSize, setCountSize, setFontSize,} from "../../../functions/styleFunctions";

library.add(faAngleDown);

const FontAwesomeIconStyled = styled(FontAwesomeIcon)`
  width: ${(props) => setCollapseTagSize(props.theme.size)};
  height: ${(props) => setCollapseTagSize(props.theme.size)};
  transform: ${(props) =>
    props.down === "true" ? "rotateX(180deg)" : "0"};
  transition: all 0.25s;
`;
const AccordionHeaderRightStyled = styled.div`
  display: flex;
  margin-left: auto;
  overflow: hidden;
  align-items: center;
  flex-wrap: wrap;
`;

const AccordionCount = styled.div`
  background-color: #00bea2;
  color: white;
  border-radius: 4px;
  margin-right: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${(props) => (props.theme.size === "small" ? "1px 4px" : "2px 6px")};
  font-size: ${(props) => setFontSize(props.theme.size)};
  ${(props) => setCountSize(props.theme.size)};
`;

export function AccordionHeaderRight(props) {
    const {count, isReversedArrow} = props;
    return (
        <AccordionHeaderRightStyled>
            {count && <AccordionCount>{count}</AccordionCount>}
            <FontAwesomeIconStyled icon={["fas", "angle-down"]} down={isReversedArrow.toString()}/>
        </AccordionHeaderRightStyled>
    );
}
