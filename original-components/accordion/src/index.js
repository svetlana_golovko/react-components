import React from "react";
import ReactDOM from "react-dom/client";
import { createGlobalStyle } from "styled-components";

import {Accordion, AccordionNew} from "./components/Accordion/Accordion";
import {InlineAccordionItem} from "./components/InlineAccordion/InlineAccordion";
import {AccordionItem} from "./components/Accordion/AccordionItem";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  body {
    margin: 0;
  }
`;

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <GlobalStyle />
      <Accordion colorScheme="light" size="medium" padding="false">
          <div header="Lorem ipsum dolor sit consectetur?">
             werwerw
          </div>
          <div header="Lorem ipsum dolor sit amet, consectetur adipiscing elit?" count="2" >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam mollis,
              libero et eleifend gravida, mauris diam hendrerit ipsum, sit amet vestibulum ipsum ante quis eros.
              Etiam cursus, risus non rutrum fringilla, diam sem placerat ex, vel tincidunt urna justo eu odio. Vivamus imperdiet felis in pharetra rhoncus.
          </div>
          <div header="Etiam cursus, risus non rutrum fringilla" icon ="star">
              Minu suur content
          </div>
      </Accordion>
      <p></p>
      <AccordionNew colorScheme="light" size="medium" padding="false">
          <AccordionItem icon="star" header="Lorem ipsum dolor sit consectetur?"
               count="2"
              content="werwerw"
          ></AccordionItem>
          <AccordionItem icon="star" header="Lorem ipsum dolor sit consectetur?"
                         count="2"
                         content="werwerw"
          ></AccordionItem>
          <AccordionItem icon="star" header="Etiam cursus, risus non rutrum fringilla"
                         count="3"
                         content="werwerw"
          ></AccordionItem>
      </AccordionNew>
      <div> <h1>Inline Accordion</h1> </div>
      <InlineAccordionItem
          text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam felis mauris, convallis a augue rhoncus, molestie euismod velit. Ut sem tortor, condimentum quis scelerisque a, interdum sed velit. Aliquam bibendum tellus et posuere sollicitudin. Praesent laoreet vitae enim nec elementum"
          header="Lorem ipsum"
      />
      <InlineAccordionItem
          text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam felis mauris, convallis a augue rhoncus, molestie euismod velit. Ut sem tortor, condimentum quis scelerisque a, interdum sed velit. Aliquam bibendum tellus et posuere sollicitudin. Praesent laoreet vitae enim nec elementum"
          header="teine"
      />
  </React.StrictMode>
);

