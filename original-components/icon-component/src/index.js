import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Icon } from "./components/Icon/Icon";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <div>
    <table>
      <tbody>
        <tr>
          <td colSpan={3}>
            <h3>Location icon</h3>
          </td>
          <td colSpan={3}>
            <h3>Notice icon</h3>
          </td>
        </tr>
        <tr>
          <td>
            <Icon name={"location"} size={18} />
          </td>
          <td>
            <Icon name={"location"} size={24} />
          </td>
          <td>
            <Icon name={"location"} size={30} />
          </td>
          <td>
            <Icon name={"notice"} size={18} />
          </td>
          <td>
            <Icon name={"notice"} size={24} />
          </td>
          <td>
            <Icon name={"notice"} size={30} />
          </td>
        </tr>
        <tr>
          <td>18px </td>
          <td>24px</td>
          <td>30px</td>
          <td>18px</td>
          <td>24px</td>
          <td>30px</td>
        </tr>
      </tbody>
    </table>

    <h3>Sound</h3>
    <table>
      <tbody>
        <tr>
          <td><Icon name={"State=on_7"} /></td>
          <td><Icon name={"State=off_7"} /></td>
        </tr>
        <tr>
          <td>State=on_7</td>
          <td>State=off_7</td>
        </tr>
      </tbody>
    </table>
    
    
  </div>
);
