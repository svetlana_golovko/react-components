import React from 'react';

import { Icon } from './Icon';

export default {
  title: 'Icon',
  component: Icon,

};

const Template = (args) => <Icon {...args} />;

export const Default = Template.bind({});

Default.args = {
  name: "location"
};

export const WithSize = Template.bind({});

WithSize.args = {
  name: "home",
  size: 24
};