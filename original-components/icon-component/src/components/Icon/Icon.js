import styled from "styled-components";
import { useRef, useState, useEffect } from "react";

export const IconStyled = styled.img`
  object-fit: contain;
  flex-grow: 0;
  width: ${(props) => (props.size ? props.size : 18)}px;
  height: ${(props) => (props.size ? props.size : 18)}px;
`;

export function Icon(props) {
  const { name, size } = props;
  let options = {};

  const ImportedIconRef = useRef();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const { onCompleted, onError } = options;
  useEffect(() => {
    setLoading(true);
    const importIcon = async () => {
      try {
        ImportedIconRef.current = (
          await import(`../../icons/${name}.svg`)
        ).default;
        if (onCompleted) {
          onCompleted(name, ImportedIconRef.current);
        }
      } catch (err) {
        if (onError) {
          onError(err);
        }
        setError(err);
      } finally {
        setLoading(false);
      }
    };
    importIcon();
  }, [name, onCompleted, onError]);
console.log(ImportedIconRef)
  return (
    <>
      <IconStyled size={size} src={`/icons/${name}.svg`} />
      <div ref={ImportedIconRef}/>
    </>
  );
}
/*
function useDynamicSVGImport(name, options = {}) {
  const ImportedIconRef = useRef();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const { onCompleted, onError } = options;
  useEffect(() => {
    setLoading(true);
    const importIcon = async () => {
      try {
        ImportedIconRef.current = (
          await require(`../../icons/${name}.svg`)
        ).default;
        if (onCompleted) {
          onCompleted(name, ImportedIconRef.current);
        }
      } catch (err) {
        if (onError) {
          onError(err);
        }
        setError(err);
      } finally {
        setLoading(false);
      }
    };
    importIcon();
  }, [name, onCompleted, onError]);

  return { error, loading, SvgIcon: ImportedIconRef.current };
}*/
