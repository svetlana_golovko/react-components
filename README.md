# React training projects

This repository consists of 4 different projects:


- ## components

    Project to create a webpage example using made components and layout to show how all components can be used when creating a page. Project uses React, Sass and Storybook.

- ## html-grids

    Project to display different possibilites of creating a grid layout using bootstrap, CSS flex and grid.

- ## original-components

    Project to create different components using React, styled-components and Storybook.
    Some components use Sass and styled-components to test what was better to use.

- ## storybook

    Assignment to create a Grid component using React, SASS and Storybook. Babel and Webpack are used to create a distributable package.
    Created a Grid component that can be used for webpage layouts and display its capabilities in Storybook.