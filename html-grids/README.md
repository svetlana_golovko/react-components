# HTML grids

This project shows grid component using different methods. There are three different methods used: CSS grid, CSS flex and Bootstrap.

The goal was to create a responsive grid component that has different amount of columns when the screen size changes.
Also the possibilty of adding a custom property to make the last row's elements fill out remaining space if needed.

## Opening project

Three seperate folders for each component. 

To display the grid component open the `.html` file in the folder.

- Grid - `GridComponent.html` 
- Flex - `GridComponentFlex.html`
- Bootstrap - `GridComponentBootstrap.html`

### Grid
Each column width is calculated depending on the number of elements.
### Flex
For each screen size there are is column number and it calculates the column min-width. 
### Bootstrap 
Uses Bootstrap styles. Screen breakpoints and grid gutter sizes are set through `custom.scss` file to override Bootstraps set sizes.

## Pros and Cons of each grid method

**Grid**
- Last row cell width should be calculated according to cell position and number of cells in the last row. 
- Calculations do not work correctly for all number of columns. For example for 12 columns last cell of last row not always at correct place. 
+ Code is too complicated and difficult to maintain.
+ If last row cells should not be justified then all columns and rows set in correct and grid mode.  

**Flex**
- Ability to change dynamically elements width on the last row if parameter is given.
+ Code is simple and easy understandable.

**Bootstrap**
- Didn't find the ability to change dynamically elements width on the last row.
- Bootstrap default screen sizes and spacers should be redefined in .scss file. Then .scss file should be transformed to .css file with sass tool.
+ If last row cells should not be justified then most of the settings done in html and css file contains gap sizes for different screen sizes. 


### Making changes to bootstrap

Bootstrap uses scss for styling and needs to be recompiled with sass when changes are made to the .scss file.

To install sass:`
 ```
 npm i -g sass
 ```

To run sass from the command line go to the designated folder and enter the following on the command: 
```
\scss\custom.scss .\bootstrap\dist\css\custom.css
```