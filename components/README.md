# React webpage

Project to create React components, layouts and a webpage that uses all of the above mentioned. Uses React, SASS, Storybook and Grid component from the [storybook](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/) project as a local depenency.

## Structure

- `/assets`

    Contains all styles, fonts and other required files. 
 Each component and layout has a `.scss` file and a corresponding mixin file if needed in `/styles/abstracts/mixins`.

 - `/components`
 
    Components with Storybook stories

 - `/layouts`

    Has all the page header, footer and body layouts. And a Main file that uses all the layouts to create the overall layout of a page. 

- `/pages`

    Has the webpage example file

The Grid component is used as a local dependency. If changes are made to the Grid component in `storybook` project then `npm run dist` must be run in that project to update the bundle that this project uses.

## Run the project

```
npm i
npm run storybook   // runs Storybook to show components
npm start           // runs webpage locally
```
