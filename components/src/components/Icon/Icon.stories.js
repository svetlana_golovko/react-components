import React from "react";

import { Icon } from "./Icon";

export default {
  title: "Components/Icon",
  component: Icon,
  argTypes: {
    color: {
      control: {
        type: "color",
      },
    },
  },
};

const Template = (args) => <Icon {...args} />;

const Primary = Template.bind({});
Primary.args = {
  name: "location",
  color: "#004152",
  size: 18
}

export { Primary };