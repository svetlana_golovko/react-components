import PropTypes from "prop-types";
import { iconsMap } from "./IconMap";

const Icon = (props) => {
  const { name } = props;
  const SVG = iconsMap[name] || iconsMap["iconMissing"];

  return <SVG className={BEM(props)} style={setStyleVariables(props)} />;
};

function setStyleVariables(props) {
  return {
    "--size": props.size,
    "--color": props.color,
  };
}

const BEM = (props) => {
  const { alert, className } = props;
  const classArray = ["icon"];

  alert && classArray.push(`icon--${alert}`);
  className && classArray.push(className);
  return classArray.join(" ");
};

Icon.propTypes = {
  color: PropTypes.string,
  name: PropTypes.string,
  size: PropTypes.number,
};

Icon.defaultProps = {
  color: null,
  name: null,
  size: 18,
};

export { Icon };
