import { ReactComponent as AddFillOff } from "../../assets/icons/Action=add, Fill=off.svg";
import { ReactComponent as AddFillOn } from "../../assets/icons/Action=add, Fill=on.svg";
import { ReactComponent as Add } from "../../assets/icons/Action=add.svg";
import { ReactComponent as AddedFillOff } from "../../assets/icons/Action=added, Fill=off.svg";
import { ReactComponent as AddedFillOn } from "../../assets/icons/Action=added, Fill=on.svg";
import { ReactComponent as Added } from "../../assets/icons/Action=added.svg";
import { ReactComponent as Delete } from "../../assets/icons/Action=delete.svg";
import { ReactComponent as RemoveAllFillOff } from "../../assets/icons/Action=remove all, Fill=off.svg";
import { ReactComponent as RemoveAllFillOn } from "../../assets/icons/Action=remove all, Fill=on.svg";
import { ReactComponent as RemoveAll } from "../../assets/icons/Action=remove all.svg";
import { ReactComponent as RemoveFillOff } from "../../assets/icons/Action=remove, Fill=off.svg";
import { ReactComponent as RemoveFillOn } from "../../assets/icons/Action=remove, Fill=on.svg";
import { ReactComponent as Remove } from "../../assets/icons/Action=remove.svg";
import { ReactComponent as AddRemoveRound } from "../../assets/icons/add-remove round.svg";
import { ReactComponent as AddRemoveRound2 } from "../../assets/icons/add-remove round_2.svg";
import { ReactComponent as AddRemove } from "../../assets/icons/add-remove.svg";
import { ReactComponent as AgeGroupAdult } from "../../assets/icons/Age group=adult.svg";
import { ReactComponent as AgeGroupChild } from "../../assets/icons/Age group=child.svg";
import { ReactComponent as AgeGroupJunior } from "../../assets/icons/Age group=junior.svg";
import { ReactComponent as AgeGroupPassangers } from "../../assets/icons/Age group=passengers.svg";
import { ReactComponent as AgeGroupYouth } from "../../assets/icons/Age group=youth.svg";
import { ReactComponent as Allergy } from "../../assets/icons/allergy.svg";
import { ReactComponent as Anchor } from "../../assets/icons/anchor.svg";
import { ReactComponent as ArrowFunctional } from "../../assets/icons/arrow functional.svg";
import { ReactComponent as ArrowFunctional2 } from "../../assets/icons/arrow functional_2.svg";
import { ReactComponent as ArrowFunctional3 } from "../../assets/icons/arrow functional_3.svg";
import { ReactComponent as ArrowLeft } from "../../assets/icons/arrow-left.svg";
import { ReactComponent as ArrowRight } from "../../assets/icons/arrow-right.svg";
import { ReactComponent as Arrow } from "../../assets/icons/arrow.svg";
import { ReactComponent as Arrow10 } from "../../assets/icons/arrow_10.svg";
import { ReactComponent as Arrow11 } from "../../assets/icons/arrow_11.svg";
import { ReactComponent as Arrow12 } from "../../assets/icons/arrow_12.svg";
import { ReactComponent as Arrow13 } from "../../assets/icons/arrow_13.svg";
import { ReactComponent as Arrow14 } from "../../assets/icons/arrow_14.svg";
import { ReactComponent as Arrow15 } from "../../assets/icons/arrow_15.svg";
import { ReactComponent as Arrow16 } from "../../assets/icons/arrow_16.svg";
import { ReactComponent as Arrow17 } from "../../assets/icons/arrow_17.svg";
import { ReactComponent as Arrow18 } from "../../assets/icons/arrow_18.svg";
import { ReactComponent as Arrow19 } from "../../assets/icons/arrow_19.svg";
import { ReactComponent as Arrow2 } from "../../assets/icons/arrow_2.svg";
import { ReactComponent as Arrow20 } from "../../assets/icons/arrow_20.svg";
import { ReactComponent as Arrow21 } from "../../assets/icons/arrow_21.svg";
import { ReactComponent as Arrow22 } from "../../assets/icons/arrow_22.svg";
import { ReactComponent as Arrow23 } from "../../assets/icons/arrow_23.svg";
import { ReactComponent as Arrow24 } from "../../assets/icons/arrow_24.svg";
import { ReactComponent as Arrow25 } from "../../assets/icons/arrow_25.svg";
import { ReactComponent as Arrow26 } from "../../assets/icons/arrow_26.svg";
import { ReactComponent as Arrow27 } from "../../assets/icons/arrow_27.svg";
import { ReactComponent as Arrow28 } from "../../assets/icons/arrow_28.svg";
import { ReactComponent as Arrow29 } from "../../assets/icons/arrow_29.svg";
import { ReactComponent as Arrow3 } from "../../assets/icons/arrow_3.svg";
import { ReactComponent as Arrow30 } from "../../assets/icons/arrow_30.svg";
import { ReactComponent as Arrow31 } from "../../assets/icons/arrow_31.svg";
import { ReactComponent as Arrow32 } from "../../assets/icons/arrow_32.svg";
import { ReactComponent as Arrow33 } from "../../assets/icons/arrow_33.svg";
import { ReactComponent as Arrow34 } from "../../assets/icons/arrow_34.svg";
import { ReactComponent as Arrow35 } from "../../assets/icons/arrow_35.svg";
import { ReactComponent as Arrow4 } from "../../assets/icons/arrow_4.svg";
import { ReactComponent as Arrow5 } from "../../assets/icons/arrow_5.svg";
import { ReactComponent as Arrow6 } from "../../assets/icons/arrow_6.svg";
import { ReactComponent as Arrow7 } from "../../assets/icons/arrow_7.svg";
import { ReactComponent as Arrow8 } from "../../assets/icons/arrow_8.svg";
import { ReactComponent as Arrow9 } from "../../assets/icons/arrow_9.svg";
import { ReactComponent as At } from "../../assets/icons/at.svg";
import { ReactComponent as Attachment } from "../../assets/icons/attachment.svg";
import { ReactComponent as AvailabilityAvailableFilledOff } from "../../assets/icons/Availability=available, Filled=off.svg";
import { ReactComponent as AvailabilityAvailableFilledOn } from "../../assets/icons/Availability=available, Filled=on.svg";
import { ReactComponent as AvailabilityNotAvailableFilledOff } from "../../assets/icons/Availability=not available, Filled=off.svg";
import { ReactComponent as AvailabilityNotAvailableFilledOn } from "../../assets/icons/Availability=not available, Filled=on.svg";
import { ReactComponent as Bag } from "../../assets/icons/bag.svg";
import { ReactComponent as Blog } from "../../assets/icons/blog.svg";
import { ReactComponent as CheckWithBg } from "../../assets/icons/check w bg.svg";
import { ReactComponent as City } from "../../assets/icons/city.svg";
import { ReactComponent as Close } from "../../assets/icons/close.svg";
import { ReactComponent as ClubOne } from "../../assets/icons/club one.svg";
import { ReactComponent as Conference } from "../../assets/icons/conference.svg";
import { ReactComponent as Confirmation } from "../../assets/icons/confirmation.svg";
import { ReactComponent as Cursor } from "../../assets/icons/cursor.svg";
import { ReactComponent as DirectionDirection5 } from "../../assets/icons/Direction=Direction5.svg";
import { ReactComponent as DirectionDown } from "../../assets/icons/Direction=down.svg";
import { ReactComponent as DirectionLeft } from "../../assets/icons/Direction=left.svg";
import { ReactComponent as DirectionRight } from "../../assets/icons/Direction=right.svg";
import { ReactComponent as DirectionUp } from "../../assets/icons/Direction=up.svg";
import { ReactComponent as Discount } from "../../assets/icons/discount.svg";
import { ReactComponent as Discount2 } from "../../assets/icons/discount_2.svg";
import { ReactComponent as Document } from "../../assets/icons/document.svg";
import { ReactComponent as Edit } from "../../assets/icons/edit.svg";
import { ReactComponent as Entertainment } from "../../assets/icons/entertainment.svg";
import { ReactComponent as Eye } from "../../assets/icons/eye.svg";
import { ReactComponent as Feedback } from "../../assets/icons/feedback.svg";
import { ReactComponent as Future } from "../../assets/icons/future.svg";
import { ReactComponent as GooglePlay } from "../../assets/icons/google play.svg";
import { ReactComponent as Hand } from "../../assets/icons/hand.svg";
import { ReactComponent as Handicap } from "../../assets/icons/handicap.svg";
import { ReactComponent as Hand2 } from "../../assets/icons/hand_2.svg";
import { ReactComponent as Hand3 } from "../../assets/icons/hand_3.svg";
import { ReactComponent as Home } from "../../assets/icons/home.svg";
import { ReactComponent as IconCopy } from "../../assets/icons/icon copy.svg";
import { ReactComponent as IconWithStatus } from "../../assets/icons/icon w status.svg";
import { ReactComponent as Icon1 } from "../../assets/icons/icon.svg";
import { ReactComponent as Icon2 } from "../../assets/icons/icon_2.svg";
import { ReactComponent as Icon3 } from "../../assets/icons/icon_3.svg";
import { ReactComponent as Icon4 } from "../../assets/icons/icon_4.svg";
import { ReactComponent as IconMissing } from "../../assets/icons/icon_missing.svg";
import { ReactComponent as Idea } from "../../assets/icons/idea.svg";
import { ReactComponent as Internet } from "../../assets/icons/internet.svg";
import { ReactComponent as Key } from "../../assets/icons/key.svg";
import { ReactComponent as Kids } from "../../assets/icons/kids.svg";
import { ReactComponent as Lipstick } from "../../assets/icons/lipstick.svg";
import { ReactComponent as Location } from "../../assets/icons/location.svg";
import { ReactComponent as Menu } from "../../assets/icons/menu.svg";
import { ReactComponent as Money } from "../../assets/icons/money.svg";
import { ReactComponent as Moon } from "../../assets/icons/moon.svg";
import { ReactComponent as Notice } from "../../assets/icons/notice.svg";
import { ReactComponent as OnOff } from "../../assets/icons/on off.svg";
import { ReactComponent as OutlineOff } from "../../assets/icons/Outline=off.svg";
import { ReactComponent as OutlineOff2 } from "../../assets/icons/Outline=off_2.svg";
import { ReactComponent as OutlineOn } from "../../assets/icons/Outline=on.svg";
import { ReactComponent as OutlineOn2 } from "../../assets/icons/Outline=on_2.svg";
import { ReactComponent as Parking } from "../../assets/icons/parking.svg";
import { ReactComponent as Pets } from "../../assets/icons/pets.svg";
import { ReactComponent as PhoneAuth } from "../../assets/icons/phone auth.svg";
import { ReactComponent as PhoneLandline } from "../../assets/icons/phone landline.svg";
import { ReactComponent as Phone } from "../../assets/icons/phone.svg";
import { ReactComponent as Phone2 } from "../../assets/icons/phone_2.svg";
import { ReactComponent as PriceTag } from "../../assets/icons/price tag.svg";
import { ReactComponent as Printer } from "../../assets/icons/printer.svg";
import { ReactComponent as Property1Filled } from "../../assets/icons/Property 1=filled.svg";
import { ReactComponent as Property1Outlined } from "../../assets/icons/Property 1=outlined.svg";
import { ReactComponent as PropertyBackToTop } from "../../assets/icons/Property=back to top.svg";
import { ReactComponent as PropertyClear } from "../../assets/icons/Property=clear.svg";
import { ReactComponent as PropertyDownload } from "../../assets/icons/Property=download.svg";
import { ReactComponent as PropertyEnter } from "../../assets/icons/Property=enter.svg";
import { ReactComponent as PropertyExit } from "../../assets/icons/Property=exit.svg";
import { ReactComponent as PropertyShare } from "../../assets/icons/Property=share.svg";
import { ReactComponent as Proximity } from "../../assets/icons/proximity.svg";
import { ReactComponent as Question } from "../../assets/icons/question.svg";
import { ReactComponent as Refresh } from "../../assets/icons/refresh.svg";
import { ReactComponent as Reminder } from "../../assets/icons/reminder.svg";
import { ReactComponent as Restaurant } from "../../assets/icons/restaurant.svg";
import { ReactComponent as RoundTrip } from "../../assets/icons/round trip.svg";
import { ReactComponent as Save } from "../../assets/icons/save.svg";
import { ReactComponent as Search } from "../../assets/icons/search.svg";
import { ReactComponent as Search2 } from "../../assets/icons/search_2.svg";
import { ReactComponent as Settings } from "../../assets/icons/settings.svg";
import { ReactComponent as Settings2 } from "../../assets/icons/settings_2.svg";
import { ReactComponent as ShapeRegularDirectionDown } from "../../assets/icons/Shape=regular, Direction=down.svg";
import { ReactComponent as ShapeRegularDirectionLeft } from "../../assets/icons/Shape=regular, Direction=left.svg";
import { ReactComponent as ShapeRegularDirectionRight } from "../../assets/icons/Shape=regular, Direction=right.svg";
import { ReactComponent as ShapeRegularDirectionUp } from "../../assets/icons/Shape=regular, Direction=up.svg";
import { ReactComponent as ShapeSlimDirectionDown } from "../../assets/icons/Shape=slim, Direction=down.svg";
import { ReactComponent as ShapeSlimDirectionLeft } from "../../assets/icons/Shape=slim, Direction=left.svg";
import { ReactComponent as ShapeSlimDirectionRight } from "../../assets/icons/Shape=slim, Direction=right.svg";
import { ReactComponent as ShapeSlimDirectionUp } from "../../assets/icons/Shape=slim, Direction=up.svg";
import { ReactComponent as ShowOff } from "../../assets/icons/Show=off.svg";
import { ReactComponent as ShowOn } from "../../assets/icons/Show=on.svg";
import { ReactComponent as Sightseeing } from "../../assets/icons/sightseeing.svg";
import { ReactComponent as Signal } from "../../assets/icons/signal.svg";
import { ReactComponent as Size } from "../../assets/icons/size.svg";
import { ReactComponent as SizeRegularVehicleBicycle } from "../../assets/icons/Size=regular, Vehicle=bicycle.svg";
import { ReactComponent as SizeRegularVehicleBicycle2 } from "../../assets/icons/Size=regular, Vehicle=bicycle_2.svg";
import { ReactComponent as SizeRegularVehicleCarlarge } from "../../assets/icons/Size=regular, Vehicle=car large.svg";
import { ReactComponent as SizeRegularVehicleCarLarge2 } from "../../assets/icons/Size=regular, Vehicle=car large_2.svg";
import { ReactComponent as SizeRegularVehicleCarParking } from "../../assets/icons/Size=regular, Vehicle=car parking.svg";
import { ReactComponent as SizeRegularVehicleCarParking2 } from "../../assets/icons/Size=regular, Vehicle=car parking_2.svg";
import { ReactComponent as SizeRegularVehicleCarShopping } from "../../assets/icons/Size=regular, Vehicle=car shopping.svg";
import { ReactComponent as SizeRegularVehicleCarShopping2 } from "../../assets/icons/Size=regular, Vehicle=car shopping_2.svg";
import { ReactComponent as SizeRegularVehicleCar } from "../../assets/icons/Size=regular, Vehicle=car.svg";
import { ReactComponent as SizeRegularVehicleCar2 } from "../../assets/icons/Size=regular, Vehicle=car_2.svg";
import { ReactComponent as SizeRegularVehicleMotorcycleWithSidecar } from "../../assets/icons/Size=regular, Vehicle=motorcycle w sidecar.svg";
import { ReactComponent as SizeRegularVehicleMotorcycleWithSidecar2 } from "../../assets/icons/Size=regular, Vehicle=motorcycle w sidecar_2.svg";
import { ReactComponent as SizeRegularVehicleMotorcycle } from "../../assets/icons/Size=regular, Vehicle=motorcycle.svg";
import { ReactComponent as SizeRegularVehicleMotorcycle2 } from "../../assets/icons/Size=regular, Vehicle=motorcycle_2.svg";
import { ReactComponent as SizeRegularVehicleNoVehicle } from "../../assets/icons/Size=regular, Vehicle=no vehicle.svg";
import { ReactComponent as SizeRegularVehicleVanHigh } from "../../assets/icons/Size=regular, Vehicle=van high.svg";
import { ReactComponent as SizeRegularVehicleVanHigh2 } from "../../assets/icons/Size=regular, Vehicle=van high_2.svg";
import { ReactComponent as SizeRegularVehicleVanParking } from "../../assets/icons/Size=regular, Vehicle=van parking.svg";
import { ReactComponent as SizeRegularVehicleVanParking2 } from "../../assets/icons/Size=regular, Vehicle=van parking_2.svg";
import { ReactComponent as SizeRegularVehicleVanShopping } from "../../assets/icons/Size=regular, Vehicle=van shopping.svg";
import { ReactComponent as SizeRegularVehicleVanShopping2 } from "../../assets/icons/Size=regular, Vehicle=van shopping_2.svg";
import { ReactComponent as SizeRegularVehicleVan } from "../../assets/icons/Size=regular, Vehicle=van.svg";
import { ReactComponent as SizeRegularVehicleVan2 } from "../../assets/icons/Size=regular, Vehicle=van_2.svg";
import { ReactComponent as SizeWideVehicleBicycle } from "../../assets/icons/Size=wide, Vehicle=bicycle.svg";
import { ReactComponent as SizeWideVehicleBicycle_2 } from "../../assets/icons/Size=wide, Vehicle=bicycle_2.svg";
import { ReactComponent as SizeWideVehicleCarLarge } from "../../assets/icons/Size=wide, Vehicle=car large.svg";
import { ReactComponent as SizeWideVehicleCarLarge2 } from "../../assets/icons/Size=wide, Vehicle=car large_2.svg";
import { ReactComponent as SizeWideVehicleCarparking } from "../../assets/icons/Size=wide, Vehicle=car parking.svg";
import { ReactComponent as SizeWideVehicleCarparking2 } from "../../assets/icons/Size=wide, Vehicle=car parking_2.svg";
import { ReactComponent as SizeWideVehicleCarshopping } from "../../assets/icons/Size=wide, Vehicle=car shopping.svg";
import { ReactComponent as SizeWideVehicleCarshopping2 } from "../../assets/icons/Size=wide, Vehicle=car shopping_2.svg";
import { ReactComponent as SizeWideVehicleCarWithTrailer } from "../../assets/icons/Size=wide, Vehicle=car w trailer.svg";
import { ReactComponent as SizeWideVehicleCarWithTrailer2 } from "../../assets/icons/Size=wide, Vehicle=car w trailer_2.svg";
import { ReactComponent as SizeWideVehicleCar } from "../../assets/icons/Size=wide, Vehicle=car.svg";
import { ReactComponent as SizeWideVehicleCar2 } from "../../assets/icons/Size=wide, Vehicle=car_2.svg";
import { ReactComponent as SizeWideVehicleMotorcycleWithSidecar } from "../../assets/icons/Size=wide, Vehicle=motorcycle w sidecar.svg";
import { ReactComponent as SizeWideVehicleMotorcycleWithSidecar2 } from "../../assets/icons/Size=wide, Vehicle=motorcycle w sidecar_2.svg";
import { ReactComponent as SizeWideVehicleMotorcycle } from "../../assets/icons/Size=wide, Vehicle=motorcycle.svg";
import { ReactComponent as SizeWideVehicleMotorcycle2 } from "../../assets/icons/Size=wide, Vehicle=motorcycle_2.svg";
import { ReactComponent as SizeWideVehicleNoVehicle } from "../../assets/icons/Size=wide, Vehicle=no vehicle.svg";
import { ReactComponent as SizeWideVehicleVanHighWithTrailer } from "../../assets/icons/Size=wide, Vehicle=van high w trailer.svg";
import { ReactComponent as SizeWideVehicleVanHighWithTrailer2 } from "../../assets/icons/Size=wide, Vehicle=van high w trailer_2.svg";
import { ReactComponent as SizeWideVehicleVanHigh } from "../../assets/icons/Size=wide, Vehicle=van high.svg";
import { ReactComponent as SizeWideVehicleVanHigh2 } from "../../assets/icons/Size=wide, Vehicle=van high_2.svg";
import { ReactComponent as SizeWideVehicleVanParking } from "../../assets/icons/Size=wide, Vehicle=van parking.svg";
import { ReactComponent as SizeWideVehicleVanParking2 } from "../../assets/icons/Size=wide, Vehicle=van parking_2.svg";
import { ReactComponent as SizeWideVehicleVanShopping } from "../../assets/icons/Size=wide, Vehicle=van shopping.svg";
import { ReactComponent as SizeWideVehicleVanShopping2 } from "../../assets/icons/Size=wide, Vehicle=van shopping_2.svg";
import { ReactComponent as SizeWideVehicleVanWithTrailer } from "../../assets/icons/Size=wide, Vehicle=van w trailer.svg";
import { ReactComponent as SizeWideVehicleVanWithTrailer2 } from "../../assets/icons/Size=wide, Vehicle=van w trailer_2.svg";
import { ReactComponent as SizeWideVehicleVan } from "../../assets/icons/Size=wide, Vehicle=van.svg";
import { ReactComponent as SizeWideVehicleVan2 } from "../../assets/icons/Size=wide, Vehicle=van_2.svg";
import { ReactComponent as Sofa } from "../../assets/icons/sofa.svg";
import { ReactComponent as SoldOut } from "../../assets/icons/sold out.svg";
import { ReactComponent as SoldOut10 } from "../../assets/icons/sold out_10.svg";
import { ReactComponent as SoldOut11 } from "../../assets/icons/sold out_11.svg";
import { ReactComponent as SoldOut12 } from "../../assets/icons/sold out_12.svg";
import { ReactComponent as SoldOut13 } from "../../assets/icons/sold out_13.svg";
import { ReactComponent as SoldOut14 } from "../../assets/icons/sold out_14.svg";
import { ReactComponent as SoldOut15 } from "../../assets/icons/sold out_15.svg";
import { ReactComponent as SoldOut16 } from "../../assets/icons/sold out_16.svg";
import { ReactComponent as SoldOut17 } from "../../assets/icons/sold out_17.svg";
import { ReactComponent as SoldOut18 } from "../../assets/icons/sold out_18.svg";
import { ReactComponent as SoldOut19 } from "../../assets/icons/sold out_19.svg";
import { ReactComponent as SoldOut2 } from "../../assets/icons/sold out_2.svg";
import { ReactComponent as SoldOut20 } from "../../assets/icons/sold out_20.svg";
import { ReactComponent as SoldOut21 } from "../../assets/icons/sold out_21.svg";
import { ReactComponent as SoldOut22 } from "../../assets/icons/sold out_22.svg";
import { ReactComponent as SoldOut23 } from "../../assets/icons/sold out_23.svg";
import { ReactComponent as SoldOut24 } from "../../assets/icons/sold out_24.svg";
import { ReactComponent as SoldOut25 } from "../../assets/icons/sold out_25.svg";
import { ReactComponent as SoldOut3 } from "../../assets/icons/sold out_3.svg";
import { ReactComponent as SoldOut4 } from "../../assets/icons/sold out_4.svg";
import { ReactComponent as SoldOut5 } from "../../assets/icons/sold out_5.svg";
import { ReactComponent as SoldOut6 } from "../../assets/icons/sold out_6.svg";
import { ReactComponent as SoldOut7 } from "../../assets/icons/sold out_7.svg";
import { ReactComponent as SoldOut8 } from "../../assets/icons/sold out_8.svg";
import { ReactComponent as SoldOut9 } from "../../assets/icons/sold out_9.svg";
import { ReactComponent as Spinner } from "../../assets/icons/spinner.svg";
import { ReactComponent as Spinner2 } from "../../assets/icons/spinner_2.svg";
import { ReactComponent as StateFlash } from "../../assets/icons/State=flash.svg";
import { ReactComponent as StateHappy } from "../../assets/icons/State=happy.svg";
import { ReactComponent as StateHappy2 } from "../../assets/icons/State=happy_2.svg";
import { ReactComponent as StateMoon } from "../../assets/icons/State=moon.svg";
import { ReactComponent as StateNeutral } from "../../assets/icons/State=neutral.svg";
import { ReactComponent as StateOff } from "../../assets/icons/State=off.svg";
import { ReactComponent as StateOff2 } from "../../assets/icons/State=off_2.svg";
import { ReactComponent as StateOff3 } from "../../assets/icons/State=off_3.svg";
import { ReactComponent as StateOff4 } from "../../assets/icons/State=off_4.svg";
import { ReactComponent as StateOff5 } from "../../assets/icons/State=off_5.svg";
import { ReactComponent as StateOff6 } from "../../assets/icons/State=off_6.svg";
import { ReactComponent as StateOff7 } from "../../assets/icons/State=off_7.svg";
import { ReactComponent as StateOff8 } from "../../assets/icons/State=off_8.svg";
import { ReactComponent as StateOff9 } from "../../assets/icons/State=off_9.svg";
import { ReactComponent as StateOn } from "../../assets/icons/State=on.svg";
import { ReactComponent as StateOn2 } from "../../assets/icons/State=on_2.svg";
import { ReactComponent as StateOn3 } from "../../assets/icons/State=on_3.svg";
import { ReactComponent as StateOn4 } from "../../assets/icons/State=on_4.svg";
import { ReactComponent as StateOn5 } from "../../assets/icons/State=on_5.svg";
import { ReactComponent as StateOn6 } from "../../assets/icons/State=on_6.svg";
import { ReactComponent as StateOn7 } from "../../assets/icons/State=on_7.svg";
import { ReactComponent as StateOn8 } from "../../assets/icons/State=on_8.svg";
import { ReactComponent as StateOn9 } from "../../assets/icons/State=on_9.svg";
import { ReactComponent as StateSad } from "../../assets/icons/State=sad.svg";
import { ReactComponent as StateSmile } from "../../assets/icons/State=smile.svg";
import { ReactComponent as StateStar } from "../../assets/icons/State=star.svg";
import { ReactComponent as StateSun } from "../../assets/icons/State=sun.svg";
import { ReactComponent as StateWind } from "../../assets/icons/State=wind.svg";
import { ReactComponent as Status } from "../../assets/icons/status.svg";
import { ReactComponent as StatusError } from "../../assets/icons/Status=Error.svg";
import { ReactComponent as StatusImportant } from "../../assets/icons/Status=Important.svg";
import { ReactComponent as StatusNoStatus } from "../../assets/icons/Status=No status.svg";
import { ReactComponent as StatusSuccess } from "../../assets/icons/Status=Success.svg";
import { ReactComponent as Status2 } from "../../assets/icons/status_2.svg";
import { ReactComponent as Status3 } from "../../assets/icons/status_3.svg";
import { ReactComponent as Subtract } from "../../assets/icons/Subtract.svg";
import { ReactComponent as Sun } from "../../assets/icons/sun.svg";
import { ReactComponent as Ticket } from "../../assets/icons/tickets.svg";
import { ReactComponent as TravelClass } from "../../assets/icons/travel class.svg";
import { ReactComponent as TraveL } from "../../assets/icons/travel.svg";
import { ReactComponent as TypeAztec } from "../../assets/icons/Type=aztec.svg";
import { ReactComponent as TypeBar } from "../../assets/icons/Type=bar.svg";
import { ReactComponent as TypeBus } from "../../assets/icons/Type=bus.svg";
import { ReactComponent as TypeConfirmation } from "../../assets/icons/Type=confirmation.svg";
import { ReactComponent as TypeDayColors1 } from "../../assets/icons/Type=day, Colors=1.svg";
import { ReactComponent as TypeDayColors2 } from "../../assets/icons/Type=day, Colors=2.svg";
import { ReactComponent as TypeDelivery } from "../../assets/icons/Type=delivery.svg";
import { ReactComponent as TypeError } from "../../assets/icons/Type=error.svg";
import { ReactComponent as TypeFilled } from "../../assets/icons/Type=filled.svg";
import { ReactComponent as TypeGeneric } from "../../assets/icons/Type=generic.svg";
import { ReactComponent as TypeImportant } from "../../assets/icons/Type=important.svg";
import { ReactComponent as TypeInfo } from "../../assets/icons/Type=info.svg";
import { ReactComponent as TypeJpg } from "../../assets/icons/Type=jpg.svg";
import { ReactComponent as TypeList } from "../../assets/icons/Type=list.svg";
import { ReactComponent as TypeMonthColors1 } from "../../assets/icons/Type=month, Colors=1.svg";
import { ReactComponent as TypeMoreInfo } from "../../assets/icons/Type=more info.svg";
import { ReactComponent as TypeOutline } from "../../assets/icons/Type=outline.svg";
import { ReactComponent as TypePdf } from "../../assets/icons/Type=pdf.svg";
import { ReactComponent as TypePng } from "../../assets/icons/Type=png.svg";
import { ReactComponent as TypeQr } from "../../assets/icons/Type=qr.svg";
import { ReactComponent as TypeShipFront } from "../../assets/icons/Type=ship front.svg";
import { ReactComponent as TypeShipSide } from "../../assets/icons/Type=ship side.svg";
import { ReactComponent as TypeSuccess } from "../../assets/icons/Type=success.svg";
import { ReactComponent as TypeTaxi } from "../../assets/icons/Type=taxi.svg";
import { ReactComponent as TypeThumbs } from "../../assets/icons/Type=thumbs.svg";
import { ReactComponent as TypeTxt } from "../../assets/icons/Type=txt.svg";
import { ReactComponent as TypeVehicle } from "../../assets/icons/Type=vehicle.svg";
import { ReactComponent as Umbrella } from "../../assets/icons/umbrella.svg";
import { ReactComponent as Union } from "../../assets/icons/Union.svg";
import { ReactComponent as Upgrade } from "../../assets/icons/upgrade.svg";
import { ReactComponent as Vacation } from "../../assets/icons/vacation.svg";
import { ReactComponent as VariantApple } from "../../assets/icons/Variant=apple.svg";
import { ReactComponent as VariantB2b } from "../../assets/icons/Variant=b2b.svg";
import { ReactComponent as VariantB2c } from "../../assets/icons/Variant=b2c.svg";
import { ReactComponent as VariantBurger } from "../../assets/icons/Variant=burger.svg";
import { ReactComponent as VariantDarkDirectionDownEnabledOff } from "../../assets/icons/Variant=dark, Direction=down, Enabled=off.svg";
import { ReactComponent as VariantDarkDirectionDownEnabledOn } from "../../assets/icons/Variant=dark, Direction=down, Enabled=on.svg";
import { ReactComponent as VariantDarkDirectionLeftEnabledOff } from "../../assets/icons/Variant=dark, Direction=left, Enabled=off.svg";
import { ReactComponent as VariantDarkDirectionLeftEnabledOn } from "../../assets/icons/Variant=dark, Direction=left, Enabled=on.svg";
import { ReactComponent as VariantDarkDirectionRightEnabledOff } from "../../assets/icons/Variant=dark, Direction=right, Enabled=off.svg";
import { ReactComponent as VariantDarkDirectionRightEnabledOn } from "../../assets/icons/Variant=dark, Direction=right, Enabled=on.svg";
import { ReactComponent as VariantDarkDirectionUpEnabledOff } from "../../assets/icons/Variant=dark, Direction=up, Enabled=off.svg";
import { ReactComponent as VariantDarkDirectionUpEnabledOn } from "../../assets/icons/Variant=dark, Direction=up, Enabled=on.svg";
import { ReactComponent as VariantEmailValidation } from "../../assets/icons/Variant=email validation.svg";
import { ReactComponent as VariantEmail } from "../../assets/icons/Variant=email.svg";
import { ReactComponent as VariantFacebook } from "../../assets/icons/Variant=facebook.svg";
import { ReactComponent as VariantFilled } from "../../assets/icons/Variant=filled.svg";
import { ReactComponent as VariantGiftCard } from "../../assets/icons/Variant=gift card.svg";
import { ReactComponent as VariantGoogle } from "../../assets/icons/Variant=google.svg";
import { ReactComponent as VariantHorisontal } from "../../assets/icons/Variant=horisontal.svg";
import { ReactComponent as VariantHorisontal2 } from "../../assets/icons/Variant=horisontal_2.svg";
import { ReactComponent as VariantInstagram } from "../../assets/icons/Variant=instagram.svg";
import { ReactComponent as VariantLightDirectionDownEnabledOff } from "../../assets/icons/Variant=light, Direction=down, Enabled=off.svg";
import { ReactComponent as VariantLightDirectionDownEnabledOn } from "../../assets/icons/Variant=light, Direction=down, Enabled=on.svg";
import { ReactComponent as VariantLightDirectionLeftEnabledOff } from "../../assets/icons/Variant=light, Direction=left, Enabled=off.svg";
import { ReactComponent as VariantLightDirectionLeftEnabledOn } from "../../assets/icons/Variant=light, Direction=left, Enabled=on.svg";
import { ReactComponent as VariantLightDirectionRightEnabledOff } from "../../assets/icons/Variant=light, Direction=right, Enabled=off.svg";
import { ReactComponent as VariantLightDirectionRightEnabledOn } from "../../assets/icons/Variant=light, Direction=right, Enabled=on.svg";
import { ReactComponent as VariantLightDirectionUpEnabledOff } from "../../assets/icons/Variant=light, Direction=up, Enabled=off.svg";
import { ReactComponent as VariantLightDirectionUpEnabledOn } from "../../assets/icons/Variant=light, Direction=up, Enabled=on.svg";
import { ReactComponent as VariantMail } from "../../assets/icons/Variant=mail.svg";
import { ReactComponent as VariantMinimal } from "../../assets/icons/Variant=minimal.svg";
import { ReactComponent as VariantMobiilivarmenne } from "../../assets/icons/Variant=mobiilivarmenne.svg";
import { ReactComponent as VariantMobileId } from "../../assets/icons/Variant=mobile-id.svg";
import { ReactComponent as VariantOnlineBank } from "../../assets/icons/Variant=online bank.svg";
import { ReactComponent as VariantOutline } from "../../assets/icons/Variant=outline.svg";
import { ReactComponent as VariantSmartId } from "../../assets/icons/Variant=smart-id.svg";
import { ReactComponent as VariantTextual } from "../../assets/icons/Variant=textual.svg";
import { ReactComponent as VariantTimesUp } from "../../assets/icons/Variant=times up.svg";
import { ReactComponent as VariantVertical } from "../../assets/icons/Variant=vertical.svg";
import { ReactComponent as VariantVertical2 } from "../../assets/icons/Variant=vertical_2.svg";
import { ReactComponent as VariantYoutube } from "../../assets/icons/Variant=youtube.svg";
import { ReactComponent as Vector } from "../../assets/icons/Vector.svg";
import { ReactComponent as Vector2 } from "../../assets/icons/Vector_2.svg";
import { ReactComponent as VehicleOptions } from "../../assets/icons/vehicle options.svg";
import { ReactComponent as VehicleOptions10 } from "../../assets/icons/vehicle options_10.svg";
import { ReactComponent as VehicleOptions11 } from "../../assets/icons/vehicle options_11.svg";
import { ReactComponent as VehicleOptions12 } from "../../assets/icons/vehicle options_12.svg";
import { ReactComponent as VehicleOptions13 } from "../../assets/icons/vehicle options_13.svg";
import { ReactComponent as VehicleOptions14 } from "../../assets/icons/vehicle options_14.svg";
import { ReactComponent as VehicleOptions15 } from "../../assets/icons/vehicle options_15.svg";
import { ReactComponent as VehicleOptions16 } from "../../assets/icons/vehicle options_16.svg";
import { ReactComponent as VehicleOptions17 } from "../../assets/icons/vehicle options_17.svg";
import { ReactComponent as VehicleOptions18 } from "../../assets/icons/vehicle options_18.svg";
import { ReactComponent as VehicleOptions19 } from "../../assets/icons/vehicle options_19.svg";
import { ReactComponent as VehicleOptions2 } from "../../assets/icons/vehicle options_2.svg";
import { ReactComponent as VehicleOptions20 } from "../../assets/icons/vehicle options_20.svg";
import { ReactComponent as VehicleOptions21 } from "../../assets/icons/vehicle options_21.svg";
import { ReactComponent as VehicleOptions22 } from "../../assets/icons/vehicle options_22.svg";
import { ReactComponent as VehicleOptions23 } from "../../assets/icons/vehicle options_23.svg";
import { ReactComponent as VehicleOptions24 } from "../../assets/icons/vehicle options_24.svg";
import { ReactComponent as VehicleOptions25 } from "../../assets/icons/vehicle options_25.svg";
import { ReactComponent as VehicleOptions26 } from "../../assets/icons/vehicle options_26.svg";
import { ReactComponent as VehicleOptions27 } from "../../assets/icons/vehicle options_27.svg";
import { ReactComponent as VehicleOptions3 } from "../../assets/icons/vehicle options_3.svg";
import { ReactComponent as VehicleOptions4 } from "../../assets/icons/vehicle options_4.svg";
import { ReactComponent as VehicleOptions5 } from "../../assets/icons/vehicle options_5.svg";
import { ReactComponent as VehicleOptions6 } from "../../assets/icons/vehicle options_6.svg";
import { ReactComponent as VehicleOptions7 } from "../../assets/icons/vehicle options_7.svg";
import { ReactComponent as VehicleOptions8 } from "../../assets/icons/vehicle options_8.svg";
import { ReactComponent as VehicleOptions9 } from "../../assets/icons/vehicle options_9.svg";
import { ReactComponent as Weather } from "../../assets/icons/weather.svg";
import { ReactComponent as WeatherActive } from "../../assets/icons/weather_active.svg";
import { ReactComponent as Wheel } from "../../assets/icons/wheel.svg";
import { ReactComponent as Window } from "../../assets/icons/window.svg";
import { ReactComponent as Wine } from "../../assets/icons/wine.svg";
import { ReactComponent as Wine2 } from "../../assets/icons/wine_2.svg";
import { ReactComponent as Zoom } from "../../assets/icons/zoom.svg";

const iconsMap = {
  addFillOff: AddFillOff,
  addFillOn: AddFillOn,
  add: Add,
  addedFillOff: AddedFillOff,
  addedFillOn: AddedFillOn,
  added: Added,
  delete: Delete,
  removeAllFillOff: RemoveAllFillOff,
  removeAllFillOn: RemoveAllFillOn,
  removeAll: RemoveAll,
  removeFillOff: RemoveFillOff,
  removeFillOn: RemoveFillOn,
  remove: Remove,
  addRemoveRound: AddRemoveRound,
  addRemoveRound2: AddRemoveRound2,
  addRemove: AddRemove,
  ageGroupAdult: AgeGroupAdult,
  ageGroupChild: AgeGroupChild,
  ageGroupJunior: AgeGroupJunior,
  ageGroupPassangers: AgeGroupPassangers,
  ageGroupYouth: AgeGroupYouth,
  allergy: Allergy,
  anchor: Anchor,
  arrowFunctional: ArrowFunctional,
  arrowFunctional2: ArrowFunctional2,
  arrowFunctional3: ArrowFunctional3,
  arrowLeft: ArrowLeft,
  arrowRight: ArrowRight,
  arrow: Arrow,
  arrow10: Arrow10,
  arrow11: Arrow11,
  arrow12: Arrow12,
  arrow13: Arrow13,
  arrow14: Arrow14,
  arrow15: Arrow15,
  arrow16: Arrow16,
  arrow17: Arrow17,
  arrow18: Arrow18,
  arrow19: Arrow19,
  arrow2: Arrow2,
  arrow20: Arrow20,
  arrow21: Arrow21,
  arrow22: Arrow22,
  arrow23: Arrow23,
  arrow24: Arrow24,
  arrow25: Arrow25,
  arrow26: Arrow26,
  arrow27: Arrow27,
  arrow28: Arrow28,
  arrow29: Arrow29,
  arrow3: Arrow3,
  arrow30: Arrow30,
  arrow31: Arrow31,
  arrow32: Arrow32,
  arrow33: Arrow33,
  arrow34: Arrow34,
  arrow35: Arrow35,
  arrow4: Arrow4,
  arrow5: Arrow5,
  arrow6: Arrow6,
  arrow7: Arrow7,
  arrow8: Arrow8,
  arrow9: Arrow9,
  at: At,
  attachment: Attachment,
  availabilityAvailableFilledOff: AvailabilityAvailableFilledOff,
  availabilityAvailableFilledOn: AvailabilityAvailableFilledOn,
  availabilityNotAvailableFilledOff: AvailabilityNotAvailableFilledOff,
  availabilityNotAvailableFilledOn: AvailabilityNotAvailableFilledOn,
  bag: Bag,
  blog: Blog,
  checkWithBg: CheckWithBg,
  city: City,
  close: Close,
  clubOne: ClubOne,
  conference: Conference,
  confirmation: Confirmation,
  cursor: Cursor,
  directionDirection5: DirectionDirection5,
  directionDown: DirectionDown,
  directionLeft: DirectionLeft,
  directionRight: DirectionRight,
  directionUp: DirectionUp,
  discount: Discount,
  discount2: Discount2,
  document: Document,
  edit: Edit,
  entertainment: Entertainment,
  eye: Eye,
  feedback: Feedback,
  future: Future,
  googlePlay: GooglePlay,
  hand: Hand,
  handicap: Handicap,
  hand2: Hand2,
  hand3: Hand3,
  home: Home,
  iconCopy: IconCopy,
  iconWithStatus: IconWithStatus,
  icon1: Icon1,
  icon2: Icon2,
  icon3: Icon3,
  icon4: Icon4,
  iconMissing: IconMissing,
  idea: Idea,
  internet: Internet,
  key: Key,
  kids: Kids,
  lipstick: Lipstick,
  location: Location,
  menu: Menu,
  money: Money,
  moon: Moon,
  notice: Notice,
  onOff: OnOff,
  outlineOff: OutlineOff,
  outlineOff2: OutlineOff2,
  outlineOn: OutlineOn,
  outlineOn2: OutlineOn2,
  parking: Parking,
  pets: Pets,
  phoneAuth: PhoneAuth,
  phoneLandline: PhoneLandline,
  phone: Phone,
  phone2: Phone2,
  priceTag: PriceTag,
  printer: Printer,
  property1Filled: Property1Filled,
  property1Outlined: Property1Outlined,
  propertyBackToTop: PropertyBackToTop,
  propertyClear: PropertyClear,
  propertyDownload: PropertyDownload,
  propertyEnter: PropertyEnter,
  propertyExit: PropertyExit,
  propertyShare: PropertyShare,
  proximity: Proximity,
  question: Question,
  refresh: Refresh,
  reminder: Reminder,
  restaurant: Restaurant,
  roundTrip: RoundTrip,
  save: Save,
  search: Search,
  search2: Search2,
  settings: Settings,
  settings2: Settings2,
  shapeRegularDirectionDown: ShapeRegularDirectionDown,
  shapeRegularDirectionLeft: ShapeRegularDirectionLeft,
  shapeRegularDirectionRight: ShapeRegularDirectionRight,
  shapeRegularDirectionUp: ShapeRegularDirectionUp,
  shapeSlimDirectionDown: ShapeSlimDirectionDown,
  shapeSlimDirectionLeft: ShapeSlimDirectionLeft,
  shapeSlimDirectionRight: ShapeSlimDirectionRight,
  shapeSlimDirectionUp: ShapeSlimDirectionUp,
  showOff: ShowOff,
  showOn: ShowOn,
  sightseeing: Sightseeing,
  signal: Signal,
  size: Size,
  sizeRegularVehicleBicycle: SizeRegularVehicleBicycle,
  sizeRegularVehicleBicycle2: SizeRegularVehicleBicycle2,
  sizeRegularVehicleCarlarge: SizeRegularVehicleCarlarge,
  sizeRegularVehicleCarLarge2: SizeRegularVehicleCarLarge2,
  sizeRegularVehicleCarParking: SizeRegularVehicleCarParking,
  sizeRegularVehicleCarParking2: SizeRegularVehicleCarParking2,
  sizeRegularVehicleCarShopping: SizeRegularVehicleCarShopping,
  sizeRegularVehicleCarShopping2: SizeRegularVehicleCarShopping2,
  sizeRegularVehicleCar: SizeRegularVehicleCar,
  sizeRegularVehicleCar2: SizeRegularVehicleCar2,
  sizeRegularVehicleMotorcycleWithSidecar:
    SizeRegularVehicleMotorcycleWithSidecar,
  sizeRegularVehicleMotorcycleWithSidecar2:
    SizeRegularVehicleMotorcycleWithSidecar2,
  sizeRegularVehicleMotorcycle: SizeRegularVehicleMotorcycle,
  sizeRegularVehicleMotorcycle2: SizeRegularVehicleMotorcycle2,
  sizeRegularVehicleNoVehicle: SizeRegularVehicleNoVehicle,
  sizeRegularVehicleVanHigh: SizeRegularVehicleVanHigh,
  sizeRegularVehicleVanHigh2: SizeRegularVehicleVanHigh2,
  sizeRegularVehicleVanParking: SizeRegularVehicleVanParking,
  sizeRegularVehicleVanParking2: SizeRegularVehicleVanParking2,
  sizeRegularVehicleVanShopping: SizeRegularVehicleVanShopping,
  sizeRegularVehicleVanShopping2: SizeRegularVehicleVanShopping2,
  sizeRegularVehicleVan: SizeRegularVehicleVan,
  sizeRegularVehicleVan2: SizeRegularVehicleVan2,
  sizeWideVehicleBicycle: SizeWideVehicleBicycle,
  sizeWideVehicleBicycle_2: SizeWideVehicleBicycle_2,
  sizeWideVehicleCarLarge: SizeWideVehicleCarLarge,
  sizeWideVehicleCarLarge2: SizeWideVehicleCarLarge2,
  sizeWideVehicleCarparking: SizeWideVehicleCarparking,
  sizeWideVehicleCarparking2: SizeWideVehicleCarparking2,
  sizeWideVehicleCarshopping: SizeWideVehicleCarshopping,
  sizeWideVehicleCarshopping2: SizeWideVehicleCarshopping2,
  sizeWideVehicleCarWithTrailer: SizeWideVehicleCarWithTrailer,
  sizeWideVehicleCarWithTrailer2: SizeWideVehicleCarWithTrailer2,
  sizeWideVehicleCar: SizeWideVehicleCar,
  sizeWideVehicleCar2: SizeWideVehicleCar2,
  sizeWideVehicleMotorcycleWithSidecar: SizeWideVehicleMotorcycleWithSidecar,
  sizeWideVehicleMotorcycleWithSidecar2: SizeWideVehicleMotorcycleWithSidecar2,
  sizeWideVehicleMotorcycle: SizeWideVehicleMotorcycle,
  sizeWideVehicleMotorcycle2: SizeWideVehicleMotorcycle2,
  sizeWideVehicleNoVehicle: SizeWideVehicleNoVehicle,
  sizeWideVehicleVanHighWithTrailer: SizeWideVehicleVanHighWithTrailer,
  sizeWideVehicleVanHighWithTrailer2: SizeWideVehicleVanHighWithTrailer2,
  sizeWideVehicleVanHigh: SizeWideVehicleVanHigh,
  sizeWideVehicleVanHigh2: SizeWideVehicleVanHigh2,
  sizeWideVehicleVanParking: SizeWideVehicleVanParking,
  sizeWideVehicleVanParking2: SizeWideVehicleVanParking2,
  sizeWideVehicleVanShopping: SizeWideVehicleVanShopping,
  sizeWideVehicleVanShopping2: SizeWideVehicleVanShopping2,
  sizeWideVehicleVanWithTrailer: SizeWideVehicleVanWithTrailer,
  sizeWideVehicleVanWithTrailer2: SizeWideVehicleVanWithTrailer2,
  sizeWideVehicleVan: SizeWideVehicleVan,
  sizeWideVehicleVan2: SizeWideVehicleVan2,
  sofa: Sofa,
  soldOut: SoldOut,
  soldOut10: SoldOut10,
  soldOut11: SoldOut11,
  soldOut12: SoldOut12,
  soldOut13: SoldOut13,
  soldOut14: SoldOut14,
  soldOut15: SoldOut15,
  soldOut16: SoldOut16,
  soldOut17: SoldOut17,
  soldOut18: SoldOut18,
  soldOut19: SoldOut19,
  soldOut2: SoldOut2,
  soldOut20: SoldOut20,
  soldOut21: SoldOut21,
  soldOut22: SoldOut22,
  soldOut23: SoldOut23,
  soldOut24: SoldOut24,
  soldOut25: SoldOut25,
  soldOut3: SoldOut3,
  soldOut4: SoldOut4,
  soldOut5: SoldOut5,
  soldOut6: SoldOut6,
  soldOut7: SoldOut7,
  soldOut8: SoldOut8,
  soldOut9: SoldOut9,
  spinner: Spinner,
  spinner2: Spinner2,
  stateFlash: StateFlash,
  stateHappy: StateHappy,
  stateHappy2: StateHappy2,
  stateMoon: StateMoon,
  stateNeutral: StateNeutral,
  stateOff: StateOff,
  stateOff2: StateOff2,
  stateOff3: StateOff3,
  stateOff4: StateOff4,
  stateOff5: StateOff5,
  stateOff6: StateOff6,
  stateOff7: StateOff7,
  stateOff8: StateOff8,
  stateOff9: StateOff9,
  stateOn: StateOn,
  stateOn2: StateOn2,
  stateOn3: StateOn3,
  stateOn4: StateOn4,
  stateOn5: StateOn5,
  stateOn6: StateOn6,
  stateOn7: StateOn7,
  stateOn8: StateOn8,
  stateOn9: StateOn9,
  stateSad: StateSad,
  stateSmile: StateSmile,
  stateStar: StateStar,
  stateSun: StateSun,
  stateWind: StateWind,
  status: Status,
  statusError: StatusError,
  statusImportant: StatusImportant,
  statusNoStatus: StatusNoStatus,
  statusSuccess: StatusSuccess,
  status2: Status2,
  status3: Status3,
  subtract: Subtract,
  sun: Sun,
  ticket: Ticket,
  travelClass: TravelClass,
  traveL: TraveL,
  typeAztec: TypeAztec,
  typeBar: TypeBar,
  typeBus: TypeBus,
  typeConfirmation: TypeConfirmation,
  typeDayColors1: TypeDayColors1,
  typeDayColors2: TypeDayColors2,
  typeDelivery: TypeDelivery,
  typeError: TypeError,
  typeFilled: TypeFilled,
  typeGeneric: TypeGeneric,
  typeImportant: TypeImportant,
  typeInfo: TypeInfo,
  typeJpg: TypeJpg,
  typeList: TypeList,
  typeMonthColors1: TypeMonthColors1,
  typeMoreInfo: TypeMoreInfo,
  typeOutline: TypeOutline,
  typePdf: TypePdf,
  typePng: TypePng,
  typeQr: TypeQr,
  typeShipFront: TypeShipFront,
  typeShipSide: TypeShipSide,
  typeSuccess: TypeSuccess,
  typeTaxi: TypeTaxi,
  typeThumbs: TypeThumbs,
  typeTxt: TypeTxt,
  typeVehicle: TypeVehicle,
  umbrella: Umbrella,
  union: Union,
  upgrade: Upgrade,
  vacation: Vacation,
  variantApple: VariantApple,
  variantB2b: VariantB2b,
  variantB2c: VariantB2c,
  variantBurger: VariantBurger,
  variantDarkDirectionDownEnabledOff: VariantDarkDirectionDownEnabledOff,
  variantDarkDirectionDownEnabledOn: VariantDarkDirectionDownEnabledOn,
  variantDarkDirectionLeftEnabledOff: VariantDarkDirectionLeftEnabledOff,
  variantDarkDirectionLeftEnabledOn: VariantDarkDirectionLeftEnabledOn,
  variantDarkDirectionRightEnabledOff: VariantDarkDirectionRightEnabledOff,
  variantDarkDirectionRightEnabledOn: VariantDarkDirectionRightEnabledOn,
  variantDarkDirectionUpEnabledOff: VariantDarkDirectionUpEnabledOff,
  variantDarkDirectionUpEnabledOn: VariantDarkDirectionUpEnabledOn,
  variantEmailValidation: VariantEmailValidation,
  variantEmail: VariantEmail,
  variantFacebook: VariantFacebook,
  variantFilled: VariantFilled,
  variantGiftCard: VariantGiftCard,
  variantGoogle: VariantGoogle,
  variantHorisontal: VariantHorisontal,
  variantHorisontal2: VariantHorisontal2,
  variantInstagram: VariantInstagram,
  variantLightDirectionDownEnabledOff: VariantLightDirectionDownEnabledOff,
  variantLightDirectionDownEnabledOn: VariantLightDirectionDownEnabledOn,
  variantLightDirectionLeftEnabledOff: VariantLightDirectionLeftEnabledOff,
  variantLightDirectionLeftEnabledOn: VariantLightDirectionLeftEnabledOn,
  variantLightDirectionRightEnabledOff: VariantLightDirectionRightEnabledOff,
  variantLightDirectionRightEnabledOn: VariantLightDirectionRightEnabledOn,
  variantLightDirectionUpEnabledOff: VariantLightDirectionUpEnabledOff,
  variantLightDirectionUpEnabledOn: VariantLightDirectionUpEnabledOn,
  variantMail: VariantMail,
  variantMinimal: VariantMinimal,
  variantMobiilivarmenne: VariantMobiilivarmenne,
  variantMobileId: VariantMobileId,
  variantOnlineBank: VariantOnlineBank,
  variantOutline: VariantOutline,
  variantSmartId: VariantSmartId,
  variantTextual: VariantTextual,
  variantTimesUp: VariantTimesUp,
  variantVertical: VariantVertical,
  variantVertical2: VariantVertical2,
  variantYoutube: VariantYoutube,
  vector: Vector,
  vector2: Vector2,
  vehicleOptions: VehicleOptions,
  vehicleOptions10: VehicleOptions10,
  vehicleOptions11: VehicleOptions11,
  vehicleOptions12: VehicleOptions12,
  vehicleOptions13: VehicleOptions13,
  vehicleOptions14: VehicleOptions14,
  vehicleOptions15: VehicleOptions15,
  vehicleOptions16: VehicleOptions16,
  vehicleOptions17: VehicleOptions17,
  vehicleOptions18: VehicleOptions18,
  vehicleOptions19: VehicleOptions19,
  vehicleOptions2: VehicleOptions2,
  vehicleOptions20: VehicleOptions20,
  vehicleOptions21: VehicleOptions21,
  vehicleOptions22: VehicleOptions22,
  vehicleOptions23: VehicleOptions23,
  vehicleOptions24: VehicleOptions24,
  vehicleOptions25: VehicleOptions25,
  vehicleOptions26: VehicleOptions26,
  vehicleOptions27: VehicleOptions27,
  vehicleOptions3: VehicleOptions3,
  vehicleOptions4: VehicleOptions4,
  vehicleOptions5: VehicleOptions5,
  vehicleOptions6: VehicleOptions6,
  vehicleOptions7: VehicleOptions7,
  vehicleOptions8: VehicleOptions8,
  vehicleOptions9: VehicleOptions9,
  weather: Weather,
  weatherActive: WeatherActive,
  wheel: Wheel,
  window: Window,
  wine: Wine,
  wine2: Wine2,
  zoom: Zoom,
};

export { iconsMap };
