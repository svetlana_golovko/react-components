import PropTypes from "prop-types";
import React from "react";

const Sidebar = (props) => {
  const { children, sticky, ...rest } = props;

  return (
    <div {...rest} className={BEM(props)}>
      <ul className="sidebar__inner">{children}</ul>
    </div>
  );
};

const BEM = (props) => {
  const { className, sticky } = props;
  const classArray = ["sidebar"];

  className && classArray.push(className);
  sticky && classArray.push("sidebar--sticky");

  return classArray.join(" ");
};

Sidebar.propTypes = {
  className: PropTypes.string,
  sticky: PropTypes.bool,
};

Sidebar.defaultProps = {
  className: null,
  sticky: false,
};

export { Sidebar };
