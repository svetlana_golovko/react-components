import PropTypes from "prop-types";
import React from "react";

const SidebarItem = (props) => {
  const { children } = props;
  return <li className={BEM(props)}>{children}</li>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["sidebar__item"];

  className && classArray.push(className);

  return classArray.join(" ");
};

SidebarItem.propTypes = {
  className: PropTypes.string,
};

SidebarItem.defaultProps = {
  className: null,
};

export { SidebarItem };
