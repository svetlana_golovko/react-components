import React from "react";

import { Sidebar } from "./Sidebar";
import { SidebarItem } from "./SidebarItem";
import { Button } from "../Button/Button";

export default {
  component: Sidebar,
  title: "Components/Sidebar",
};

const TemplatePrimary = (args) => (
  <Sidebar>
    <SidebarItem>
      <Button variant={"empty"} size={"tiny"}>
        Home
      </Button>
    </SidebarItem>
    <SidebarItem>
      <Button variant={"empty"} size={"tiny"}>
        News
      </Button>
    </SidebarItem>
    <SidebarItem>
      <Button variant={"empty"} size={"tiny"}>
        Contact
      </Button>
    </SidebarItem>
    <SidebarItem>
      <Button variant={"empty"} size={"tiny"}>
        About
      </Button>
    </SidebarItem>
  </Sidebar>
);

const Primary = TemplatePrimary.bind({});
Primary.args = {};

export { Primary };
