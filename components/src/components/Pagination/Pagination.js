import PropTypes from "prop-types";
import { Icon } from "../Icon/Icon";

const Pagination = (props) => {
  return (
    <div className={BEM(props)}>
      <div className="pagination__container">
        <ul className="pagination__items">
          {[...Array(5)].map((item, index) => (
            <li key={index} />
          ))}
        </ul>
      </div>
      <div className="pagination__controls">
        <button className="pagination__controls-previous" type="button">
          <Icon name="arrowLeft" size={14} />
        </button>
        <button className="pagination__controls-next" type="button">
          <Icon name="arrowRight" size={14} />
        </button>
      </div>
    </div>
  );
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["pagination"];

  className && classArray.push(className);

  return classArray.join(" ");
};

Pagination.propTypes = {
  className: PropTypes.string,
};

Pagination.defaultProps = {
  className: null,
};

export { Pagination };
