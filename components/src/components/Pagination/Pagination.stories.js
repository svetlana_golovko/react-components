import { Pagination } from "../Pagination/Pagination";

export default {
  title: "Components/Pagination",
  component: Pagination,
};

const TemplatePrimary = (args) => <Pagination {...args}></Pagination>;

const Primary = TemplatePrimary.bind({});
Primary.args = {
};

export { Primary };
