import { Button } from "./Button";

export default {
  title: "Components/Button",
  component: Button,
};

const TemplatePrimary = (args) => <Button {...args}>Lorem ipsum</Button>;

const Primary = TemplatePrimary.bind({});
Primary.args = {
  size: "medium",
  type: "button",
  variant: "primary",
  width: "auto",
};

export { Primary };
