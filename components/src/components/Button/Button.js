import PropTypes from "prop-types";
import { modal } from "../../utils/utils";

const Button = (props) => {
  const {
    onClick,
    children,
    className,
    disabled,
    size,
    type,
    variant,
    width,
    id,
    ...rest
  } = props;

  const handleClick = (e) => {
    if (modal.showId(rest)) {
      return;
    }

    onClick && onClick(e);
  };

  return (
    <button className={BEM(props)} onClick={handleClick} {...rest}>
      {children}
    </button>
  );
};

const BEM = (props) => {
  const { className, variant, size, width } = props;
  const classArray = [
    "button",
    `button--size-${size}`,
    `button--variant-${variant}`,
    `button--width-${width}`,
  ];

  className && classArray.push(className);

  return classArray.join(" ");
};

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.string,
  size: PropTypes.oneOf(["large", "medium", "small", "tiny"]),
  type: PropTypes.oneOf(["button", "reset", "submit"]),
  variant: PropTypes.oneOf([
    "primary",
    "primary-dark",
    "secondary",
    "secondary-inverted",
    "secondary-no-border",
    "secondary-inverted-no-border",
    "empty",
    "cancel",
  ]),
  width: PropTypes.oneOf(["auto", "fluid", "fluid-mobile"]),
};

Button.defaultProps = {
  className: null,
  onClick: null,
  id: null,
  size: "medium",
  type: "button",
  variant: "primary",
  width: "auto",
};

export { Button };
