import React from "react";
import PropTypes from "prop-types";

const Text = (props) => {
  const { children } = props;
  return <p className={BEM(props)}>{children}</p>;
};

const BEM = (props) => {
  const { className, size, color } = props;
  const classArray = ["p", `p--size-${size}`];

  className && classArray.push(className);
  color && classArray.push(`p--color-${color}`);

  return classArray.join(" ");
};

Text.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["lead", "p", "small", "tiny"]),
  color: PropTypes.oneOf([
    "action",
    "action-light",
    "bottom-of-the-sea",
    "buoy",
    "buoy-light",
    "buoy-link",
    "dark-grey",
    "dance-floor",
    "error",
    "group-grey",
    "info",
    "lemon",
    "logotype-blue",
    "logotype-red",
    "medium-grey",
    "midnight",
    "notice",
    "pool-light",
    "seafoam",
    "shuttle-green",
    "sunset",
    "twilight",
    "white",
  ]),
};

Text.defaultProps = {
  className: null,
  size: "p",
};

export { Text };
