import React from "react";

import { Text } from "./Text";

export default {
  component: Text,
  title: "Components/Text"
};

const TemplatePrimary = (args) => (
  <Text {...args}>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut at purus quam.
    Cras viverra orci nec ex posuere, eget finibus nisl blandit. Sed quis
    pharetra nibh. Vestibulum nunc enim, vulputate eget euismod sed, consequat
    ut libero. Duis quis hendrerit sem, vel faucibus magna. Duis tincidunt
    imperdiet volutpat. Suspendisse sit amet luctus sapien.
  </Text>
);

const Primary = TemplatePrimary.bind({});
Primary.args = {
    size: "p"
};

export { Primary };
