import PropTypes from "prop-types";

const Span = (props) => {
  const { children, className, color } = props;
  return <span className={BEM(props)}>{children}</span>;
};

const BEM = (props) => {
  const { className, color } = props;
  const classArray = ["span"];

  className && classArray.push(classArray);
  color && classArray.push(`span--color-${color}`);

  return classArray.join(" ");
};

Span.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf([
    "action",
    "action-light",
    "bottom-of-the-sea",
    "buoy",
    "buoy-light",
    "buoy-link",
    "dark-grey",
    "dance-floor",
    "error",
    "group-grey",
    "info",
    "lemon",
    "logotype-blue",
    "logotype-red",
    "medium-grey",
    "midnight",
    "notice",
    "pool-light",
    "seafoam",
    "shuttle-green",
    "sunset",
    "twilight",
    "white",
  ]),
};

Span.defaultProps = {
  className: null,
  color: "midnight",
};

export { Span };
