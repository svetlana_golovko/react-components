import React from "react";

import { Span } from "./Span";

export default {
  component: Span,
  title: "Components/Span",
};

const TemplatePrimary = (args) => (
  <Span>
    Lorem ipsum<Span {...args}> dolor sit amet</Span>
  </Span>
);

const Primary = TemplatePrimary.bind({});
Primary.args = {
  color: "sunset",
};

export { Primary };
