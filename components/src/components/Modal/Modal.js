import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Button } from "../Button/Button";
import { Icon } from "../Icon/Icon";
import { modal } from "../../utils/utils";

const Modal = (props) => {
  const {
    showCloseButton,
    closeOnClickOutside,
    isBottom,
    id,
    children,
    ...rest
  } = props;

  const overlayRef = useRef(null);
  const modalRef = useRef(null);

  useEffect(() => {
    if (closeOnClickOutside) {
      const handleClickOutside = (e) => {
        const checkRefContainsTarget = (ref) => {
          if (ref.current) {
            if (ref.current.contains(e.target)) {
              return true;
            }
          }
          return false;
        };

        if (checkRefContainsTarget(overlayRef)) {
          if (!checkRefContainsTarget(modalRef)) {
            hideModal();
          }
        }
      };
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });

  const hideModal = () => {
    modal.hide(id);
  };
  return (
    <div className="modal-overlay" ref={overlayRef} {...rest} id={id}>
      <div
        className={`modal-container ${isBottom && "modal-container-bottom"}`}
        ref={modalRef}
      >
        {children}
        {showCloseButton && (
          <div className="modal-close-button-container">
            <Button
              className="modal-close-button"
              size="tiny"
              variant="empty"
              onClick={() => {
                hideModal();
              }}
            >
              <Icon name={"close"} size={12} />
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

Modal.propTypes = {
  showCloseButton: PropTypes.bool,
  closeOnClickOutside: PropTypes.bool,
  isBottom: PropTypes.bool,
  id: PropTypes.string,
};

Modal.defaultProps = {
  showCloseButton: true,
  closeOnClickOutside: false,
  isBottom: false,
  id: null,
};

export { Modal };
