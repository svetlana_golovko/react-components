import React from "react";

import { Modal } from "./Modal";
import { ModalContent } from "./ModalContent";
import { ModalFooter } from "./ModalFooter";
import { Button } from "../Button/Button";
import { Text } from "../Text/Text";
import { Header } from "../Header/Header";

export default {
  title: "Components/Modal",
  component: Modal,
};

const Template = (args) => (
  <>
    <Button data-modal-id="modal-id">Open Modal</Button>
    <Modal {...args}>
      <ModalContent>
        <Header variant="h2">Header here</Header>
        <Text>
          Modal body <br />
          Can be anything <br />
          Please fill in tht fields for our representative to go through them
          and send you our offer back <br />
        </Text>
      </ModalContent>
      <ModalFooter>
        <Button>
          Submit
        </Button>
      </ModalFooter>
    </Modal>
  </>
);

const Primary = Template.bind({});

Primary.args = {
  isBottom: false,
  showCloseButton: true,
  closeOnClickOutside: false,
  id: "modal-id",
};

export { Primary };