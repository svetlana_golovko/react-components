import React from "react";

import { Header } from "./Header";

export default {
  component: Header,
  title: "Components/Header",
};

const TemplatePrimary = (args) => <Header {...args}>Lorem ipsum</Header>;

const Primary = TemplatePrimary.bind({});
Primary.args = {
  variant: "h3",
};

export { Primary };
