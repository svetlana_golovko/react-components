import PropTypes from "prop-types";

const Header = (props) => {
  const { variant, color, children, ...rest } = props;

  const HeaderTag = variant;

  return (
    <HeaderTag {...rest} className={BEM(props)}>
      {children}
    </HeaderTag>
  );
};

const BEM = (props) => {
  const { className, variant, color } = props;
  const classArray = [`header-${variant}`];

  className && classArray.push(className);
  color && classArray.push(`header--color-${color}`);

  return classArray.join(" ");
};

Header.propTypes = {
  variant: PropTypes.oneOf(["h1", "h2", "h3", "h4"]),
  color: PropTypes.oneOf([
    "action",
    "action-light",
    "bottom-of-the-sea",
    "buoy",
    "buoy-light",
    "buoy-link",
    "dark-grey",
    "dance-floor",
    "error",
    "group-grey",
    "info",
    "lemon",
    "logotype-blue",
    "logotype-red",
    "medium-grey",
    "midnight",
    "notice",
    "pool-light",
    "seafoam",
    "shuttle-green",
    "sunset",
    "twilight",
    "white",
  ]),
};

Header.defaultProps = {
  variant: "h4",
};

export { Header };
