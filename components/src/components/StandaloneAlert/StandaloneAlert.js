import { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { fn } from "../../utils/utils";

const StandaloneAlert = (props) => {
  const { children } = props;
  const alertBoxRef = useRef();
  const [alertFlexDirection, setAlertFlexDirection] = useState(false);

  useEffect(() => {
    if (alertBoxRef.current) {
      const { width } = fn.getRefProperties(alertBoxRef);
      setAlertFlexDirection(width < 600);
    }
  }, [alertFlexDirection]);

  return (
    <div
      className={BEM(props, alertFlexDirection)}
      {...props}
      ref={alertBoxRef}
    >
      {children}
    </div>
  );
};
const BEM = (props, alertFlexDirection) => {
  const { type, size, border, className } = props;
  const classArray = ["sa-alert"];

  type && classArray.push(`sa-alert--${type}`);
  size && classArray.push(`sa-alert--${size}`);
  border && classArray.push(`sa-alert--border`);
  alertFlexDirection && classArray.push(`sa-alert--direction-column`);
  className && classArray.push(className);

  return classArray.join(" ");
};

const StandaloneAlertIcon = (props) => {
  const { children } = props;
  return <div className="sa-alert-icon">{children}</div>;
};

const StandaloneAlertInfo = (props) => {
  const { children } = props;
  return (
    <div className="sa-alert-body">
      <div className="sa-alert-body-content">{children}</div>
    </div>
  );
};

const StandaloneAlertInteractions = (props) => {
  const { children } = props;
  return <div className="sa-alert-interactions">{children}</div>;
};

const StandaloneAlertBody = (props) => {
  const { children } = props;
  return <div className="sa-alert-left">{children}</div>;
};

StandaloneAlert.propTypes = {
  type: PropTypes.oneOf(["info", "error", "success"]),
  size: PropTypes.oneOf(["large", "small"]),
  border: PropTypes.bool,
};

StandaloneAlert.defaultProps = {
  type: "info",
  size: "small",
  border: false,
};

export {
  StandaloneAlert,
  StandaloneAlertBody,
  StandaloneAlertIcon,
  StandaloneAlertInfo,
  StandaloneAlertInteractions,
};
