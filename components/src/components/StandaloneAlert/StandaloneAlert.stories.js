import React from "react";

import {
  StandaloneAlert,
  StandaloneAlertIcon,
  StandaloneAlertBody,
  StandaloneAlertInfo,
  StandaloneAlertInteractions,
} from "./StandaloneAlert";
import { Icon } from "../Icon/Icon";
import { Header } from "../Header/Header";
import { Button } from "../Button/Button";
import { Text } from "../Text/Text";

export default {
  title: "Components/Alerts/Standalone alert",
  component: StandaloneAlert,
};

const Template = (args) => (
  <StandaloneAlert {...args}>
    <StandaloneAlertBody>
      <StandaloneAlertIcon>
        <Icon name={"home"} size={28} />
      </StandaloneAlertIcon>
      <StandaloneAlertInfo>
        <Header variant={"h3"}>Test header</Header>
        <Text>Test text</Text>
      </StandaloneAlertInfo>
    </StandaloneAlertBody>

    <StandaloneAlertInteractions>
      <Button
        onClick={() => {
          alert("clicked");
        }}
      >
        Button
      </Button>
    </StandaloneAlertInteractions>
  </StandaloneAlert>
);
const Primary = Template.bind({});
Primary.args = {
  type: "success",
  size: "small",
  border: false,
};

export { Primary };