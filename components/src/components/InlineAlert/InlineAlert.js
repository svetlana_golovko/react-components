import { Icon } from "../Icon/Icon";
import PropTypes from "prop-types";

const typeAndIconPairs = {
  error: { name: "typeError", alert: "error" },
  important: { name: "typeImportant", alert: "important" },
  success: { name: "typeSuccess", alert: "success" },
  info: { name: "typeInfo", alert: "info" },
};

const InlineAlert = (props) => {
  const { type, children } = props;
  return (
    <div className={BEM(props)}>
      <div className="alert-icon">
        <Icon {...getTypeIcon(type)} />
      </div>
      {children}
    </div>
  );
};

function getTypeIcon(type) {
  return typeAndIconPairs[type];
}

const BEM = (props) => {
  const { className } = props;
  const classArray = ["alert-box"];

  className && classArray.push(className);

  return classArray.join(" ");
};

InlineAlert.propTypes = {
  type: PropTypes.oneOf(["error", "important", "success", "info"]),
};

InlineAlert.defaultProps = {
  type: "info",
};

export { InlineAlert };
