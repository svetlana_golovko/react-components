import React from "react";

import { InlineAlert } from "./InlineAlert";
import { Text } from "../Text/Text";

export default {
  title: "Components/Alerts/InlineAlert",
  component: InlineAlert,
};

const Template = (args) => (
  <InlineAlert {...args}>
    <Text>Alert text</Text>
  </InlineAlert>
);

const Primary = Template.bind({});
Primary.args = {
  type: "info"
}

export { Primary };
