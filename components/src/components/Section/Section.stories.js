import React from "react";

import { Section } from "./Section";
import { Text } from "../Text/Text";

export default {
  component: Section,
  title: "Components/Section",
};

const TemplatePrimary = (args) => (
  <Section className={"bg-lemon p-16"}>
    <Text>Lorem ipsum</Text>
    <Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus
      tristique eros quis ante dictum consequat eget ac nulla. Sed sodales mi ac
      lacus sodales sagittis. Suspendisse id ligula non ante aliquet convallis.
    </Text>
  </Section>
);

const Primary = TemplatePrimary.bind({});
Primary.args = {};

export { Primary };
