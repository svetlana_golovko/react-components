import PropTypes from "prop-types";
import React from "react";

const Section = (props) => {
  const { children, ...rest } = props;
  return <section {...rest} className={BEM(props)} >{children}</section>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["section"];

  className && classArray.push(className);

  return classArray.join(" ");
};

Section.propTypes = {
  className: PropTypes.string,
};

Section.defaultProps = {
  className: null,
};

export { Section };
