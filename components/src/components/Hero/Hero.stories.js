import { React } from "react";
import { Hero } from "./Hero";
import { HeroImage } from "./HeroContainers/HeroImage";
import { HeroBody } from "./HeroContainers/HeroBody";
import { HeroIcon } from "./HeroContainers/HeroIcon";
import { Text } from "../Text/Text";
import { Header } from "../Header/Header";
import { Button } from "../Button/Button";
import { Span } from "../Span/Span";
import { Image } from "../Image/Image";

export default {
  title: "Core/Hero",
  component: Hero,
};

const TemplatePrimary = (args) => {
  if (args.mode === "media-only") {
    return (
      <>
        <Hero mode="media-only">
          <HeroImage>
            <Image src="/img/earth.jpg" alt="name pic"></Image>
          </HeroImage>
        </Hero>
      </>
    );
  } else {
    return (
      <>
        <Hero {...args}>
          <HeroImage>
            <Image src={"/img/earth.jpg"} alt={"name pic"}></Image>
          </HeroImage>
          <HeroBody>
            <HeroIcon>
              <Image src={"/img/water.jpg"} alt={"Icon"}></Image>
            </HeroIcon>
            <Header variant={"h1"}>
              Quisque tristique odio <Span>sit Ämet tellus</Span>
            </Header>
            <Text size={"lead"}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
              placerat mauris ut eros elementum venenatis ac et felis venenatis.
            </Text>
            <Button>Quisque tristique</Button>
          </HeroBody>
        </Hero>
      </>
    );
  }
};

const Primary = TemplatePrimary.bind({});
Primary.args = {
  center: false,
  mode: "full-bleed",
  type: "combo-shuttle",
  variant: "combo-1",
};

export { Primary };
