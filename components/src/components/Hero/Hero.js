import PropTypes from "prop-types";

const Hero = (props) => {
  const { children, center, mode, type, variant } = props;
  return <div className={BEM(props)}>{children}</div>;
};

const BEM = (props) => {
  const { className, center, mode, type, variant } = props;
  const classArray = ["hero"];

  className && classArray.push(className);

  mode && classArray.push(`hero--mode-${mode}`);
  center &&
    mode === "full-bleed" &&
    classArray.push("hero--center", "hero--center-fb");
  center &&
    mode === "half-bleed" &&
    type === "cruise" &&
    classArray.push("hero--center");
  type && classArray.push(`hero--${type}`);
  type && variant && classArray.push(`hero--${type}-${variant}`);

  return classArray.join(" ");
};

Hero.propTypes = {
  className: PropTypes.string,
  center: PropTypes.bool,
  mode: PropTypes.oneOf(["full-bleed", "half-bleed", "media-only"]),
  type: PropTypes.oneOf(["combo-shuttle", "cruise"]),
  variant: PropTypes.oneOf(["combo-1", "combo-2", "shuttle-1", "shuttle-2"]),
};

Hero.defaultProps = {
  className: null,
  center: false,
  mode: "full-bleed",
  type: "combo-shuttle",
  variant: "combo-1",
};

export { Hero };
