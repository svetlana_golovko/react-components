const HeroImage = (props) => {
  const { children } = props;
  return <div className={BEM(props)}>{children}</div>;
}

const BEM = (props) => {
  const { className } = props;
  const classArray = ["hero__image"];

  className && classArray.push(className);

  return classArray.join(" ");
};

export { HeroImage };
