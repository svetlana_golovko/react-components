const HeroBody = (props) => {
  const { children } = props;
  return (
    <div className="hero__body-wrapper">
      <div className="hero__body-container">
        <div className={BEM(props)}>{children}</div>
      </div>
    </div>
  );
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["hero__body"];

  className && classArray.push(className);

  return classArray.join(" ");
};

export { HeroBody };
