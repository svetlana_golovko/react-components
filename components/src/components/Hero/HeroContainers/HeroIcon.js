const HeroIcon = (props) => {
  const { children } = props;
  return <div className={BEM(props)}>{children}</div>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["hero__icon"];

  className && classArray.push(className);

  return classArray.join(" ");
};

export { HeroIcon };
