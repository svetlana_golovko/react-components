import PropTypes from "prop-types";

const Image = (props) => {
  const { src, alt, className } = props;
  return <img className={BEM(props)} src={src} alt={alt} />;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["image"];

  className && classArray.push(className);

  return classArray.join(" ");
};

Image.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string,
  alt: PropTypes.string,
};

Image.defaultProps = {
  className: null,
  src: null,
  alt: "Default text",
};

export { Image };
