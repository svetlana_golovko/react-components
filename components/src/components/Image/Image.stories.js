import { Image } from "./Image";

export default {
  title: "Components/Image",
  component: Image,
};

const TemplatePrimary = (args) => <Image {...args} />;

const Primary = TemplatePrimary.bind({});
Primary.args = {
  className: null,
  src: "/img/earth.jpg",
  alt: "name pic",
};

export { Primary };
