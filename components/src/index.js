import React from 'react';
import "./assets/styles/main.scss";
import { Home } from './pages/Home';

import { createRoot } from 'react-dom/client';
const container = document.getElementById('root');
const root = createRoot(container); 
root.render(<Home/>);