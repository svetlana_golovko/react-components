const modal = {
  showId: (rest) => {
    if ("data-modal-id" in rest) {
      return modal.show(rest["data-modal-id"]);
    }
    return false;
  },

  hide: function (id) {
    document
      .getElementById(id)
      .classList.remove("modal-overlay-open", "modal-container-open");
    modal.page.activate();
  },

  show: function (id) {
    const modalId = document.getElementById(id);
    if (modalId) {
      document.getElementById(id).className +=
        " modal-overlay-open modal-container-open";
      modal.page.deactivate();
      return true;
    }
    return false;
  },
  page: {
    activate: function () {
      let root = document.documentElement;
      root.classList.remove("modal-container-open");
      document.body.style.overflow = "unset";
    },

    deactivate: function () {
      let root = document.documentElement;
      root.className += " modal-container-open";
      document.body.style.overflow = "hidden";
    },
  },
};

const fn = {
  getRefProperties: (ref) => {
    return {
      right: ref.current.getBoundingClientRect().right,
      left: ref.current.getBoundingClientRect().left,
      width: ref.current.getBoundingClientRect().width,
      bottom: ref.current.getBoundingClientRect().bottom
    };
  },
};

export { modal, fn };
