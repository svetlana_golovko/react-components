import { Layout } from "../layout/Main";
import { GridMargus } from "react-components";
import { GridMargusCol } from "react-components";
import { GridMargusRow } from "react-components";
import { Header } from "../components/Header/Header";
import { Text } from "../components/Text/Text";
import { Span } from "../components/Span/Span";
import { Button } from "../components/Button/Button";
import { PageBody } from "../layout/PageBody/PageBody";
import { Hero } from "../components/Hero/Hero";
import { HeroImage } from "../components/Hero/HeroContainers/HeroImage";
import { HeroBody } from "../components/Hero/HeroContainers/HeroBody";
import { HeroIcon } from "../components/Hero/HeroContainers/HeroIcon";
import { Image } from "../components/Image/Image";
import { InlineAlert } from "../components/InlineAlert/InlineAlert";
import { Section } from "../components/Section/Section";
import { Sidebar } from "../components/Sidebar/Sidebar";
import { Icon } from "../components/Icon/Icon";

const Home = () => {
  return (
    <Layout className={"bg-group-grey"}>
      <PageBody className={"mr-24 ml-24"}>
        <Section className={"mt-24"}>
          <GridMargus>
            <GridMargusRow>
              <GridMargusCol col={8}>
                <Text>Lorem ipsum</Text>

                <Hero mode="media-only" className={"mt-16"}>
                  <HeroImage>
                    <Image src="/img/earth.jpg" alt="earth"></Image>
                  </HeroImage>
                </Hero>
              </GridMargusCol>
              <GridMargusCol col={4} align={"center"}>
                <Header>
                  Lorem <Span color={"info"}>ipsum</Span>
                </Header>
                <Text>Lorem ipsum</Text>
                <Text>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
                  condimentum libero id felis varius, eget euismod tellus
                  euismod. Aenean elementum, elit quis faucibus ultricies,
                  libero mauris ullamcorper felis, ut suscipit nunc ipsum in
                  leo. Sed sit amet imperdiet ex. Integer eu aliquet massa.
                </Text>
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </Section>

        <Section className={"mt-24"}>
          <GridMargus>
            <GridMargusRow>
              <GridMargusCol col={8}>
                <InlineAlert type={"info"}>
                  <Text>Alert text</Text>
                </InlineAlert>
              </GridMargusCol>
              <GridMargusCol col={4}>
                <Text>Lorme ipsum 2</Text>
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </Section>

        <Sidebar className={"mt-24 bg-midnight"} sticky={true}>
          <GridMargus>
            <GridMargusRow horizontal={"center"} vertical={"center"}>
              <GridMargusCol col={4} align={"center"}>
                <Button variant={"secondary-inverted-no-border"}>
                  <Icon color={"white"} size={24} name={"location"} /> Lorem
                  ipsum 1
                </Button>
              </GridMargusCol>
              <GridMargusCol col={4}>
                <Button variant={"secondary-inverted-no-border"}>
                  Lorem ipsum 2
                </Button>
              </GridMargusCol>
              <GridMargusCol col={4}>
                <Button variant={"secondary-inverted-no-border"}>
                  Lorem ipsum 3
                </Button>
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </Sidebar>

        <Section>
          <GridMargus className={"mt-24"}>
            <GridMargusRow>
              <GridMargusCol col={12}>
                <GridMargus fluid={true} className={"mt-16"}>
                  <GridMargusRow>
                    <GridMargusCol col={6}>
                      <Text>Lorem ipsum 1</Text>
                    </GridMargusCol>
                    <GridMargusCol col={6} align={"end"}>
                      <Text>Lorme ipsum 2</Text>
                    </GridMargusCol>
                  </GridMargusRow>
                </GridMargus>
                <Hero className={"mt-16"} variant={"shuttle-1"}>
                  <HeroImage>
                    <Image src={"/img/earth.jpg"} alt={"name pic"}></Image>
                  </HeroImage>
                  <HeroBody>
                    <HeroIcon>
                      <Image src={"/img/water.jpg"} alt={"Icon"}></Image>
                    </HeroIcon>
                    <Header variant={"h1"}>
                      Quisque tristique odio <Span>sit Ämet tellus</Span>
                    </Header>
                    <Text size={"lead"}>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Donec placerat mauris ut eros elementum venenatis ac et
                      felis venenatis.
                    </Text>
                    <Button>Quisque tristique</Button>
                  </HeroBody>
                </Hero>
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </Section>

        <Section className={"mt-24"}>
          <GridMargus>
            <GridMargusRow>
              <GridMargusCol col={12} align={"center"}>
                <Text className={"mb-24"}>Lorem ipsum Lorem ipsum</Text>
                <Text className={"mb-24"}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
                  condimentum libero id felis varius, eget euismod tellus
                  euismod. Aenean elementum, elit quis faucibus ultricies,
                  libero mauris ullamcorper felis, ut suscipit nunc ipsum in
                  leo. Sed sit amet imperdiet ex. Integer eu aliquet massa.
                </Text>
                <Text className={"mb-24"}>Lorem ipsum</Text>
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </Section>
      </PageBody>
    </Layout>
  );
};

export { Home };
