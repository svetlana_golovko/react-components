import PropTypes from "prop-types";
import React from "react";

const PageFooter = (props) => {
  const { children } = props;
  return <footer className={BEM(props)}>{children}</footer>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = [];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageFooter.propTypes = {
  className: PropTypes.string,
};

PageFooter.defaultProps = {
  className: null,
};

export { PageFooter };
