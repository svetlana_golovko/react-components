import PropTypes from "prop-types";
import React from "react";

const PageBody = (props) => {
  const { children } = props;
  return <div className={BEM(props)}>{children}</div>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["page-main"];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageBody.propTypes = {
  className: PropTypes.string,
};

PageBody.defaultProps = {
  className: null,
};

export { PageBody };
