import React from "react";

import { PageHeader } from "./PageHeader/PageHeader";
import { PageFooter } from "./PageFooter/PageFooter";
import { GridMargus } from "react-components";
import { GridMargusCol } from "react-components";
import { GridMargusRow } from "react-components";
import { Header } from "../components/Header/Header";
import { Text } from "../components/Text/Text";
import { Button } from "../components/Button/Button";
import { PageHeaderMenu } from "./PageHeader/PageHeaderMenu/PageHeaderMenu";
import { PageHeaderMenuLeft } from "./PageHeader/PageHeaderMenu/PageHeaderMenuLeft";
import { PageHeaderMenuRight } from "./PageHeader/PageHeaderMenu/PageHeaderMenuRight";
import { PageHeaderNav } from "./PageHeader/PageHeaderNav/PageHeaderNav";
import { Sidebar } from "../components/Sidebar/Sidebar";
import { SidebarItem } from "../components/Sidebar/SidebarItem";

const Layout = (props) => {
  const { children } = props;
  return (
    <div className={BEM(props)}>
      <PageHeader>
        <PageHeaderMenu>
          <PageHeaderMenuLeft>
            <Button variant={"secondary-inverted-no-border"} size={"small"}>
              Home
            </Button>

            <Button variant={"secondary-inverted-no-border"} size={"small"}>
              Contact
            </Button>

            <Button variant={"secondary-inverted-no-border"} size={"small"}>
              Info
            </Button>
          </PageHeaderMenuLeft>

          <PageHeaderMenuRight>
            <Button variant={"secondary-inverted-no-border"} size={"medium"}>
              Login
            </Button>
          </PageHeaderMenuRight>
        </PageHeaderMenu>

        <PageHeaderNav>
          <Sidebar>
            <SidebarItem>
            <Button variant={"empty"} size={"tiny"}>
              Home
            </Button>
            </SidebarItem>
            <SidebarItem>
            <Button variant={"empty"} size={"tiny"}>
              News
            </Button>
            </SidebarItem>
            <SidebarItem>
            <Button variant={"empty"} size={"tiny"}>
              Contact
            </Button>
            </SidebarItem>
            <SidebarItem>
            <Button variant={"empty"} size={"tiny"}>
              About
            </Button>
            </SidebarItem>
          </Sidebar>
        </PageHeaderNav>
      </PageHeader>

      {children}

      <PageFooter className={"bg-midnight"}>
        <GridMargus fluid={true} className={"p-16"}>
          <GridMargusRow>
            <GridMargusCol>
              <Header variant={"h4"} color={"white"}>Footer 1</Header>
              <Text size={"p"} color={"white"}>Home</Text>
            </GridMargusCol>
            <GridMargusCol>
              <Header variant={"h4"} color={"white"}>Footer 2</Header>
              <Text size={"p"} color={"white"}>Info</Text>
              <Text size={"p"} color={"white"}>FAQ</Text>
            </GridMargusCol>
            <GridMargusCol>
              <Header variant={"h4"} color={"white"}>Footer 3</Header>
              <Text size={"p"} color={"white"}>Social 1</Text>
              <Text size={"p"} color={"white"}>Social 2</Text>
              <Text size={"p"} color={"white"}>Social 1</Text>
              <Text size={"p"} color={"white"}>Social 2</Text>
            </GridMargusCol>
          </GridMargusRow>
        </GridMargus>
      </PageFooter>
    </div>
  );
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["layout-main"];

  className && classArray.push(className);

  return classArray.join(" ");
};

export { Layout };
