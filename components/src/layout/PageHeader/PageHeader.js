import PropTypes from "prop-types";
import React, { useEffect, useState, useRef } from "react";

const PageHeader = (props) => {
  const { children, scroll } = props;

  const prevScrollY = useRef(0);
  const [scrollDirection, setScrollDirection] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      const currentScrollY = window.scrollY;
      if (prevScrollY.current < currentScrollY && currentScrollY > 80 && scrollDirection) {
        setScrollDirection(false);
      }
      if (prevScrollY.current > currentScrollY && !scrollDirection) {
        setScrollDirection(true);
      }

      prevScrollY.current = currentScrollY;
    };

    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [scrollDirection]);

  return <header className={BEM(props, scrollDirection)}>{children}</header>;
};

const BEM = (props, scrollDirection) => {
  const { className, scroll } = props;
  const classArray = ["page-header"];

  className && classArray.push(className);
  scroll && !scrollDirection && classArray.push("page-header--scroll");

  return classArray.join(" ");
};

PageHeader.propTypes = {
  className: PropTypes.string,
  scroll: PropTypes.bool
};

PageHeader.defaultProps = {
  className: null,
};

export { PageHeader };
