import PropTypes from "prop-types";
import React from "react";

const PageHeaderMenuLeft = (props) => {
  const { children } = props;
  return <div className={BEM(props)}>{children}</div>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["page-header__menu-left"];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageHeaderMenuLeft.propTypes = {
  className: PropTypes.string,
};

PageHeaderMenuLeft.defaultProps = {
  className: null,
};

export { PageHeaderMenuLeft };
