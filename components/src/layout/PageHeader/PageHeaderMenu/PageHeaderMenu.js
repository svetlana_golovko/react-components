import PropTypes from "prop-types";
import React from "react";

const PageHeaderMenu = (props) => {
  const { children } = props;
  return <header className={BEM(props)}>{children}</header>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["page-header__menu"];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageHeaderMenu.propTypes = {
  className: PropTypes.string,
};

PageHeaderMenu.defaultProps = {
  className: null,
};

export { PageHeaderMenu };
