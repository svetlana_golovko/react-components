import PropTypes from "prop-types";
import React from "react";

const PageHeaderMenuRight = (props) => {
  const { children } = props;
  return <div className={BEM(props)}>{children}</div>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["page-header__menu-right"];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageHeaderMenuRight.propTypes = {
  className: PropTypes.string,
};

PageHeaderMenuRight.defaultProps = {
  className: null,
};

export { PageHeaderMenuRight };
