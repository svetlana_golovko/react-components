import PropTypes from "prop-types";
import React from "react";

const PageHeaderNav = (props) => {
  const { children } = props;
  return <header className={BEM(props)}>{children}</header>;
};

const BEM = (props) => {
  const { className } = props;
  const classArray = ["page-header__nav"];

  className && classArray.push(className);

  return classArray.join(" ");
};

PageHeaderNav.propTypes = {
  className: PropTypes.string,
};

PageHeaderNav.defaultProps = {
  className: null,
};

export { PageHeaderNav };
