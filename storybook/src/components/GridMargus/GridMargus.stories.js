import React from "react";

import { GridMargus } from "./GridMargus";
import { GridMargusCol } from "./GridMargusCol/GridMargusCol";
import { GridMargusRow } from "./GridMargusRow/GridMargusRow";

export default {
  title: "Core/Grid Margus",
  component: GridMargus,
};

const TemplatePrimary = (args) => {
  return (
    <>
      <GridMargus fluid={args.fluid}>
        <GridMargusRow>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={1}>
            <div className="example-empty-div" />
          </GridMargusCol>
        </GridMargusRow>

        <GridMargusRow className={"mt-16 mt-md-24"}>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={2}>
            <div className="example-empty-div" />
          </GridMargusCol>
        </GridMargusRow>

        <GridMargusRow className={"mt-16 mt-md-24"}>
          <GridMargusCol col={3}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={3}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={3}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={3}>
            <div className="example-empty-div" />
          </GridMargusCol>
        </GridMargusRow>

        <GridMargusRow className={"mt-16 mt-md-24"}>
          <GridMargusCol col={4}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={4}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={4}>
            <div className="example-empty-div" />
          </GridMargusCol>
        </GridMargusRow>

        <GridMargusRow className={"mt-16 mt-md-24"}>
          <GridMargusCol col={6}>
            <div className="example-empty-div" />
          </GridMargusCol>
          <GridMargusCol col={6}>
            <div className="example-empty-div" />
          </GridMargusCol>
        </GridMargusRow>
      </GridMargus>
    </>
  );
};

const TemplateOneColWidth = (args) => {
  return (
    <GridMargus {...args}>
      <GridMargusRow>
        <GridMargusCol col={args.col}>
          <div className="example-empty-div" />
        </GridMargusCol>
        <GridMargusCol col={args.col}>
          <div className="example-empty-div" />
        </GridMargusCol>

        <GridMargusCol
          col={args.col}
          xs={args.xs}
          sm={args.sm}
          md={args.md}
          lg={args.lg}
          xl={args.xl}
        >
          <div className="example-empty-div" />
        </GridMargusCol>

        <GridMargusCol col={args.col}>
          <div className="example-empty-div" />
        </GridMargusCol>
      </GridMargusRow>
    </GridMargus>
  );
};

const TemplateNested = (args) => {
  return (
    <GridMargus className={args.className} fluid={args.fluid}>
      <GridMargusRow>
        <GridMargusCol
          xs={args.xs}
          sm={args.sm}
          md={args.md}
          lg={args.lg}
          xl={args.xl}
        >
          <GridMargus fluid={args.fluid}>
            <GridMargusRow>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </GridMargusCol>
        <GridMargusCol
          xs={args.xs}
          sm={args.sm}
          md={args.md}
          lg={args.lg}
          xl={args.xl}
        >
          <div className="example-empty-div" />
        </GridMargusCol>
      </GridMargusRow>
    </GridMargus>
  );
};

const Primary = TemplatePrimary.bind({});
Primary.args = {};

const OneColumnWidthPerScreenSize = TemplateOneColWidth.bind({});
OneColumnWidthPerScreenSize.args = {
  col: 1,
  xs: 2,
  sm: 3,
  md: 4,
  lg: 6,
  xl: 1,
};

const NestedGrid = TemplateNested.bind({});
NestedGrid.args = {
  xs: 6,
  sm: 6,
  md: 6,
  lg: 6,
  xl: 6,
  fluid: true,
};

export { Primary, OneColumnWidthPerScreenSize, NestedGrid };
