import React from "react";
import PropTypes from "prop-types";

const GridMargusRow = (props) => {
  const { className, children, vertical, horizontal, ...rest } = props;
  return (
    <div className={BEM(props)} {...rest}>
      {children}
    </div>
  );
};

const BEM = (props) => {
  const { className, vertical, horizontal } = props;
  const classArray = ["grid-margus__row"];

  className && classArray.push(className);
  vertical && classArray.push(`grid-margus__row--vertical-${vertical}`);
  horizontal && classArray.push(`grid-margus__row--horizontal-${horizontal}`);

  return classArray.join(" ");
};

GridMargusRow.propTypes = {
  className: PropTypes.string,
  horizontal: PropTypes.oneOf(["center", "left", "right", "space-between"]),
  vertical: PropTypes.oneOf(["top", "bottom", "center", "stretch"]),
};

GridMargusRow.defaultProps = {
  className: null,
};

export { GridMargusRow };
