import React from "react";
import PropTypes from "prop-types";

const GridMargusCol = (props) => {
  const { className, col, xs, sm, md, lg, xl, align, children, ...rest } =
    props;
  return (
    <div className={BEM(props)} {...rest}>
      {children}
    </div>
  );
};

const BEM = (props) => {
  const { className, col, xs, sm, md, lg, xl, align } = props;
  const classArray = ["grid-margus__col"];

  col && classArray.push(`grid-margus__col--${col}`);
  xs && classArray.push(`grid-margus__col--xs-${xs}`);
  sm && classArray.push(`grid-margus__col--sm-${sm}`);
  md && classArray.push(`grid-margus__col--md-${md}`);
  lg && classArray.push(`grid-margus__col--lg-${lg}`);
  xl && classArray.push(`grid-margus__col--xl-${xl}`);
  align && classArray.push(`grid-margus__col--align-${align}`);

  className && classArray.push(className);

  return classArray.join(" ");
};

GridMargusCol.propTypes = {
  className: PropTypes.string,
  col: PropTypes.number,
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  align: PropTypes.oneOf(["start", "end", "center"]),
};

GridMargusCol.defaultProps = {
  className: null,
};

export { GridMargusCol };
