import React from "react";
import PropTypes from "prop-types";

const GridMargus = (props) => {
  const { className, fluid, ...rest } = props;
  return <div className={BEM(props)} {...rest}></div>;
};

const BEM = (props) => {
  const { className, fluid } = props;
  const classArray = ["grid-margus"];

  className && classArray.push(className);
  fluid && classArray.push("grid-margus--fluid");

  return classArray.join(" ");
};

GridMargus.propTypes = {
  className: PropTypes.string,
  fluid: PropTypes.bool,
};

GridMargus.defaultProps = {
  className: null,
  fluid: false,
};

export { GridMargus };
