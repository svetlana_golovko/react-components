import React from "react";
import PropTypes from "prop-types";

const GridSveta = (props) => {
  const { className, ...rest } = props;
  return <div className={BEM(props)} {...rest}></div>;
};

const BEM = (props) => {
  const { className, spanLastRow } = props;
  const classArray = [];
  spanLastRow && classArray.push("grid_with_last_row_span");
  !spanLastRow && classArray.push("grid");

  className && classArray.push(className);

  return classArray.join(" ");
};

GridSveta.propTypes = {
  className: PropTypes.string,
  cellCount: PropTypes.string.isRequired,
  spanLastRow: PropTypes.bool.isRequired,
};

GridSveta.defaultProps = {
  className: null,
  cellCount: "6",
  spanLastRow: true,
};

export { GridSveta };
