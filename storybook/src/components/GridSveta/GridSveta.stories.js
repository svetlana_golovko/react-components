import React from 'react';

import { GridSveta } from './GridSveta';

export default {
    title: 'Core/Grid Sveta',
    component: GridSveta,
    argTypes: {
        className: {
            table: {
              category: "Component arguments",
            },
          },
        spanLastRow: {
          table: {
            category: "Component arguments",
          },
        },
        cellCount: {
          table: {
            category: "Story arguments",
          },
          description:
            "<b>Only generates cells for story, this variable is NOT used in the component</b>",
        },
      },
};

const dummyCells = (count, content) => {
    var cellCount = count ? parseInt(count) : 12;
    return [...Array(cellCount).keys()].map((i) =>
        <div key='${i}' className= "example-empty-div"> {content ? `Content ${i+1}` : ""} </div>);
}

const Template = (args) => {
    var data = dummyCells(args.cellCount, "")
    return (<div >
                <GridSveta {...args} >
                    {data}
                </GridSveta>
            </div>);
}

export const Primary = Template.bind({});
Primary.args = {
    cellCount : "6",
    spanLastRow : true
};

Primary.parameters = {
    viewport: {
        defaultViewport: "360",
    },
    controls: {
        expanded: true,
      },
};
