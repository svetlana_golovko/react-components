import "../src/assets/styles/main.scss";
import "../src/assets/styles/abstracts/_placeholders.scss";

const customViewports = {
  360: {
    name: "@360px",
    styles: {
      width: "360px",
      height: "640px",
    },
  },
  600: {
    name: "@600px",
    styles: {
      width: "600px",
      height: "800px",
    },
  },
  800: {
    name: "@800px",
    styles: {
      width: "800px",
      height: "600px",
    },
  },
  1024: {
    name: "@1024px",
    styles: {
      width: "1024px",
      height: "768px",
    },
  },
  1288: {
    name: "@1288px",
    styles: {
      width: "1288px",
      height: "1024px",
    },
  },
};

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  viewport: {
    viewports: customViewports,
  },
  layout: "fullscreen",
};
