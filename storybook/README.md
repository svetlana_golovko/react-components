# Grid test assignment

## Installation

To install and run the Storybook:

- clone the repository
- navigate to the `storybook` folder
- run the following commands in console to start Storybook:

```
npm i
npm run storybook
```

## Margus Grid component

- Implemented with Flex

Component and storybook files are in `src/components/`

- [GridMargus](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/components/GridMargus/)

Scss files are located in `src/assets/styles/components` and `src/assets/styles/abstracts`

Each component has a separate mixin file if needed

- [grid-margus-col.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/components/_grid-margus-col.scss)
- [grid-margus.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/components/_grid-margus.scss)
- [variables.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/abstracts/_variables.scss)
- [mixins.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/mixins/)

## Sveta Grid component

- Implemented with Grid

Component and storybook files are in `src/components/`

- [GridSveta](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/components/GridSveta/)

Scss files are located in `src/assets/styles/components` and `src/assets/styles/abstracts`

- [grid-sveta.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/components/_grid-sveta.scss)
- [variables.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/abstracts/_variables.scss)
- [mixins.scss](https://bitbucket.org/svetlana_golovko/react-components/src/master/storybook/src/assets/styles/mixins/)

## Storybook

Running Storybook command will open up a new webpage where you can see the components.
Grid components are visible under CORE section.

> CORE
>
> > Grid Margus
> >
> > Grid Sveta

Input control `cellCount` changes the number of cells shown in the gird. This input is only used for generating cells automatically, this is **NOT** a component argument.

Input control `spanLastRow` sets the last row cells to take up all the free space evenly.

There are 5 predefined viewport measurements that you can change the screen size with. The Viewport option can be changed at the top of the canvas on the toolbar. Default viewport is set to 360px at start.

## Nested Grid

To create a nested grid you have to create a new Grid in GridCol. And then declare each col size for the nested grids columns. 
Nested grid layouts can be created with the following example:

```
  <GridMargus className={args.className} fluid={args.fluid}>
      <GridMargusRow>
        <GridMargusCol>
          <GridMargus fluid={args.fluid}>
            <GridMargusRow>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
              <GridMargusCol col={6}>
                <div className="example-empty-div" />
              </GridMargusCol>
            </GridMargusRow>
          </GridMargus>
        </GridMargusCol>
        <GridMargusCol>
          <div className="example-empty-div" />
        </GridMargusCol>
      </GridMargusRow>
    </GridMargus>
```

## Importing project components to other projects


This project is used in [components](https://bitbucket.org/svetlana_golovko/react-components/src/master/components/) project as a local dependency. 

**!! After making changes run**
```
npm run dist
```

#### Other options
There is the option to create `npm package` and publish it as a public or private package. Both need a npm account, but private needs a paid account. And then you can use it as a dependency.

Another option would be using `npm link` to create links locally between projects.
Run `npm link` in the project folder you have your components and then `npm link <package name>` in the projects folder you want to use these components in.